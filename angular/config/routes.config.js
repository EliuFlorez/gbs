export function RoutesConfig ($stateProvider, $urlRouterProvider) {
	'ngInject'

	var getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`
	}

	var getLayout = (layout) => {
		return `./views/app/pages/layout/${layout}.page.html`
	}

	var setList = (url, tmpl) => {
		return { 
			url: url, 
			data: {auth: true},
			views: {'main@app': {template: '<'+tmpl+'-lists></'+tmpl+'-lists>'}}
		}
	}

	var setAdd = (url, tmpl) => {
		return { 
			url: url, 
			data: {auth: true},
			views: {'main@app': {template: '<'+tmpl+'-add></'+tmpl+'-add>'}},
			params: {alerts: null}
		}
	}

	var setEdit = (url, tmpl) => {
		return { 
			url: url, 
			data: {auth: true},
			views: {'main@app': {template: '<'+tmpl+'-edit></'+tmpl+'-edit>'}},
			params: {alerts: null, id: null}
		}
	}

	$urlRouterProvider.otherwise('/')

	$stateProvider
	.state('app', {
		abstract: true,
		views: {
			'layout': {templateUrl: getLayout('layout')},
			'header@app': {templateUrl: getView('header')},
			'footer@app': {templateUrl: getView('footer')},
			main: {}
		},
		data: {bodyClass: 'hold-transition skin-blue sidebar-mini'}
	})
	.state('app.landing', {
		url: '/',
		data: {auth: true},
		views: {
			'main@app': {templateUrl: getView('landing')}
		}
	})
	
	// Admin - User
	.state('app.user-lists', setList('/admin/users/lists', 'user'))
	.state('app.user-add', setAdd('/admin/users/add', 'user'))
	.state('app.user-edit', setEdit('/admin/users/edit/:id', 'user'))

	// Admin - Roles
	.state('app.role-lists', setList('/admin/roles/lists', 'role'))
	.state('app.role-add', setAdd('/admin/roles/add', 'role'))
	.state('app.role-edit', setEdit('/admin/roles/edit/:id', 'role'))

	// Admin - Permissions
	.state('app.permission-lists', setList('/admin/permissions/lists', 'permission'))
	.state('app.permission-add', setAdd('/admin/permissions/add', 'permission'))
	.state('app.permission-edit', setEdit('/admin/permissions/edit/:id', 'permission'))

	// Reception
	.state('app.policy-add', setAdd('/receptions/policy/add', 'policy'))
	.state('app.claim-add', setAdd('/receptions/claim/add', 'claim'))

	// Sale - Company
	.state('app.company-lists', setList('/sales/company/lists', 'company'))
	.state('app.company-add', setAdd('/sales/company/add', 'company'))
	.state('app.company-edit', setEdit('/sales/company/edit/:id', 'company'))

	// Sale - Sellers
	.state('app.seller-lists', setList('/sales/seller/lists', 'seller'))
	.state('app.seller-add', setAdd('/sales/seller/add', 'seller'))
	.state('app.seller-edit', setEdit('/sales/seller/edit/:id', 'seller'))

	// Sale - Sales
	.state('app.sale-lists', setList('/sales/lists', 'sale'))
	.state('app.sale-add', setAdd('/sales/add', 'sale'))
	.state('app.sale-edit', setEdit('/sales/edit/:id', 'sale'))

	// Sale - Commission
	.state('app.commission-company-lists', setList('/sales/commission-company/lists', 'commission-company'))
	.state('app.commission-seller-lists', setList('/sales/commission-seller/lists', 'commission-seller'))

	// Service - Hospital
	.state('app.hospital-lists', setList('/services/hospital/lists', 'hospital'))
	.state('app.hospital-add', setAdd('/services/hospital/add', 'hospital'))
	.state('app.hospital-edit', setEdit('/services/hospital/edit/:id', 'hospital'))

	// Service - Doctor
	.state('app.doctor-lists', setList('/services/doctor/lists', 'doctor'))
	.state('app.doctor-add', setAdd('/services/doctor/add', 'doctor'))
	.state('app.doctor-edit', setEdit('/services/doctor/edit/:id', 'doctor'))

	// Service - Specialty
	.state('app.specialty-lists', setList('/services/specialty/lists', 'specialty'))
	.state('app.specialty-add', setAdd('/services/specialty/add', 'specialty'))
	.state('app.specialty-edit', setEdit('/services/specialty/edit/:id', 'specialty'))

	// General - Policies
	.state('app.policies-lists', setList('/general/policies/lists', 'policies'))

	// General - Affiliates
	.state('app.affiliates-lists', setList('/general/affiliates/lists', 'affiliates'))

	// General - Procedure
	.state('app.procedure-lists', setList('/general/procedure/lists', 'procedure'))
	.state('app.procedure-add', setAdd('/general/procedure/add', 'procedure'))
	.state('app.procedure-edit', setEdit('/general/procedure/edit/:id', 'procedure'))
	
	// General - Messenger
	.state('app.messenger-lists', setList('/general/messenger/lists', 'messenger'))
	.state('app.messenger-add', setAdd('/general/messenger/add', 'messenger'))
	.state('app.messenger-edit', setEdit('/general/messenger/edit/:id', 'messenger'))

	.state('app.profile', {
		url: '/profile',
		data: {auth: true},
		views: {
			'main@app': {template: '<user-profile></user-profile>'}
		},
		params: {alerts: null}
	})
	.state('login', {
		url: '/login',
		views: {
			'layout': {templateUrl: getView('login')},
			'header@app': {},
			'footer@app': {}
		},
		data: {bodyClass: 'hold-transition login-page'},
		params: {registerSuccess: null, successMsg: null}
	})
	.state('loginloader', {
		url: '/login-loader',
		views: {
			'layout': {templateUrl: getView('login-loader')},
			'header@app': {},
			'footer@app': {}
		},
		data: {bodyClass: 'hold-transition login-page'}
	})
	.state('register', {
		url: '/register',
		views: {
			'layout': {templateUrl: getView('register')},
			'header@app': {},
			'footer@app': {}
		},
		data: {bodyClass: 'hold-transition register-page'}
	})
	.state('userverification', {
		url: '/userverification/:status',
		views: {
			'layout': {templateUrl: getView('user-verification')}
		},
		data: {bodyClass: 'hold-transition login-page'},
		params: {status: null}
	})
	.state('forgot_password', {
		url: '/forgot-password',
		views: {
			'layout': {templateUrl: getView('forgot-password')},
			'header@app': {},
			'footer@app': {}
		},
		data: {bodyClass: 'hold-transition login-page'}
	})
	.state('reset_password', {
		url: '/reset-password/:email/:token',
		views: {
			'layout': {templateUrl: getView('reset-password')},
			'header@app': {},
			'footer@app': {}
		},
		data: {bodyClass: 'hold-transition login-page'}
	})
	.state('app.logout', {
		url: '/logout',
		views: {
			'main@app': {
				controller: function ($rootScope, $scope, $auth, $state, AclService) {
					$auth.logout().then(function () {
						delete $rootScope.me
						AclService.flushRoles()
						AclService.setAbilities({})
						$state.go('login')
					})
				}
			}
		}
	})
}
