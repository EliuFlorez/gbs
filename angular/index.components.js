
/* Reception */
import { PolicyAddComponent } from './app/components/reception/policy-add/policy-add.component';
import { ClaimAddComponent } from './app/components/reception/claim-add/claim-add.component';

/* Sales - Company */
import { CompanyEditComponent } from './app/components/sales/companies/edit/company-edit.component';
import { CompanyAddComponent } from './app/components/sales/companies/add/company-add.component';
import { CompanyListsComponent } from './app/components/sales/companies/lists/company-lists.component';

/* Sales - Sellers */
import { SellerEditComponent } from './app/components/sales/sellers/edit/seller-edit.component';
import { SellerAddComponent } from './app/components/sales/sellers/add/seller-add.component';
import { SellerListsComponent } from './app/components/sales/sellers/lists/seller-lists.component';

import { CommissionCompanyListsComponent } from './app/components/sales/commission-company/commission-company-lists.component';
import { CommissionSellerListsComponent } from './app/components/sales/commission-seller/commission-seller-lists.component';

/* Sales - Sales */
import { SaleEditComponent } from './app/components/sales/sales/edit/sale-edit.component';
import { SaleAddComponent } from './app/components/sales/sales/add/sale-add.component';
import { SaleListsComponent } from './app/components/sales/sales/lists/sale-lists.component';

/* Service - Hospital */
import { HospitalEditComponent } from './app/components/services/hospital/edit/hospital-edit.component';
import { HospitalAddComponent } from './app/components/services/hospital/add/hospital-add.component';
import { HospitalListsComponent } from './app/components/services/hospital/lists/hospital-lists.component';

/* Service - Doctor */
import { DoctorEditComponent } from './app/components/services/doctor/edit/doctor-edit.component';
import { DoctorAddComponent } from './app/components/services/doctor/add/doctor-add.component';
import { DoctorListsComponent } from './app/components/services/doctor/lists/doctor-lists.component';

/* Service - Specialty */
import { SpecialtyEditComponent } from './app/components/services/specialty/edit/specialty-edit.component';
import { SpecialtyAddComponent } from './app/components/services/specialty/add/specialty-add.component';
import { SpecialtyListsComponent } from './app/components/services/specialty/lists/specialty-lists.component';

/* General - Policies */
import { PoliciesListsComponent } from './app/components/general/policies/lists/policies-lists.component';

/* General - Affiliates */
import { AffiliatesListsComponent } from './app/components/general/affiliates/lists/affiliates-lists.component';

/* General - Proveedor */
import { ProcedureEditComponent } from './app/components/general/procedure/edit/procedure-edit.component';
import { ProcedureAddComponent } from './app/components/general/procedure/add/procedure-add.component';
import { ProcedureListsComponent } from './app/components/general/procedure/lists/procedure-lists.component';

import { MessengerEditComponent } from './app/components/general/messenger/edit/messenger-edit.component';
import { MessengerAddComponent } from './app/components/general/messenger/add/messenger-add.component';
import { MessengerListsComponent } from './app/components/general/messenger/lists/messenger-lists.component';

import { UserProfileComponent } from './app/components/user-profile/user-profile.component'
import { UserVerificationComponent } from './app/components/user-verification/user-verification.component'

/* Admin - Users */
import { UserEditComponent } from './app/components/admin/users/edit/user-edit.component'
import { UserAddComponent } from './app/components/admin/users/add/user-add.component'
import { UserListsComponent } from './app/components/admin/users/lists/user-lists.component'

/* Admin - Permissions */
import { PermissionEditComponent } from './app/components/admin/permissions/edit/permission-edit.component'
import { PermissionAddComponent } from './app/components/admin/permissions/add/permission-add.component'
import { PermissionListsComponent } from './app/components/admin/permissions/lists/permission-lists.component'

/* Admin - Roles */
import { RoleEditComponent } from './app/components/admin/roles/edit/role-edit.component'
import { RoleAddComponent } from './app/components/admin/roles/add/role-add.component'
import { RoleListsComponent } from './app/components/admin/roles/lists/role-lists.component'

import { DashboardComponent } from './app/components/dashboard/dashboard.component'
import { NavSidebarComponent } from './app/components/nav-sidebar/nav-sidebar.component'
import { NavHeaderComponent } from './app/components/nav-header/nav-header.component'
import { LoginLoaderComponent } from './app/components/login-loader/login-loader.component'
import { ResetPasswordComponent } from './app/components/reset-password/reset-password.component'
import { ForgotPasswordComponent } from './app/components/forgot-password/forgot-password.component'
import { LoginFormComponent } from './app/components/login-form/login-form.component'
import { RegisterFormComponent } from './app/components/register-form/register-form.component'

angular.module('app.components')
	.component('policyAdd', PolicyAddComponent)
	.component('claimAdd', ClaimAddComponent)
	
	.component('companyEdit', CompanyEditComponent)
	.component('companyAdd', CompanyAddComponent)
	.component('companyLists', CompanyListsComponent)
	
	.component('sellerEdit', SellerEditComponent)
	.component('sellerAdd', SellerAddComponent)
	.component('sellerLists', SellerListsComponent)
	
	.component('saleEdit', SaleEditComponent)
	.component('saleAdd', SaleAddComponent)
	.component('saleLists', SaleListsComponent)
	
	.component('commissionCompanyLists', CommissionCompanyListsComponent)
	.component('commissionSellerLists', CommissionSellerListsComponent)
	
	.component('hospitalEdit', HospitalEditComponent)
	.component('hospitalAdd', HospitalAddComponent)
	.component('hospitalLists', HospitalListsComponent)
	
	.component('doctorEdit', DoctorEditComponent)
	.component('doctorAdd', DoctorAddComponent)
	.component('doctorLists', DoctorListsComponent)
	
	.component('specialtyEdit', SpecialtyEditComponent)
	.component('specialtyAdd', SpecialtyAddComponent)
	.component('specialtyLists', SpecialtyListsComponent)
	
	.component('policiesLists', PoliciesListsComponent)
	
	.component('affiliatesLists', AffiliatesListsComponent)
	
	.component('procedureEdit', ProcedureEditComponent)
	.component('procedureAdd', ProcedureAddComponent)
	.component('procedureLists', ProcedureListsComponent)
	
	.component('messengerEdit', MessengerEditComponent)
	.component('messengerAdd', MessengerAddComponent)
	.component('messengerLists', MessengerListsComponent)
	
	.component('userProfile', UserProfileComponent)
	.component('userVerification', UserVerificationComponent)

	.component('userEdit', UserEditComponent)
	.component('userAdd', UserAddComponent)
	.component('userLists', UserListsComponent)

	.component('permissionEdit', PermissionEditComponent)
	.component('permissionAdd', PermissionAddComponent)
	.component('permissionLists', PermissionListsComponent)

	.component('roleEdit', RoleEditComponent)
	.component('roleAdd', RoleAddComponent)
	.component('roleLists', RoleListsComponent)

	.component('dashboard', DashboardComponent)
	.component('navSidebar', NavSidebarComponent)
	.component('navHeader', NavHeaderComponent)
	.component('loginLoader', LoginLoaderComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('registerForm', RegisterFormComponent)
