class HospitalAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
		
		let Countries = API.service('countries')
    Countries.getList()
      .then((response) => {
        let slCountries = []
        let rResponse = response.plain()
				console.log('rResponse:', rResponse)
				angular.forEach(rResponse, function (value) {
          slCountries.push({id: value.id, name: value.name})
        })
        this.slCountries = slCountries
      })
			
		let States = API.service('states')
    States.getList()
      .then((response) => {
        let slStates = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slStates.push({id: value.id, name: value.name})
        })
        this.slStates = slStates
      })
			
		let Cities = API.service('cities')
    Cities.getList()
      .then((response) => {
        let slCities = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slCities.push({id: value.id, name: value.name})
        })
        this.slCities = slCities
      })
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let Hospitals = this.API.service('hospitals')
      let $state = this.$state

      Hospitals.post({
        'name': this.name,
        'country_id': this.country.id,
        'province_id': this.state.id,
        'city_id': this.city.id,
        'address': this.address
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const HospitalAddComponent = {
  templateUrl: './views/app/components/services/hospital/add/hospital-add.component.html',
  controller: HospitalAddController,
  controllerAs: 'vm',
  bindings: {}
}
