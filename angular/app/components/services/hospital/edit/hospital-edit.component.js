class HospitalEditController {
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

		let Countries = API.service('countries')
    Countries.getList()
      .then((response) => {
        let slCountries = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slCountries.push({id: value.id, name: value.name})
        })
        this.slCountries = slCountries
      })
			
		let States = API.service('states')
    States.getList()
      .then((response) => {
        let slStates = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slStates.push({id: value.id, name: value.name})
        })
        this.slStates = slStates
      })
			
		let Cities = API.service('cities')
    Cities.getList()
      .then((response) => {
        let slCities = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slCities.push({id: value.id, name: value.name})
        })
        this.slCities = slCities
      })

    let id = $stateParams.id
    
    API.one('hospitals', id).get()
      .then((response) => {
        this.rs = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.rs.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Success!', msg: 'El registro ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const HospitalEditComponent = {
  templateUrl: './views/app/components/services/hospital/edit/hospital-edit.component.html',
  controller: HospitalEditController,
  controllerAs: 'vm',
  bindings: {}
}
