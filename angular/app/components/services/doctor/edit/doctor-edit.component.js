class DoctorEditController {
	
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

		let Hospitals = API.service('hospitals')
    Hospitals.getList()
      .then((response) => {
        let slHospitals = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slHospitals.push({id: value.id, name: value.name})
        })
        this.slHospitals = slHospitals
      })
			
    let id = $stateParams.id
		
    API.one('doctors', id).get()
      .then((response) => {
        this.rs = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.rs.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Listo!', msg: 'El registros ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const DoctorEditComponent = {
  templateUrl: './views/app/components/services/doctor/edit/doctor-edit.component.html',
  controller: DoctorEditController,
  controllerAs: 'vm',
  bindings: {}
}
