class DoctorAddController {
	
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
		
		let Hospitals = API.service('hospitals')
    Hospitals.getList()
      .then((response) => {
        let slHospitals = []
        let rResponse = response.plain()
				angular.forEach(rResponse, function (value) {
          slHospitals.push({id: value.id, name: value.name})
        })
        this.slHospitals = slHospitals
      })
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('doctors')
      let $state = this.$state

      apiData.post({
        'name': this.name,
				'hospital_id': this.hospital.id
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const DoctorAddComponent = {
  templateUrl: './views/app/components/services/doctor/add/doctor-add.component.html',
  controller: DoctorAddController,
  controllerAs: 'vm',
  bindings: {}
}
