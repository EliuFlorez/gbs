class EmployeeEditController {
	
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

    let id = $stateParams.id
		
    API.one('employees', id).get()
      .then((response) => {
        this.rs = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.rs.put()
        .then(() => {
          let alert = { 
						type: 'success', 
						'title': 'Listo!', 
						msg: 'El registros ha sido actualizado.' 
					}
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { 
						type: 'error', 
						'title': 'Error!', 
						msg: response.data.message 
					}
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const EmployeeEditComponent = {
  templateUrl: './views/app/components/rrhh/employees/edit/employee-edit.component.html',
  controller: EmployeeEditController,
  controllerAs: 'vm',
  bindings: {}
}
