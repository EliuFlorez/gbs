class EmployeeAddController {
	
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('employees')
      let $state = this.$state

      apiData.post({
        'first_name': this.first_name,
        'last_name': this.last_name
      }).then(function () {
        let alert = { 
					type: 'success', 
					'title': 'Listo!', 
					msg: 'El registro ha sido agregado.' 
				}
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { 
					type: 'error', 
					'title': 'Error!', 
					msg: response.data.message 
				}
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const EmployeeAddComponent = {
  templateUrl: './views/app/components/rrhh/employees/add/employee-add.component.html',
  controller: EmployeeAddController,
  controllerAs: 'vm',
  bindings: {}
}
