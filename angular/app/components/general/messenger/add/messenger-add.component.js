class MessengerAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []
		this.API = API

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('messengers')
      let $state = this.$state

      apiData.post({
        'name': this.name,
        'type': this.type,
        'dni_type': this.dni_type,
        'dni': this.dni
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const MessengerAddComponent = {
  templateUrl: './views/app/components/general/messenger/add/messenger-add.component.html',
  controller: MessengerAddController,
  controllerAs: 'vm',
  bindings: {}
}
