class PoliciesListsController {
	
  constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
    'ngInject'
    this.API = API
    this.$state = $state

    let apiData = this.API.service('employees')

    apiData.getList()
      .then((response) => {
        let dataSet = response.plain()

        this.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('data', dataSet)
          .withOption('createdRow', createdRow)
          .withOption('responsive', true)
          .withBootstrap()

        this.dtColumns = [
          DTColumnBuilder.newColumn('id').withTitle('ID'),
          DTColumnBuilder.newColumn('first_name').withTitle('Titular'),
          DTColumnBuilder.newColumn('last_name').withTitle('Plan/Deducible'),
          DTColumnBuilder.newColumn('last_name').withTitle('Agente'),
          DTColumnBuilder.newColumn('last_name').withTitle('Inicio de Cobertura'),
          DTColumnBuilder.newColumn('last_name').withTitle('Fin de Cobertura'),
          DTColumnBuilder.newColumn('last_name').withTitle('Estado'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
            .renderWith(actionsHtml)
        ]

        this.displayTable = true
      })

    let createdRow = (row) => {
      $compile(angular.element(row).contents())($scope)
    }

    let actionsHtml = (data) => {
      return `
				<button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
						<i class="fa fa-trash-o"></i>
				</button>`
    }
  }

  delete (id) {
    let API = this.API
    let $state = this.$state

    swal({
      title: '¿Estás seguro de eliminar este registro?',
      text: '¡No podrá recuperar estos datos!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si, Eliminar el registrol',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: false
    }, function () {
      API.one('employees', id).remove()
        .then(() => {
          swal({
            title: 'Borrado!',
            text: 'Se ha eliminado el registro.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })
    })
  }

  $onInit () {}
}

export const PoliciesListsComponent = {
  templateUrl: './views/app/components/general/policies/lists/policies-lists.component.html',
  controller: PoliciesListsController,
  controllerAs: 'vm',
  bindings: {}
}
