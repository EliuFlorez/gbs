class ProcedureAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []
		this.API = API

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('procedures')
      let $state = this.$state

      apiData.post({
        'name': this.name,
        'description': this.description
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const ProcedureAddComponent = {
  templateUrl: './views/app/components/general/procedure/add/procedure-add.component.html',
  controller: ProcedureAddController,
  controllerAs: 'vm',
  bindings: {}
}
