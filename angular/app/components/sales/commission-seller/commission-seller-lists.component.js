class CommissionSellerListsController {
  constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
    'ngInject'
    this.API = API
    this.$state = $state

    let apiData = this.API.service('commissionsSellers')

    apiData.getList()
      .then((response) => {
        let dataSet = response.plain()

        this.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('data', dataSet)
          .withOption('createdRow', createdRow)
          .withOption('responsive', true)
          .withBootstrap()

        this.dtColumns = [
          DTColumnBuilder.newColumn('id').withTitle('ID'),
          DTColumnBuilder.newColumn('policy_number').withTitle('Numero de Póliza'),
          DTColumnBuilder.newColumn('company_id').withTitle('Compañia'),
          DTColumnBuilder.newColumn('seller_id').withTitle('Agente'),
          DTColumnBuilder.newColumn('commission_type_id').withTitle('Tipo de Comision'),
          DTColumnBuilder.newColumn('percentage_applied').withTitle('Porcentaje aplicado'),
          DTColumnBuilder.newColumn('extra_percentage_applied').withTitle('Porcentaje adicional'),
          DTColumnBuilder.newColumn('value').withTitle('Pago'),
          DTColumnBuilder.newColumn('created_at').withTitle('Fecha'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
            .renderWith(actionsHtml)
        ]
		
        this.displayTable = true
      })

    let createdRow = (row) => {
      $compile(angular.element(row).contents())($scope)
    }

    let actionsHtml = (data) => {
      return `
		<a class="btn btn-xs btn-warning">
			<i class="fa fa-edit"></i>
		</a>
		&nbsp
		<button class="btn btn-xs btn-danger">
			<i class="fa fa-trash-o"></i>
		</button>`
    }
  }

  $onInit () {}
}

export const CommissionSellerListsComponent = {
  templateUrl: './views/app/components/sales/commission-seller/commission-seller-lists.component.html',
  controller: CommissionSellerListsController,
  controllerAs: 'vm',
  bindings: {}
}
