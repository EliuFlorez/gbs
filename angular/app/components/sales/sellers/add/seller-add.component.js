class SellerAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
	
	let Companies = API.service('companies')
    Companies.getList()
      .then((response) => {
        let slCompanies = []
        let companyResponse = response.plain()

        angular.forEach(companyResponse, function (value) {
          slCompanies.push({id: value.id, name: value.name})
        })

        this.slCompanies = slCompanies
      })
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('sellers')
      let $state = this.$state

      apiData.post({
        'company_id': this.company.id,
        'name': this.name,
        'monthly_fee': this.monthly_fee,
        'percentage_extra': this.percentage_extra,
        'percentage_vn': this.percentage_vn,
        'percentage_rp': this.percentage_rp,
        'commission_type_id': 1
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const SellerAddComponent = {
  templateUrl: './views/app/components/sales/sellers/add/seller-add.component.html',
  controller: SellerAddController,
  controllerAs: 'vm',
  bindings: {}
}
