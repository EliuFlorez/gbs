class SellerEditController {
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

	let Companies = API.service('companies')
    Companies.getList()
      .then((response) => {
        let slCompanies = []
        let companyResponse = response.plain()

        angular.forEach(companyResponse, function (value) {
          slCompanies.push({id: value.id, name: value.name})
        })

        this.slCompanies = slCompanies
      })
	  
    let id = $stateParams.id
		
    API.service('sellers').one(id).get()
      .then((response) => {
        this.rs = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.rs.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Listo!', msg: 'El registros ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const SellerEditComponent = {
  templateUrl: './views/app/components/sales/sellers/edit/seller-edit.component.html',
  controller: SellerEditController,
  controllerAs: 'vm',
  bindings: {}
}
