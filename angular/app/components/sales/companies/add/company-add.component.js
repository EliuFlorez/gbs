class CompanyAddController {
	
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('companies')
      let $state = this.$state

      apiData.post({
        'name': this.name,
        'commission_type_id': 1,
		'percentage': this.percentage
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const CompanyAddComponent = {
  templateUrl: './views/app/components/sales/companies/add/company-add.component.html',
  controller: CompanyAddController,
  controllerAs: 'vm',
  bindings: {}
}
