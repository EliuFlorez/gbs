class SaleAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
	
	let Companies = API.service('companies')
    Companies.getList()
      .then((response) => {
        let slCompanies = []
        let companyResponse = response.plain()

        angular.forEach(companyResponse, function (value) {
          slCompanies.push({id: value.id, name: value.name})
        })

        this.slCompanies = slCompanies
      })
	  
	let Sellers = API.service('sellers')
    Sellers.getList()
      .then((response) => {
        let slSellers = []
        let sellerResponse = response.plain()

        angular.forEach(sellerResponse, function (value) {
          slSellers.push({id: value.id, name: value.name})
        })

        this.slSellers = slSellers
      })
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('sales')
      let $state = this.$state

      apiData.post({
        'policy_number': this.policy_number,
        'sale_type_id': this.sale_type_id,
        'company_id': this.company.id,
        'seller_id': this.seller.id,
        'total_amount': this.total_amount,
        'sale_date': this.sale_date,
        'payment_date': this.payment_date,
        'confirmation_date': this.confirmation_date
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const SaleAddComponent = {
  templateUrl: './views/app/components/sales/sales/add/sale-add.component.html',
  controller: SaleAddController,
  controllerAs: 'vm',
  bindings: {}
}
