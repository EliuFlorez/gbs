class RoleEditController {
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

    let Permissions = API.service('permissions')

    Permissions.getList()
      .then((response) => {
        let permissionList = []
        let permissionResponse = response.plain()

        angular.forEach(permissionResponse, function (value) {
          permissionList.push({id: value.id, name: value.name})
        })

        this.systemPermissions = permissionList
      })

    let id = $stateParams.id
    
    API.one('roles', id).get()
      .then((response) => {
        let rolePermissions = []

        angular.forEach(response.data.permissions, function (value) {
          rolePermissions.push(value.id)
        })

        response.data.permissions = rolePermissions

        this.role = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.role.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Success!', msg: 'El registro ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const RoleEditComponent = {
  templateUrl: './views/app/components/admin/roles/edit/role-edit.component.html',
  controller: RoleEditController,
  controllerAs: 'vm',
  bindings: {}
}
