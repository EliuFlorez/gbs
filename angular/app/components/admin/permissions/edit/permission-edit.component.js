class PermissionEditController {
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

    let id = $stateParams.id
    
    API.one('permissions', id).get()
      .then((response) => {
        this.permission = API.copy(response)
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.permission.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const PermissionEditComponent = {
  templateUrl: './views/app/components/admin/permissions/edit/permission-edit.component.html',
  controller: PermissionEditController,
  controllerAs: 'vm',
  bindings: {}
}
