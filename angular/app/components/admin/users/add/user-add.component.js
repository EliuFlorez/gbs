class UserAddController {
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
	
	let Roles = API.service('roles')
    Roles.getList()
      .then((response) => {
        let ckRoles = []
        let roleResponse = response.plain()

        angular.forEach(roleResponse, function (value) {
          ckRoles.push({id: value.id, name: value.name})
        })

        this.ckRoles = ckRoles
      })
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('users')
      let $state = this.$state

      apiData.post({
        'name': this.name,
        'email': this.email,
        'password': this.password,
        'password_confirmation': this.password_confirmation,
        'role': this.role
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
		/*
		let formErrors = []

		if (angular.isDefined(response.data.errors.message)) {
			formErrors = response.data.errors.message[0]
		} else {
			formErrors = response.data.errors
		}

		angular.forEach(formErrors, function (value, key) {
			let varkey = key.replace('data.', '')
			uiForm[varkey].$invalid = true
			uiForm[varkey].customError = value[0]
		})
		
		//<p ng-show="uiForm.varkey.customError" class="help-block">{{uiForm.varkey.customError}}</p>
		*/
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const UserAddComponent = {
  templateUrl: './views/app/components/admin/users/add/user-add.component.html',
  controller: UserAddController,
  controllerAs: 'vm',
  bindings: {}
}
