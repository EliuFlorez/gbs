class UserListsController {
  constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
    'ngInject'
    this.API = API
    this.$state = $state

    let Usuarios = this.API.service('users')

    Usuarios.getList()
      .then((response) => {
        let dataSet = response.plain()

        this.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('data', dataSet)
          .withOption('createdRow', createdRow)
          .withOption('responsive', true)
          .withBootstrap()

        this.dtColumns = [
          DTColumnBuilder.newColumn('id').withTitle('ID'),
          DTColumnBuilder.newColumn('name').withTitle('Nombre'),
          DTColumnBuilder.newColumn('email').withTitle('Email'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
            .renderWith(actionsHtml)
        ]

        this.displayTable = true
      })

    let createdRow = (row) => {
      $compile(angular.element(row).contents())($scope)
    }

    let actionsHtml = (data) => {
      return `
		<a class="btn btn-xs btn-warning" ui-sref="app.user-edit({id: ${data.id}})">
				<i class="fa fa-edit"></i>
		</a>
		&nbsp
		<button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
				<i class="fa fa-trash-o"></i>
		</button>`
    }
  }

  delete (id) {
    let API = this.API
    let $state = this.$state

    swal({
      title: '¿Estás seguro de eliminar este registro?',
      text: '¡No podrá recuperar estos datos!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si, Eliminar el registrol',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: false
    }, function () {
      API.one('users', id).remove()
        .then(() => {
          swal({
            title: 'Borrado!',
            text: 'Se ha eliminado el registro.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })
    })
  }

  $onInit () {}
}

export const UserListsComponent = {
  templateUrl: './views/app/components/admin/users/lists/user-lists.component.html',
  controller: UserListsController,
  controllerAs: 'vm',
  bindings: {}
}
