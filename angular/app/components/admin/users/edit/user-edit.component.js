class UserEditController {
  constructor ($stateParams, $state, API) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []
    this.userRolesSelected = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

    let id = $stateParams.id

    let Roles = API.service('roles')
    Roles.getList()
      .then((response) => {
        let ckRoles = []
        let roleResponse = response.plain()

        angular.forEach(roleResponse, function (value) {
          ckRoles.push({id: value.id, name: value.name})
        })

        this.ckRoles = ckRoles
      })

    let UserData = API.service('users')
    UserData.one(id).get()
      .then((response) => {
        let userRole = []
        let userResponse = response.plain()

        angular.forEach(userResponse.data.role, function (value) {
          userRole.push(value.id)
        })

        response.data.role = userRole
        
		this.rs = API.copy(response)
		this.rs.data.password = ''
        this.rs.data.password_confirmation = ''
      })
  }

  save (isValid) {
    if (isValid) {
      let $state = this.$state
      this.rs.put()
        .then(() => {
          let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido actualizado.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const UserEditComponent = {
  templateUrl: './views/app/components/admin/users/edit/user-edit.component.html',
  controller: UserEditController,
  controllerAs: 'vm',
  bindings: {}
}
