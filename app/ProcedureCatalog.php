<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcedureCatalog extends Model
{
	protected $table = 'procedure_catalog';
	
	protected $fillable = [
		'name',
		'description'
	];

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
		'deleted_at'
	];

	public function procedureEntry()
	{
		return $this->hasMany('App\ProcedureEntry', 'procedure_catalog_id', 'id');
	}

	public function processCatalog()
	{
		return $this->hasMany('App\ProcessCatalog', 'procedure_catalog_id', 'id');
	}

	public function getFirstProcessCatalog()
	{
		return $this->processCatalog()->where('number_next', 1)->firstOrFail();
	}
	
	public function getLastProcessCatalog()
	{
		return $this->processCatalog()->where('number_last', 1)->firstOrFail();
	}
	
}
