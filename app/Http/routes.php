<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');
    Route::get('/unsupported-browser', 'AngularController@unsupported');
    Route::get('user/verify/{verificationCode}', ['uses' => 'Auth\AuthController@verifyEmail']);
    Route::get('/api/authenticate/user', 'Auth\AuthController@getAuthenticatedUser');
});

$api->group(['middleware' => ['api']], function ($api) {
    $api->post('auth/login', 'Auth\AuthController@postLogin');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
	// Me
    $api->get('users/me', 'Auth\AuthController@getMe');
    $api->put('users/me', 'Auth\AuthController@putMe');
	
	// Address
	$api->get('countries', 'AddressController@getCountry');
	$api->get('states', 'AddressController@getStates');
	$api->get('cities', 'AddressController@getCity');
});

$api->group(['middleware' => ['api', 'api.auth', 'role:admin.super|admin.user']], function ($api) {
    // Admin
	$api->resource('users', 'Settings\UsersController');
	$api->resource('roles', 'Settings\RolesController');
	$api->resource('permissions', 'Settings\PermissionsController');
	
	// Service Client
	$api->resource('hospitals', 'Services\HospitalsController');
	$api->resource('doctors', 'Services\DoctorsController');
	$api->resource('specialtys', 'Services\SpecialtysController');
	
	// Generals
	$api->resource('messengers', 'General\MessengersController');
	$api->resource('procedures', 'General\ProceduresController');
	
	// RRHH
	$api->resource('employees', 'Rrhh\EmployeesController');
	
	// Reception
	$api->resource('reception/policy', 'Reception\PolicyController');
});

Route::get('policy', 'Reception\PolicyController@store');
Route::get('email/basic', 'MailController@basic_email');
Route::get('email/html', 'MailController@html_email');
Route::get('email/attachment', 'MailController@attachment_email');
