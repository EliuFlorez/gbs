<?php

namespace App\Http\Controllers\Rrhh;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Rrhh\Employee;
use Auth;
use Validator;

class EmployeesController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($bool = false, $unique = null)
    {
		$input = '';
		if ($bool == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',dni,'.$unique;
		}
		
		$rules = [
			$input.'first_name' => 'required|max:50',
			$input.'last_name' => 'required|max:50',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$employees = Employee::all();

        return response()->success(compact('employees'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules();
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$employee = Employee::create([
            'first_name' => $formData['first_name'],
            'last_name' => $formData['last_name'],
        ]);

        return response()->success(compact('employee'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$employee = Employee::find($id);

        return response()->success($employee);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Employee::where('id', '=', intval($id))->update([
            'first_name' => $formData['first_name'],
            'last_name' => $formData['last_name'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);

        return response()->success('success');
    }
}
