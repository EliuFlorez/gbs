<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Models\General\Messenger;
use Illuminate\Http\Request;

use Auth;
use Validator;

class MessengersController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($required = true, $unique = null)
    {
		$input = '';
		if ($required == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',dni,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'type' => 'required|max:50',
			$input.'dni' => 'required|max:50|unique:messengers' . $unique,
			$input.'dni_type' => 'required|max:50',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$messengers = Messenger::all();

        return response()->success(compact('messengers'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$messenger = Messenger::create([
            'name' => $formData['name'],
            'type' => $formData['type'],
            'dni' => $formData['dni'],
            'dni_type' => $formData['dni_type'],
        ]);

        return response()->success(compact('messenger'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$messenger = Messenger::find($id);

        return response()->success($messenger);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Messenger::where('id', '=', intval($id))->update([
            'name' => $formData['name'],
            'type' => $formData['type'],
            'dni' => $formData['dni'],
            'dni_type' => $formData['dni_type'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Messenger::destroy($id);

        return response()->success('success');
    }
}
