<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Models\General\Procedure;
use Illuminate\Http\Request;

use Auth;
use Validator;

class ProceduresController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($required = true, $unique = null)
    {
		$input = '';
		if ($required == true) {
			$input = 'data.';
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'description' => 'required|max:50',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$procedures = Procedure::all();

        return response()->success(compact('procedures'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$procedure = Procedure::create([
            'name' => $formData['name'],
            'description' => $formData['description'],
        ]);

        return response()->success(compact('procedure'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$procedure = Procedure::find($id);

        return response()->success($procedure);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Procedure::where('id', '=', intval($id))->update([
            'name' => $formData['name'],
            'description' => $formData['description'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Procedure::destroy($id);

        return response()->success('success');
    }
}
