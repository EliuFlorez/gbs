<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\AddressCountry;
use App\Models\AddressState;
use App\Models\AddressCity;

class AddressController extends Controller
{
    public function index()
    {
        $countries = AddressCountry::all();
		$states = AddressState::all();
		$cities = AddressCity::all();
		
		return response()->success(compact('countries', 'states', 'cities'));
    }
	
	public function getCountry()
    {
        $countries = AddressCountry::all();
		return response()->success(compact('countries'));
    }
	
	public function getStates()
    {
        
		return response()->success(compact('states'));
    }
	
	public function getCity()
    {
        
        return response()->success(compact('cities'));
    }
}
