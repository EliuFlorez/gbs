<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use Validator;

class RolesController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($required = true, $unique = null)
    {
		$input = '';
		if ($required == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',slug,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'slug' => 'required|max:50|unique:roles' . $unique,
			//$input.'level' => 'required',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$roles = Role::all();

        return response()->success(compact('roles'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$role = Role::create([
            'name' => $formData['name'],
            'slug' => str_slug($formData['slug'], '.'),
            'description' => $formData['description'],
        ]);

        return response()->success(compact('role'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$role = Role::find($id);

        $role['permissions'] = $role->permissions()
			->select([
				'permissions.name', 
				'permissions.id'
			])
			->get();

        return response()->success($role);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$formData = $request->input('data');
		
		$rules = $this->getRules(true, $id);
		
        $this->validate($request, $rules);
		
		$roleData = [
            'name' => $formData['name'],
            'slug' => $formData['slug'],
            'description' => $formData['description'],
        ];

        $formData['slug'] = str_slug($formData['slug'], '.');
		
        Role::where('id', '=', intval($id))->update($roleData);
			
        $role = Role::find($id);

        $role->detachAllPermissions();

        foreach ($formData['permissions'] as $setPermission) 
		{
            $role->attachPermission($setPermission);
        }

        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Role::destroy($id);

        return response()->success('success');
    }
}
