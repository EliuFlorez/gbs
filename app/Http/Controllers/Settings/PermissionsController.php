<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Bican\Roles\Models\Permission;
use Illuminate\Http\Request;
use Validator;

class PermissionsController extends Controller 
{
	/**
     * Validation rules.
     *
     */
    protected function getRules($required = true, $unique = null)
    {
		$input = '';
		if ($required == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',slug,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'slug' => 'required|max:50|unique:permissions' . $unique,
			//$input.'model' => 'required',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
		$permissions = Permission::all();

        return response()->success(compact('permissions'));
    }

	/**
	 * Show the form for creating a new resource.4
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$permission = Permission::create([
            'name' => $formData['name'],
            'slug' => str_slug($formData['slug'], '.'),
            'description' => $formData['description'],
        ]);

        return response()->success(compact('permission'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$permission = Permission::find($id);

        return response()->success($permission);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$formData = $request->input('data');
		
		$rules = $this->getRules(true, $id);
		
        $this->validate($request, $rules);
		
        $formData['slug'] = str_slug($formData['slug'], '.');
		
        Permission::where('id', '=', intval($id))->update($formData);

        return response()->success($formData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Permission::destroy($id);

        return response()->success('success');
    }
}
