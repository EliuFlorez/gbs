<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Bican\Roles\Models\Permission;
use Bican\Roles\Models\Role;
use App\User;
use Auth;
use JWTAuth;
use Validator;
use Input;
use Hash;
use Mail;

class UsersController extends Controller
{
	/**
     * Validation rules.
     *
     */
    protected function getRules($required = true, $unique = null)
    {
		$input = '';
		if ($required == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',email,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|min:3',
            $input.'email' => 'required|email|unique:users' . $unique,
		];
		
        return $rules;
    }
	
    /**
     * Get all users.
     *
     * @return JSON
     */
    public function index()
    {
        $users = User::all();

        return response()->success(compact('users'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		//
	}
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		$rules['password'] = 'required|min:8|confirmed';
		$rules['password_confirmation'] = 'required|min:8';
		
        $this->validate($request, $rules);
		
		$formData = $request->all();

        $verificationCode = str_random(40);
		
        $user = new User();
        $user->name = trim($formData['name']);
        $user->email = trim(strtolower($formData['email']));
        $user->password = bcrypt($formData['password']);
        $user->email_verification_code = $verificationCode;
        $user->save();

		$user->detachAllRoles();

        foreach ($formData['role'] as $setRole) {
            $user->attachRole($setRole);
        }
		
        $token = JWTAuth::fromUser($user);
		
        Mail::send('emails.userverification', ['verificationCode' => $verificationCode], function ($m) use ($request) {
            $m->to($formData['email'], 'test')->subject('Email Confirmation');
        });

        return response()->success(compact('user', 'token'));
    }
	
    /**
     * Get user details referenced by id.
     *
     * @param int User ID
     *
     * @return JSON
     */
    public function show($id)
    {
        $user = User::find($id);
		
        $user['role'] = $user->roles()
			->select(['slug', 'roles.id', 'roles.name'])
			->get();

        return response()->success($user);
    }

    /**
     * Update user data.
     *
     * @return JSON success message
     */
    public function update(Request $request, $id)
    {
        $formData = $request->input('data');
		
		$rules = $this->getRules(true, $id);
		
		if (!empty($formData['password']) || !empty($formData['password_confirmation'])) {
			$rules['data.password'] = 'required|min:8|confirmed';
			$rules['data.password_confirmation'] = 'required|min:8';
		}
		
        $this->validate($request, $rules);

        $userData = [
            'name' => $formData['name'],
            'email' => $formData['email'],
			'password' => bcrypt($formData['password'])
        ];

        User::where('id', '=', intval($id))->update($userData);

        $user->detachAllRoles();

        foreach ($formData['role'] as $setRole) {
            $user->attachRole($setRole);
        }

        return response()->success('success');
    }

    /**
     * Delete User Data.
     *
     * @return JSON success message
     */
    public function destroy($id)
    {
        User::destroy($id);
		
        return response()->success('success');
    }
}
