<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\Service\Doctor;
use Illuminate\Http\Request;

use Auth;
use Validator;

class DoctorsController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($bool = false, $unique = null)
    {
		$input = '';
		if ($bool == true) {
			$input = 'data.';
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'hospital_id' => 'required|integer',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$doctors = Doctor::all();

        return response()->success(compact('doctors'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$doctor = Doctor::create([
            'name' => $formData['name'],
            'hospital_id' => $formData['hospital_id'],
        ]);

        return response()->success(compact('doctor'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$doctor = Doctor::find($id);

        return response()->success($doctor);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Doctor::where('id', '=', intval($id))->update([
            'name' => $formData['name'],
			'hospital_id' => $formData['hospital_id'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Doctor::destroy($id);

        return response()->success('success');
    }
}
