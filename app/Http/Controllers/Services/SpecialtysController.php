<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\Service\Specialty;
use Illuminate\Http\Request;

use Auth;
use Validator;

class SpecialtysController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($bool = false, $unique = null)
    {
		$input = '';
		if ($bool == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',name,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'display_name' => 'required|max:50',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$specialtys = Specialty::all();

        return response()->success(compact('specialtys'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules();
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$specialty = Specialty::create([
            'name' => $formData['name'],
            'display_name' => $formData['display_name'],
        ]);

        return response()->success(compact('specialty'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$specialty = Specialty::find($id);

        return response()->success($specialty);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Specialty::where('id', '=', intval($id))->update([
            'name' => $formData['name'],
            'display_name' => $formData['display_name'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Specialty::destroy($id);

        return response()->success('success');
    }
}
