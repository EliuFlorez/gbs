<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\Service\Hospital;
use Illuminate\Http\Request;

use Auth;
use Validator;

class HospitalsController extends Controller 
{
    /**
     * Validation rules.
     *
     */
    protected function getRules($bool = false, $unique = null)
    {
		$input = '';
		if ($bool == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',name,'.$unique;
		}
		
		$rules = [
			$input.'name' => 'required|max:50',
			$input.'country_id' => 'required|integer',
			$input.'province_id' => 'required|integer',
			$input.'city_id' => 'required|integer',
		];
		
        return $rules;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$hospitals = Hospital::all();

        return response()->success(compact('hospitals'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$rules = $this->getRules(false);
		
        $this->validate($request, $rules);
		
		$formData = $request->all();
		
		$doctor = Hospital::create([
            'name' => $formData['name'],
            'country_id' => $formData['country_id'],
            'province_id' => $formData['province_id'],
            'city_id' => $formData['city_id'],
            'address' => $formData['address'],
        ]);

        return response()->success(compact('hospital'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$hospital = Hospital::find($id);
		/*
		$hospital['specialtys'] = $hospital->specialtys()
			->select([
				'specialtys.name', 
				'specialtys.id'
			])
			->get();
		*/
        return response()->success($hospital);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Hospital::where('id', '=', intval($id))->update([
            'name' => $formData['name'],
            'country_id' => $formData['country_id'],
            'province_id' => $formData['province_id'],
            'city_id' => $formData['city_id'],
            'address' => $formData['address'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Hospital::destroy($id);

        return response()->success('success');
    }
}
