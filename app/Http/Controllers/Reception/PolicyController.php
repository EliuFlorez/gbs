<?php

namespace App\Http\Controllers\Reception;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Plan\Plan;
use App\Moduls\Policy\Policy;
use App\Moduls\Policy\PolicyAffiliate;
use App\Models\Affiliate\Affiliate;
use App\ProcessEntry;

use Auth;
use Validator;

//$process = ProcessEntry::findProcess(1);
//$procedure = new ProcedureEntry('name_procedure');
//$process->start(null, $policy->id);
//$process->finish();
/*ALTER TABLE `policy` CHANGE `agent_id` `seller_id` INT(11) UNSIGNED NOT NULL;*/
/*ALTER TABLE `policy` DROP INDEX `policy_agent_id_foreign`, ADD INDEX `policy_seller_id_foreign` (`seller_id`) USING BTREE;*/

class PolicyController extends Controller 
{
	use \App\UploadAndDownloadFile;
	
    /**
     * Validation rules.
     *
     */
    protected function getRules($bool = false, $unique = null)
    {
		$input = '';
		if ($bool == true) {
			$input = 'data.';
		}
		
		if (!empty($unique)) {
			$unique = ',email,'.$unique;
		}
		
		$rules = [
			// Required - Policy
			$input.'seller_id' 		=> 'required',
			$input.'plan_type_id' 	=> 'required',
			
			// Required - Affiliate
			$input.'first_name' => 'required|max:50',
			$input.'last_name' 	=> 'required|max:50',
            //$input.'address' 	=> 'required|max:200',
            $input.'phone' 		=> 'required|max:50',
            $input.'mobile' 	=> 'required|max:50',
            $input.'email' 		=> 'required|email|unique:affiliate' . $unique,
            //$input.'country_id' => 'required',
            //$input.'state_id' 	=> 'required',
            //$input.'city_id' 	=> 'required',
		];
		
        return $rules;
    }
	
	/*
	if( isset($request['deleteFile']) ){
		//delete the file send in the request
		$resp = $this->deteleFileProcess($request, $procedure, $process_ID);
		$this->novaMessage->setData($resp);
		$this->novaMessage->addSuccesMessage('SUCCESS', 'file deleted');
		return $this->returnJSONMessage();
	}else if( isset($request['file']) ){
		$resp = $this->processFiles($request, $procedure, $process_ID);
		$this->novaMessage->setData($resp);
		$this->novaMessage->addSuccesMessage('SUCCESS', 'file uploaded');
		return $this->returnJSONMessage();
	}
	*/
	
	private function processFiles(Request $request, $procedure, $process_ID)
	{
		\DB::beginTransaction();
		try {
			$category = $request->input('category', false);
			$description = $request->input('description', '');
			$ts = $request->input('ts', false);
			
			$table_type = 'procedure_entry';
			$old_fid = $request->input('fid', false);

			if ( empty($ts) || empty($category) ) {
				throw new \Exception('some data missing');
			}

			$params = array();
			$params['fieldname'] = 'file';
			$params['subfolder'] = 'newPolicy/'.$procedure->id;
			$params['table_type'] = $table_type;
			$params['table_id'] = $procedure->id;
			$params['data'] = json_encode(array(
				'ts' => $ts,
				'procedure_document_id' => $category,
				'process_id' => $process_ID
			));
			$params['multiple'] = false;

			if ( $old_fid ) {
				//if get file id try to update
				$updated = $this->updateFile($request, $old_fid, $params);
				$uploadedFiles = array($updated);
			}
			else {
				$uploadedFiles = $this->uploadFiles($request, $params);
			}

			\DB::commit();
			
			return [
				"ts" => $ts, 
				"id" => $uploadedFiles[0]
			];
		}
		catch( \Exception $e ) {
			\DB::rollback();
			throw $e;
		}
	}
	
	private function deteleFile(Request $request, $procedure, $process_ID)
	{
		\DB::beginTransaction();
    	try {
    		$input = $request->all();

	    	if (!isset($input["fid"])) {
	    		throw new \Exception("Petición es inválida");
	    	}

	    	$fid = $input["fid"];
	    	$fe = FileEntry::find($fid);
			
	    	if (!($fe->table_type == "procedure_entry" && $fe->table_id == $procedure->id))
			{
	    		throw new \Exception("Archivo es inválido");
	    	}

	    	$this->deleteFile($fe->id);

			\DB::commit();
    	}
		catch( \Exception $e ) {
    		\DB::rollback();
			throw $e;
    	}
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$policies = Policy::all();

        return response()->success(compact('policies'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
		//
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		\DB::beginTransaction();
		try {
			$rules = $this->getRules(false);
		
			$this->validate($request, $rules);
			
			$formData = $request->all();
			
			$process = new ProcessEntry([], 'PolicyInitial');
			
			if ($process == null) {
				throw new \Exception("Process with does id does not exist", 1);  
			}

			if ($process->state == 'finished' || $process->state == 'cancelled' || $process->state == 'rellocated') {
				throw new \Exception("Process is already complete", 1); 
			}

			$data = $request->all();
			
			$policy = Policy::create([
				'seller_id'	=> $data['seller_id'],
				'plan_type_id' => $data['plan_type_id'],
				'plan_deducible_id' => 1,
				'payment_number_id' => 1,
				'state' => 'inprocess',
			]);
			
			if (!empty($policy)) 
			{
				$affiliate = Affiliate::create([
					'first_name' => $data['first_name'],
					'last_name' => $data['last_name'],
					'policy_id' => $policy->id,
					'role'  => 1,
				]);
				
				$policyAffiliate = PolicyAffiliate::create([
					'affiliate_id' => $affiliate->id,
					'policy_id' => $policy->id,
					'role' => 1,
				]);
			}
			
			$process->start(null, $policy->id);
			
			\DB::commit();
			
			return response()->success('success');
		} 
		catch(\Exception $e) {
			\DB::rollback();
			return response()->error('NOT FOUND' . $e->getMessage());
		}
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$policy = Policy::find($id);

        return response()->success($policy);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return JSON Data
	 */
	public function edit($id)
	{
		//
	}
	
    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
		$rules = $this->getRules(true);
		
        $this->validate($request, $rules);
		
		$formData = $request->input('data');
		
        Policy::where('id', '=', intval($formData['id']))->update([
            'agent_id'	=> $data['agent_id'],
			'plan_type'	=> $data['plan_type'],
        ]);
			
        return response()->success('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Policy::destroy($id);

        return response()->success('success');
    }
}
