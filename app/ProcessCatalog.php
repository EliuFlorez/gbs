<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessCatalog extends Model
{
	protected $table = 'process_catalog';

	protected $fillable = [
		'name',
		'role_id',
		'procedure_catalog_id',
		'number_next',
		'number_last',
		'icon',
		'link',
	];

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
		'deleted_at'
	];

	public function procedureCatalog()
	{
		return $this->belongsTo('App\ProcedureCatalog', 'procedure_catalog_id', 'id');
	}

	public function processEntry()
	{
		return $this->hasMany('App\ProcessEntry', 'process_entry_id', 'id');
	}

	public function department()
	{
		return $this->belongsTo('Bican\Roles\Models\Role', 'role_id', 'id');
	}

	public function getNextProcesses()
	{
		$processes = ProcessCatalog::where('id', 1)->get();

		return $processes;
	}

}
