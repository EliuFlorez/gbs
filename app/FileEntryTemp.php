<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileEntryTemp extends Model
{
	protected $table = 'file_entry_temp';

	protected $fillable = [
		'file_name',
		'file_mime',
		'file_original',
		'file_path',
		'driver',
		'status',
		'table_type',
		'table_id',
		'data',
		'description'
	];
}
