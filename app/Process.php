<?php

namespace App;

interface Process
{
	public function finish();
	public function start();
	public function cancel();
}