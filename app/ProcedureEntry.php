<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ProcessCatalog;
use Carbon\Carbon;

class ProcedureEntry extends Model
{
	protected $table = 'procedure_entry';

	protected $fillable = [
		'procedure_catalog_id',
		'start_date',
		'end_date',
		'state',
		'user_id',
		'policy_id'
	];

    use SoftDeletes;

    protected $dates = [
		'deleted_at'
	];

    private $procedureCatalogName = '';

    function __construct($procedureCatalogName = '', array $attributes = array())
	{
    	$this->procedureCatalogName = $procedureCatalogName;
		
    	parent::__construct($attributes);
    }

	public function procedureCatalog()
	{
		return $this->belongsTo('App\ProcedureCatalog', 'procedure_catalog_id', 'id');
	}

	public function processEntry()
	{
		return $this->hasMany('App\ProcessEntry', 'procedure_entry_id', 'id');
	}

	public function policy()
	{
		return $this->belongsTo('App\Moduls\Policy\Policy', 'policy_id', 'id');
	}

	public function responsible()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function emergency()
	{
		//return $this->hasMany('App\Moduls\Emergency', 'procedure_entry_id', 'id');
	}

	public function hospitalization()
	{
		//return $this->hasMany('App\Moduls\Hospitalization', 'procedure_entry_id', 'id');
	}

	public function claims()
	{
        /*
		return $this->belongsToMany('App\Moduls\Claim',
        	'claim_procedure',
            'procedure_entry_id',
            'claim_id'
		);
		*/
    }

    public function procedureCancellation()
	{
    	return $this->hasOne('App\ProcedureCancellation', 'procedure_entry_id');
    }

	public function pendiente($query)
	{
		return $query->where('state', '!=', 'finished')
			->where('state', '!=', 'cancelled');
	}

    public function scopePending($query)
    {
        return $query->where('state', '!=', 'finished')
			->where('state', '!=', 'cancelled');
    }

	public function start($policy_id = null)
	{
		$carbon = new \Carbon\Carbon();
		$date = $carbon->now();

		$procedureCatalog = ProcedureCatalog::where('name', $this->procedureCatalogName)->firstOrFail();

		if (!empty($procedureCatalog)) {
			$this->procedure_catalog_id = $procedureCatalog->id;
			$this->policy_id = $policy_id;
			$this->start_date = $date;
			$this->state = 'inprocess';
			$this->save();
		} else {
			throw new \Exception('Procedure does not Exist', 15);
		}
	}

	public function finish()
	{
		$carbon = new \Carbon\Carbon();
		$date = $carbon->now();

		$this->end_date = $date;
		$this->state = 'finished';
		$this->save();
	}

	public function cancel($reason, $responsible)
	{
		if( ! $this->isActive() ){
			throw new \Exception('El trámite no está activo');
		}

		switch ($this->procedureCatalog->name) {
			case 'policy':
				$this->cancelPolicy();
				break;
			case 'claim':
				$this->cancelClaim();
				break;
			case 'settlement':
				$this->cancelSettlement();
				break;
			case 'renewal':
				$this->cancelRenewal();
				break;
			case 'clientservice':
				$this->cancelService();
				break;
			default:
				# code...
				break;
		}

		$carbon = new \Carbon\Carbon();
		$date = $carbon->now();

		\App\ProcedureCancellation::create([
			'reason'             => $reason,
			'responsible_id'     => $responsible,
			'procedure_entry_id' => $this->id
		]);

		foreach ($this->getCurrentProcesses() as $process) {
			$process->cancel();
		}

		$this->end_date = $date;
		$this->state = 'cancelled';
		$this->save();
	}

	private function cancelPolicy()
	{
		/*
		| delete affiliate_policy
		| delste affiliate_policy_deducible
		| policy_cost                |	
		| policy_cost_detail         |
		| delete cheque_payment_detail, credit_card_payment_detail, deposit_payment_detail,
		| policy_cost_tax_fees       |
		| policy_deducible           |
		| policy_discount            |
		| delete customer (solo si el customer no esta asociado a ninguna poliza)
		| delete affiliate
		| delte policy               |
		*/
	}

	private function cancelClaim()
	{
		/*
		| claim                      |
		| claim_file                 |
		| claim_procedure            |
		| ticket , relate to the claim |
		*/
	}

	private function cancelSettlement()
	{
		/*
		| claim_settlement           |
		| claim_settlement_refund    |
		*/
	}

	private function cancelRenewal()
	{
		//
	}	

	private function cancelService()
	{
		//
	}

	public function getCurrentProcesses()
	{
		$model = ProcessEntry::where('procedure_entry_id', $this->id)
			->where('process_entry.state', '!=', 'finished')
			->where('process_entry.state', '!=', 'cancelled')
			->get();
			
		return $model;
	}

	public function getLastActiveProcess()
	{
		$model = ProcessEntry::where('procedure_entry_id', $this->id)
			->where('process_entry.state', '!=', 'finished')
			->where('process_entry.state', '!=', 'cancelled')
			->orderBy('id', 'desc')
			->first();
			
		return $model;
	}

	public function isActive()
	{
		return ( ($this->state != 'finished') && ($this->state != 'cancelled') );
	}

}
