<?php

namespace App;

interface Procedure
{
	public function start();
	public function finish();
	public function cancell();
}
