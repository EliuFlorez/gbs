<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ProcessCatalog;
use App\ProcedureCatalog;
use App\ProcedureEntry;
use Bican\Roles\Models\Role;
use App\User;

class ProcessEntry extends Model
{
	protected $table = 'process_entry';

	protected $fillable = [
		'process_catalog_id',
		'procedure_entry_id',
		'start_date',
		'end_date',
		'state',
		'user_id'
	];

    use SoftDeletes;

    protected $dates = [
		'deleted_at'
	];

    private $processCatalog;

    private $procedureEntry;

    function __construct(array $attributes = array(), $processCatalogName = '')
	{
        parent::__construct($attributes);
		
        if ($processCatalogName != '') {
            $this->processCatalog = ProcessCatalog::where('name', $processCatalogName)->firstOrFail();

            if ($this->isFirstProcess()) {
                $procedureCatalogName = $this->processCatalog->procedureCatalog->name;
                $this->procedureEntry = new ProcedureEntry($procedureCatalogName);
            }
        }
    }

    public static function findProcess($id, $columns = array('*'))
    {
        $result = parent::find($id, $columns);
		
        if ($result != null) {
            $result->processCatalog = ProcessCatalog::findOrFail($result->process_catalog_id);
        }
        
		return $result;
    }


    public function setProcedureEntry($procedureEntry) 
	{
        $this->procedureEntry = $procedureEntry;
    }

    public function processCatalogRel() 
	{
        return $this->belongsTo('App\ProcessCatalog', 'process_catalog_id', 'id');
    }

    public function procedureEntryRel() 
	{
		return $this->belongsTo('App\ProcedureEntry', 'procedure_entry_id', 'id');
    }

    public function poliza()
	{
    	return $this->belongsToManyThrough('App\Moduls\Policy\Policy',
			'App\ProcedureEntry',
			'procedure_entry_id',
			'policy_id'
		);
    }

    public function responsible()
	{
		return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeState($query, $state)
	{
    	return $query->where('state', $state);
    }

    public function scopeResponsible($query, $id)
	{
    	return $query->where('user_id', $id);
    }

    public function getStatesProcess()
	{
    	return [
			'inprocess'   => 'En Proceso',
			'finished'    => 'Terminado',
			'ticket'      => 'ConTicket',
			'realocated'  => 'Reasignado',
			'cancelled'   => 'Cancelado'
		];
    }

    public function getProcessCatalog()
	{
        return $this->processCatalog;
    }

    public function isFirstProcess()
    {
        if ($this->processCatalog->number_next == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isLastProcess()
    {
        if ($this->processCatalog->number_last == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function start($procedureEntry = null, $policy_id = null)
    {
        if ($this->isFirstProcess()) {
            $this->procedureEntry->start($policy_id);
        } else {
            if(isset($procedureEntry)){
                $p = ProcedureEntry::find($procedureEntry->id)
					->where('state', '!=', 'finished')
					->where('state', '!=', 'cancelled')
					->select('id')
					->get();
                if (isset($p)) {
                    $this->setProcedureEntry($procedureEntry);
                } else {
                    throw new \Exception('Procedure does not Exist', 15);
                }
            } else {
                throw new \Exception('Procedure does not Exist', 15);
            }
        }

        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
		
		$this->user_id 			  = 1;
		$this->procedure_entry_id = $this->procedureEntry->id;
		$this->process_catalog_id = $this->processCatalog->id;
		$this->start_date         = $date;
		$this->state              = 'inprocess';
		$this->save();
		
		return $this->procedureEntry->id;
    }

    public function finish()
	{
        if ($this->state != 'finished') {
            $carbon = new \Carbon\Carbon();
            $date = $carbon->now();

            $this->end_date  = $date;
            $this->state     = 'finished';
            $this->save();

            if (is_null($this->procedureEntry) || $this->procedureEntry->id == null) {
                $this->procedureEntry = $this->procedureEntryRel;
            }
			
            if (is_null($this->processCatalog) || $this->processCatalog->id == null) {
                $this->processCatalog = $this->processCatalogRel;
            }

            if ($this->isLastProcess()) {
                $this->procedureEntry->finish();
            } else {
                $nextProcessesCat = $this->processCatalog->getNextProcesses();
				foreach ($nextProcessesCat as $key => $processCat) {
					//$this->start($this->procedureEntry);
                }
            }
        }
    }

    public function cancel($comment = '')
	{
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();

        $this->end_date = $date;
        $this->state 	= 'cancelled';
        $this->update();
    }

	public function isFinished()
	{
		return $this->state == 'finished';
	}

	public function isActive()
	{
		return ( $this->state != 'finished' && $this->state != 'cancelled' );
	}

}
