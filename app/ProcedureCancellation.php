<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcedureCancellation extends Model
{
	protected $table = 'procedure_cancellation';

	protected $fillable = [
		'user_id',
		'reason',
		'procedure_entry_id'
	];
	
	use SoftDeletes;

	public function procedure()
	{
		return $this->belongsTo('App\ProcedureEntry', 'procedure_entry_id');
	}

	public function responsible()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
}
