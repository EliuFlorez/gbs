<?php

namespace App\Moduls\Policy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PolicyAffiliate extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'policy_affiliate';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'id',
		'affiliate_id',
		'policy_id',
		'effective_date',
		'dismiss_date',
		'premium_amount',
		'role',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = [
		'created_at', 
		'updated_at'
	];
}
