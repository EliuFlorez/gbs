<?php 

namespace App\Moduls\Policy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Moduls\Plan\Plan;

use Carbon\Carbon;

class Policy extends Model
{
	protected $table = 'policy';

	protected $fillable = [
		'policy_number',
		'plan_deducible_id',
		'plan_type_id',
		'payment_number_id',
		'seller_id',
		'emision_number',
		'endoso_number',
		'renewal_number',
		'start_date',
		'end_date',
		'endoso_date',
		'emision_date',
		'plan_type',
	];

	//to enable soft delete in the model
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
		'deleted_at'
	];

	/*
    public function planDeducible()
	{
        return $this->belongsTo('App\Moduls\Plan\PlanDeducible', 'plan_deducible_id', 'id');
    }

	public function affiliates()
	{
        return $this->hasMany('App\Moduls\Affiliate\Affiliate', 'policy_id')
			->whereNull("dismiss_date");
    }

	public function policyDeducible()
	{
        return $this->hasMany('App\Moduls\Policy\PolicyDeducible', 'policy_id');
    }

	public function planType()
	{
        return $this->belongsTo('App\Moduls\Plan\PlanType', 'plan_type_id', 'id');
    }

    public function getPlan()
	{
        return Plan::find($this->planDeducible->plan_id);
    }

    public function PaymentNumber()
	{
    	return $this->belongsTo("App\Moduls\Payment\PaymentNumber", "payment_number_id");
    }

    public function procedureEntry()
	{
        return $this->hasOne('App\ProcedureEntry', 'policy_id', 'id');
    }

    public function agent()
	{
        return $this->belongsTo('App\Moduls\Agent', 'agent_id', 'id');
    }

    public function policyCost()
	{
    	//return $this->hasMany("Modules\Payment\Entities\PolicyCost", "policy_id");
    }

    public function ticket()
	{
		return $this->hasMany("App\Moduls\Ticket\Ticket", "policy_id", "id");
    }

    public function discounts()
	{
		return $this->hasMany("App\Moduls\Policy\PolicyDiscount", "policy_id");
    }
	*/
}
