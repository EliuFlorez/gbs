<?php 

namespace App\Moduls\Policy;

use App\Moduls\Policy\Policy;
use App\Moduls\Policy\PolicyDiscount;
use App\Moduls\Policy\QuoteCode;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PolicyCalculator 
{
	protected $policy;

	function __construct( Policy $policy ){
		$this->policy = $policy;
	}
	
	function getAgeRange($date1, $date2) 
	{
		$date1 = date('Y-m-d', strtotime($date1));
		$date2 = date('Y-m-d', strtotime($date2));
		 
		if (count(explode("-", $date1)) != 3) {
			$date1 = "0000-00-00";
		}

		$nDate1 = explode("-", $date1);
		$nYear = intval($nDate1[0]);
		$nMon  = intval($nDate1[1]);
		$nDay  = intval($nDate1[2]);

		$Year  = intval(date('Y', strtotime($date2)));
		$Mon   = intval(date('m', strtotime($date2)));
		$Day   = intval(date('d', strtotime($date2)));

		$rMon  = 0;
		$rYear = 0;

		if ($Day > $nDay) {
			$rMon = 1;
		}

		if ($Mon > $nMon) {
			$rYear = 1;
		} elseif ($Mon == $nMon) {
			if ($rMon == 1) {
				$rYear = 1;
			}
		}

		if ($Day == $nDay && $Mon == $nMon) {
			$rYear = 1;
		}

		$age = abs($Year - $nYear + $rYear - 1);
		
		return $age;
	}
	
	/*
	Prima neta: es el valor que se obtiene con la cotización osea es el valor que se obiente de las tabla de precios
	Admin Fee: es siempre $75 dolares
	IVA: es el 14% del admin fee osea 10.50
	TAx(SSC)= es el 5% de la prima neta
	Prima Neta + Admin Fee + Iva + Tax
	*/
	public function getCalculate()
	{
		$valueAge = 18;
		
		$policy_id = $this->policy->id;
		$plan_deducible_id = $this->policy->plan_deducible_id;
		$plan_type_id = $this->policy->plan_type_id;
		$payment_number_id = $this->policy->payment_number_id;
		$start_date = $this->policy->start_date;
		
		$dataPayment = DB::table('payment_number')
			->where('id', $payment_number_id)
			->first();
		
		$quotePolicy = 1;
		$typePolicy = 'ANUAL';
		
		if (!empty($dataPayment)) {
			$quotePolicy = $dataPayment->number;
		}
		
		/*
		Anual = 300
		Semestral = 159 Incluido el 6%
		Trimestral = 82.50 incluido el 10%
		Mensual = 27.50 incluido el 10%
		*/
		$feePolicy = 1;
		$valueRider = 300;
		$statuRider = false;
		if ($quotePolicy == 2) {
			$feePolicy = 6;
			$valueRider = 150;
		} else if ($quotePolicy == 4) {
			$feePolicy = 10;
			$valueRider = 75;
		} else if ($quotePolicy == 12) {
			$feePolicy = 10;
			$valueRider = 25;
		}
		
		$affiliates = $this->policy->affiliates;
		
		$total = 0;
		$valueFee = 0;
		
		$array = [];
		
		if (!empty($affiliates)) 
		{
			$affTitle = true;
			$plan = $this->policy->planDeducible->plan;
			$plan_id = $plan->id;
			
			$starttime = strtotime($start_date);
			$endtime = strtotime(date('Y').'-04-01');
			
			if ($starttime < $endtime) {
				$dueYear = date('Y') - 1;
			} else {
				$dueYear = date('Y');
			}
			
			$countChild = 0;
			
			$affiliateCount = count((array) $affiliates);
			
			foreach ($affiliates as $affiliate)
			{
				$valueAge = $this->getAgeRange($affiliate->brithday, $start_date);
				
				$valueChild = false;
				
				if ($valueAge <= 24 && $affiliate->role == 3) {
					$valueChild = true;
				} 
				
				if ($valueAge <= 24 && $affiliate->role == 1) {
					$valueChild = false;
					$valueAge = 18;
				}
				
				if ($statuRider == false && $affiliate->rider_statu == 1) {
					$statuRider = true;
				}
				
				$minAge = 0;
				$maxAge = 0;
				
				if ($valueAge >= 18 && $valueAge <= 24) {
					$minAge = 18;
					$maxAge = 24;
				} else if ($valueAge >= 25 && $valueAge <= 29) {
					$minAge = 25;
					$maxAge = 29;
				} else if ($valueAge >= 30 && $valueAge <= 34) {
					$minAge = 30;
					$maxAge = 34;
				} else if ($valueAge >= 35 && $valueAge <= 39) {
					$minAge = 35;
					$maxAge = 39;
				} else if ($valueAge >= 40 && $valueAge <= 44) {
					$minAge = 40;
					$maxAge = 44;
				} else if ($valueAge >= 45 && $valueAge <= 49) {
					$minAge = 45;
					$maxAge = 49;
				} else if ($valueAge >= 50 && $valueAge <= 54) {
					$minAge = 50;
					$maxAge = 54;
				} else if ($valueAge >= 55 && $valueAge <= 59) {
					$minAge = 55;
					$maxAge = 59;
				} else if ($valueAge >= 60 && $valueAge <= 64) {
					$minAge = 60;
					$maxAge = 64;
				} else if ($valueAge >= 65 && $valueAge <= 69) {
					$minAge = 65;
					$maxAge = 69;
				} else if ($valueAge >= 70 && $valueAge <= 74) {
					$minAge = 70;
					$maxAge = 74;
				} else if ($valueAge >= 75 && $valueAge <= 79) {
					$minAge = 75;
					$maxAge = 79;
				} else if ($valueAge >= 80 && $valueAge <= 150) {
					$minAge = 80;
					$maxAge = 150;
				}
				
				if ($valueChild == true) {
					++$countChild;
				} 
				else {
					$policyValue = DB::table('references_times')
						->where('plan_id', $plan_id)
						->where('deductible_id', $plan_deducible_id)
						->where('min_age', '=', $minAge)
						->where('max_age', '=', $maxAge)
						->where('display_time', $typePolicy)
						->where('due_year', $dueYear)
						->first();
					
					if (empty($policyValue)) 
					{
						$policyValue = DB::table('references_times')
							->where('plan_id', $plan_id)
							->where('deductible_id', $plan_deducible_id)
							->where('min_age', '>=', $minAge)
							->where('max_age', '<=', $maxAge)
							->where('display_time', $typePolicy)
							->where('due_year', $dueYear)
							->first();
						
						// Error al obtener valor de los precios de las póliza
						if (empty($policyValue)) 
						{
							return array();
						}
					}
					
					if ($affTitle == true) {
						$name = 'P/H Premium';
					} 
					else {
						$name = 'Spouse Premium';
					}
					
					$affTitle = false;
					
					$amount = 0;
					
					if (!empty($policyValue)) {
						$amount = $policyValue->price / $quotePolicy;
					}
					
					$total = $total + $amount;
					
					$array[] = [
						'name' => $name,
						'amount' => $amount,
						'commissionable' => 1,
						'is_discount' => 0
					];
				}
			}
			
			// Childs +3
			if ($countChild > 0) 
			{
				$name = $countChild . ' Child';
				
				if ($countChild >= 3) {
					$countChild = 3;
					$name = '+3 Childs';
				}
				
				$policyValue = DB::table('references_childs')
					->where('childs', '=', $countChild)
					->where('plan_id', $plan_id)
					->where('deductible_id', $plan_deducible_id)
					->where('display_time', $typePolicy)
					->where('due_year', $dueYear)
					->groupBy('childs')
					->first();
					
				$amount = 0;
				
				if (!empty($policyValue)) {
					$amount = $policyValue->price / $quotePolicy;
				}
				
				$total = $total + $amount;
				
				$array[] = [
					'name' => $name,
					'amount' => $amount,
					'commissionable' => 1,
					'is_discount' => 0
				];
			}
			
			// Prima Neta - Before
			$totalDiscount = $total;
			
			// Discount Value
			$policyDiscount = PolicyDiscount::where('policy_id', '=', $policy_id)->first();
			if (!empty($policyDiscount)) {
				if (!empty($policyDiscount->percentage) && $policyDiscount->state == 1) {
					$valuePercentage = ($totalDiscount * $policyDiscount->percentage) / 100;
					$array[] = [
						'name' => 'Descuento',
						'amount' => $valuePercentage,
						'commissionable' => 0,
						'is_discount' => 1
					];
				}
			}
			
			// Install Fee
			if ($valueFee == 0 && $feePolicy > 1) {
				$valueFee = ($total * $feePolicy) / 100;
				$total = $total + $valueFee;
				$array[] = [
					'name' => 'Installment Fee',
					'amount' => $valueFee,
					'commissionable' => 1,
					'is_discount' => 0
				];
			}
			
			// Applique Rider
			if ($statuRider == true) 
			{
				// Transplate Rider
				if ($valueRider > 0 && $plan_id == 10) {
					$total = $total + $valueRider;
					$array[] = [
						'name' => 'Transplant Rider',
						'amount' => $valueRider,
						'commissionable' => 1,
						'is_discount' => 0
					];
				}
				
				// Transplate Rider & Install Fee
				if ($valueRider > 0 && $plan_id == 10 && $feePolicy > 1) {
					$valueRiderFee = ($valueRider * $feePolicy) / 100;
					$total = $total + $valueRiderFee;
					$array[] = [
						'name' => 'Installment Fee',
						'amount' => $valueRiderFee,
						'commissionable' => 1,
						'is_discount' => 0
					];
				}
			}
		}
		
		/*
		$value = 100;
		$adminFee = 75;
		$taxFee = ($value * Quote) / 100;
		$taxIVA = (75 * 12) / 100;
		$taxSSC = ($total * 0.5) / 100;
		*/
		$countQoute = 0;
		$arrayQoute = [];
		for ($i = 1; $i <= $quotePolicy; $i++) 
		{
			$items = $array;
			
			$countQoute++;
			
			if ($countQoute == 1) {
				$items[] = ['name' => 'Admin Fee', 'amount' => 75, 'commissionable' => 0, 'isdiscount' => 0];
				$items[] = ['name' => 'Tax (IVA)', 'amount' => (75 * 12) / 100, 'commissionable' => 0, 'isdiscount' => 0];
			}
			
			$items[] = ['name' => 'Tax (SSC)', 'amount' => ($total * 0.5) / 100, 'commissionable' => 0, 'isdiscount' => 0];
			
			$arrayQoute[] = [
				'total' => $total,
				'plan_id' => $plan_id,
				'items' => $items
			];
		}
		
		return $arrayQoute;
	}
	
}
