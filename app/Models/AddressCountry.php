<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressCountry extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'address_country';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'id',
		'name',
		'iso2',
		'iso3',
		'code'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = [
		'created_at', 
		'updated_at'
	];
}
