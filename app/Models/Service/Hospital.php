<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'hospital';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'id',
		'name',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = [
		'created_at', 
		'updated_at'
	];
	
	/**
     * Role belongs to many permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specialtys()
    {
        return $this->belongsToMany('App\Models\Specialty')->withTimestamps();
    }
}
