<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketCat;
use Validator;
use Carbon\Carbon;

class Ticket extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'ticket';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'table_type', 
		'table_id',
		'ticket_cat_id',
		'policy_id',
		'start_date',
		'end_date',
		'short_desc',
		'user_id'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = [
		'created_at', 
		'updated_at'
	];
	
	public function ticketCat() {
		return $this->belongsTo('App\Moduls\Ticket\TicketCat', 'ticket_cat_id', 'id');
	}

	public function policy() {
		return $this->belongsTo('App\Moduls\Policy\Policy', 'policy_id');
	}
	
	public function responsible() {
		return $this->belongsTo('App\User', 'user_id', 'id');
	}
	
	public function ticketDetail() {
		return $this->hasMany('App\Moduls\Ticket\TicketDetail', 'ticket_id');
	}

	public function emergency() {
		//return $this->hasOne('App\Moduls\Service\Emergency', 'ticket_id', 'id');
	}

	public function hospitalization() {
		//return $this->hasOne('App\Moduls\Service\Hospitalization', 'ticket_id', 'id');
	}
	
	public function validate(array $data)
	{
		$code  = null;
		$rules = array(
			'policy_id'  => 'required',
			'short_desc' => 'required',
		);

		$validator = Validator::make($data, $rules, array());
		
		if ($validator->fails()) {
			$code = 422;
			throw new \Exception('Falta ingresar datos', 422);
		}
	}

	public function news(array $data)
	{
		try {
			$this->validate($data);

			$policy = Policy::find($data['policy_id']);
			
			if ($policy == null) {
				$code = 422;
				throw new \Exception('La poliza no existe', 422);
			}

			$category = TicketCat::find($data['type_ticket']);
			
			if ($category == null) {
				$code = 422;
				throw new \Exception('La categoria no existe', 422);
			}

			$date = Carbon::now();

			$this->table_type		= $data['table_type'];
			$this->table_id			= (isset($data['table_id'])) ? $data['table_id'] : 0;
			$this->ticket_cat_id	= $data['type_ticket'];
			$this->policy_id		= $data['policy_id'];
			$this->start_date		= $date;
			$this->end_date			= null;
			$this->short_desc		= $data['short_desc'];
			$this->user_id			= 1;

			$this->save();
			$code = 200;
		}
		catch(\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());
		}
	}
}
