<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Ticket\Ticket;
use Validator;
use Carbon\Carbon;

class TicketDetail extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'ticket_detail';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'user_id', 
		'comment',
		'type',
		'ticket_id'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = [
		'created_at', 
		'updated_at'
	];
	
	public function ticket() {
		return $this->belongsTo('App\Moduls\Ticket\Ticket', 'ticket_id', 'id');
	}

	public function ticketAttach() {
		return $this->belongsToMany('App\FileEntry',
			'ticket_attach',
			'ticket_detail_id',
			'file_entry_id'
		);
	}

	public function user() {
		return $this->belongsTo('App\User',
			'user_id',
			'id'
		);
	}
	
	//SERVICES
	private function createCall(array $data)
	{
		try {
			$this->user_id 		= $data['user_id'];
			$this->comment 		= $data['comment'];
			$this->type 		= $data['type'];
			$this->ticket_id 	= $data['ticket_id'];
			$this->save();
		}
		catch(\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());	
		}
	}

	private function createEmail(array $data)
	{
		try {			
			$extraData = array();
			$extraData['To'] = explode(';', $data['email']);
			$extraData['Cc'] = explode(';', $data['copy']);
			$this->user_id 		= $data['user_id'];
			$this->comment 		= $data['comment'];
			$this->type 		= $data['type'];
			$this->ticket_id 	= $data['ticket_id'];
			$this->extra_data 	= json_encode($extraData);
			$this->save();
		}
		catch(\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());	
		}
	}

	private function createReplay(array $data)
	{
		try {
			$this->user_id 		= $data['user_id'];
			$this->comment 		= $data['comment'];
			$this->type 		= $data['type'];
			$this->ticket_id 	= $data['ticket_id'];
			$this->save();
		}
		catch(\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());
		}
	}

	public function news(array $data)
	{
		$code  = null;
		$rules = array(
			'type' => 'required',
		);
		
		$validator = Validator::make($data, $rules, array());
		
		if ($validator->fails()) {
			$code = 422;
			throw new \Exception('Falta ingresar tipo', 422);
		}

		if ($data['type'] == self::EMAIL) {
			$this->createEmail($data);
			// Email
		}

		if ($data['type'] == self::REPLAY) {
			$this->createReplay($data);
		} 

		if ($data['type'] == self::LLAMADA) {
			$this->createCall($data);
			// Upload
		}
	}

	public function getTypeDesc()
	{
		switch ($this->type) {
			case TicketDetail::LLAMADA:
				return 'Llamada';
				break;
			case TicketDetail::EMAIL:
				return 'Email';
				break;
			default:
				return $this->state;
				break;
		}
	}
}
