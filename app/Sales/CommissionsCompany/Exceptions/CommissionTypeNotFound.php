<?php

namespace App\Sales\CommissionsCompany\Exceptions;

class CommissionTypeNotFound extends CommissionException
{
}