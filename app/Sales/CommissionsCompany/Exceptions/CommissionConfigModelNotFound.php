<?php

namespace App\Sales\CommissionsCompany\Exceptions;

class CommissionConfigModelNotFound extends CommissionException
{
}