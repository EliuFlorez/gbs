<?php

namespace App\Sales\CommissionsCompany;

use App\Sales\Models\Sale;
use App\Sales\Models\CommissionType;
use App\Sales\Models\CompanyCommission;
use App\Sales\CommissionsCompany\Exceptions\ConfigRowNotFound;
use App\Sales\CommissionsCompany\Exceptions\CommissionTypeNotFound;
use App\Sales\CommissionsCompany\Exceptions\CommissionConfigModelNotFound;

/**
* Comision base
*/
abstract class Commission
{
	
	/**
	 * Venta de la cual se calculará la comisión
	 * @var App\Sales\Models\Sale
	 */
	protected $sale;
	
	/**
	 * Compañia al que se adjudica la comisión
	 * @var App\Sales\Models\Company
	 */
	protected $company;
	
	/**
	 * Vendedor al que se adjudica la comisión
	 * @var App\Sales\Models\Seller
	 */
	protected $seller;

	/**
	 * Porcentaje que se aplicará a la venta para obtener la comisión
	 * @var Decimal
	 */
	protected $percentage;

	/**
	 * Valor sobre el cual se aplicará el porcentaje de comisión
	 * @var Decimal
	 */
	protected $baseCommissionValue;

	/**
	 * Valor de la comisión.
	 * @var Decimal
	 */
	protected $value;

	/**
	 * Modelo que corresponde a la tabla de configuración de este tipo de comisión.
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $configModel;

	/**
	 * Modelo en donde se guardará la info de la comisión calculada
	 */
	protected $model;

	public function __construct(Sale $sale)
	{
		$this->sale = $sale;
		$this->company = $sale->company;
		$this->seller = $sale->seller;
		
		if(! class_exists($this->configModel)){
			throw new CommissionConfigModelNotFound("The class " . $this->configModel . " does not exists");
		}
		
		$this->configModel = new $this->configModel;

		$this->setUpValues();
	}

	/**
	 * Calcula la comisión correspondiente por la venta
	 * @return this
	 */
	public function calculate($percentage = null)
	{
		if (!empty($percentage)) {
			$this->percentage = $percentage;
		}
		
		$this->value = ($this->baseCommissionValue * $this->percentage) / 100;
		
		return $this;
	}

	/**
	 * Guarda la comisión y sus detalles en la base de datos
	 * @return this
	 */
	public function save()
	{
		$sale_id = $this->sale['id'];
		
		$companyCommission = $this->sale->commissionCompanies()->where('sale_id', '=', $sale_id)->first();
		
		if (!empty($companyCommission)) {
			$companyCommission['commission_type_id']	= $this->getCommissionTypeId();
			$companyCommission['company_id']			= $this->company->id;
			$companyCommission['seller_id']				= $this->seller->id;
			$companyCommission['percentage_applied']	= $this->percentage;
			$companyCommission['value']					= $this->value;
			$companyCommission->save();
		} else {
			$this->model = new CompanyCommission([
				'commission_type_id'	=> $this->getCommissionTypeId(),
				'company_id'			=> $this->company->id,
				'seller_id'				=> $this->seller->id,
				'percentage_applied'	=> $this->percentage,
				'value'					=> $this->value
			]);
			$this->sale->commissionCompanies()->save($this->model);
		}
		
		return $this;
	}

	/**
	 * Prepara los valores para el cálculo de la comisión
	 */
	public function setupValues()
	{
		$this->percentage = $this->getConfigPercentage();
		
		$commissionValue = $this->sale->baseCommissionValue;
		
		if ($commissionValue > 0) {
			$this->baseCommissionValue = $commissionValue;
		} else {
			$this->baseCommissionValue = $this->sale->total_amount;
		}
		
		return $this;
	}

	/**
	 * Obtiene el porcentage que debe ser aplicado para calcular la comisión
	 * @return decimal 
	 */
	abstract public function getConfigPercentage();

	/**
	 * Numero que corresponde al id del tipo de comision en la tabla
	 * sls_commission_types
	 * @return integer
	 */
	public function getCommissionTypeId()
	{
		$commissionName = $this->getCommissionName();
		$commissionType = CommissionType::findByName($commissionName)->first();
		
		if (! $commissionType) {
			throw new CommissionTypeNotFound("Commission Type '$commissionName' not found in commission_types table", 1);
			
		}

		return $commissionType->id;
	}

	/**
	 * Obtiene el nombre de la comisión basado en el nombre de la clase
	 * @return string
	 */
	public function getCommissionName()
	{
		return str_replace('Commission', '', collect(explode('\\', get_class($this)))->last());
	}

	/**
	 * Obtener datos de la comisión calculada
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function getData()
	{
		return $this->model;
	}

	/**
	 * Obtiene el nombre del modelo de configuración
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function getConfigModel()
	{
		return $this->configModel;
	}

	/**
	 * Arroja una excepción para cuando no se encuentra una fila de configuración
	 * @param  array  $fields Array con parametros que no se encontraron en la busqueda
	 * @return App\Sales\CommissionsCompany\Exceptions\ConfigRowNotFound         
	 */
	protected function throwNoConfigRow(array $fields=[])
	{
		$string = '';

		foreach ($fields as $field => $value) {
			$string .= $field . ':' . $value . ' ';
		}

		throw new ConfigRowNotFound('There\'s no configuration row for ' . ($string ? $string : ' this case'));
	}

	/**
	 * Magic method
	 * @param  string $name 
	 * @return mixed       
	 */
	public function __get($name)
	{
		if (property_exists($this, $name)) {
			return $this->$name;
		}

		return false;
	}
}