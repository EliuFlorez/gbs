<?php

namespace App\Sales\CommissionsCompany;

use App\Sales\Models\Sale;
use App\Sales\Models\CompanyCommission;
use App\Sales\Models\ConfigFixedCommission;

/**
* La comisión que recibe la empresa por la venta es siempre la misma.
*/
class FixedCommission extends Commission
{
	protected $configModel = ConfigFixedCommission::class;

	/**
	 * Obtiene el porcentage que debe ser aplicado para calcular la comisión
	 * @return decimal 
	 */
	public function getConfigPercentage()
	{
		$configRow = $this->configModel->where('company_id', $this->sale->company_id)->first();
		
		if (! $configRow) {
			$this->throwNoConfigRow([
				'company_id' 	=> $this->sale->company_id
			]);
		}

		return $configRow->percentage;
	}
}