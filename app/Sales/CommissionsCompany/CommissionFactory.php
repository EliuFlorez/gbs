<?php

namespace App\Sales\CommissionsCompany;

use App\Sales\Models\Sale;
use App\Sales\Models\Company;

class CommissionFactory
{

	protected $namespace = 'App\\Sales\\CommissionsCompany\\';

	public function make(Company $company, Sale $sale)
	{
		$commssionClass = $this->namespace . $company->commissionType->name . 'Commission';
		
		return app()->make($commssionClass, [$sale]);
	}
}