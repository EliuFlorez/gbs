<?php 

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

/**
* Modelo base para tablas de configuración
*/
abstract class BaseConfigModel extends Model
{
	protected $casts = ['percentage' => 'float'];
}