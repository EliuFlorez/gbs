<?php

namespace App\Sales\Models;

class ConfigFixedSellerCommission extends BaseConfigModel
{
	protected $table = 'sls_config_fixed_seller_commission';

	protected $fillable = [
		'company_id', 
		'seller_id', 
		'percentage',
		'percentage_rn',
	];

	protected $hidden = [
		'created_at', 
		'updated_at'
	];

	/**
	 * Buscar la configuración basado en vendedor / compañia
	 *
	 * @param      Query    $query
	 * @param      integer  $sellerId   El id del vendedor
	 * @param      integer  $companyId  El id de la compañía
	 *
	 */
	public function scopeFindConfigValues($query, $sellerId, $companyId)
	{
		return $query->where('seller_id', $sellerId)->where('company_id', $companyId);
	}
	
	/**
     * Companía a la que pertenece la venta
     */
    public function company()
    {
        return $this->belongsTo(Company::class)->select('id', 'name');
    }

    /**
     * Obtiene el vendedor que hizo la venta
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class)->select('id', 'name');
    }
}
