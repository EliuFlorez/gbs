<?php

namespace App\Sales\Models;

class ConfigFixedCommission extends BaseConfigModel
{
	protected $table = 'sls_config_fixed_commission';

	protected $fillable = ['company_id', 'percentage'];
	
	protected $hidden = ['created_at', 'updated_at'];
	
	/**
     * Companía a la que pertenece la venta
     */
    public function company()
    {
        return $this->belongsTo(Company::class)->select('id', 'name');
    }
}
