<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class SellerCommission extends Model
{
    protected $table = 'sls_sellers_commissions';

    protected $fillable = [
    	'sale_id',
        'company_id',
        'seller_id',
    	'commission_type_id',
        'percentage_applied',
    	'extra_percentage_applied',
    	'value',
		'created_at'
    ];

    protected $hidden = [
    	'updated_at',
    ];
	
	/**
     * Obtiene el tipo de comision del broker
     */
    public function commissionType()
    {
    	return $this->belongsTo(CommissionType::class);
    }
	
	/**
     * Ventas a la que pertenece
     */
    public function sale()
    {
        return $this->belongsTo(Sale::class)->select('id', 'policy_number');
    }
	
	/**
     * Companía a la que pertenece la venta
     */
    public function company()
    {
        return $this->belongsTo(Company::class)->select('id', 'name');
    }

    /**
     * Obtiene el vendedor que hizo la venta
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class)->select('id', 'name');
    }
}
