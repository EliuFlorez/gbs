<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionType extends Model
{
	protected $table = 'sls_commission_types';
	
	protected $fillable = ['name'];	

	protected $hidden = ['created_at', 'updated_at', 'name'];

	/**
	 * Buscar por name
	 *
	 * @param      Query   $query
	 * @param      string  $name   Nombre
	 *
	 * @return     Query
	 */
	public function scopeFindByName($query, $name)
	{
		return $query->where('name', $name);
	}
}
