<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $table = 'sls_sales_details';
	
    protected $fillable = [
    	'sale_id',
    	'concept',
    	'amount',
    	'apply_commission',
    ];
}
