<?php

namespace App\Sales\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sls_sales';

    protected $fillable = [
    	'company_id',
    	'total_amount',
    	'sale_date',
        'payment_date',
    	'confirmation_date',
    	'policy_number',
    	'quote_number',
    	'seller_id',
    	'plan_id',
    	'service_type_id',
    	'sale_type_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Obtiene el valor de la venta sobre el cual se aplicará la comisión. Solamente los 
     * detalles de venta que contengan apply_commission = 1 aplican para comisión.
     * @return Decimal
     */
    public function getBaseCommissionValueAttribute()
    {
        return $this->details->sum(function ($detail) {
            if ($detail->apply_commission == 1) {
                return $detail->amount;
            }
        });
    }

    /**
     * Detalles de la venta
     */
    public function details()
    {
    	return $this->hasMany(SaleDetail::class);
    }

    /**
     * Companía a la que pertenece la venta
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Obtiene el vendedor que hizo la venta
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * Obtiene la comision (de la compañía) de la venta
     */
    public function commissionCompanies()
    {
        return $this->hasOne(CompanyCommission::class);
    }

    /**
     * Obtiene las comisiones (de los vendedores) de la venta
     */
    public function commissionsSellers()
    {
        return $this->hasMany(SellerCommission::class);
    }
	
	/**
     * Obtiene la comision (de la compañía) de la venta
     */
    public function commissionCompany()
    {
        return $this->hasOne(CompanyCommission::class);
    }

    /**
     * Obtiene las comisiones (de los vendedores) de la venta
     */
    public function commissionsSeller()
    {
        return $this->hasOne(SellerCommission::class);
    }

    /**
     * Filtrar por ventas del presente mes
     */
    public function scopeCurrentMonth($query)
    {
        return $query->where('sale_date', '>=', Carbon::now()->startOfMonth());
    }
}
