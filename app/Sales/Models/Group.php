<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'sls_groups';

    protected $fillable = ['name', 'percentage'];

    protected $hidden = ['created_at', 'updated_at'];
}
