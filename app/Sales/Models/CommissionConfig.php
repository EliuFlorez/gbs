<?php

namespace App\Sales\Models;

class CommissionConfig extends BaseConfigModel
{
	protected $table = 'sls_commission_config';

	protected $fillable = [
		'relation_id', 
		'percentage_type',
		'percentage'
	];
	
	protected $hidden = [
		'created_at', 
		'updated_at'
	];
}
