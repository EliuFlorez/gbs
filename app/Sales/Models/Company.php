<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;
use App\Sales\Models\CompanyCommission;

class Company extends Model
{
    protected $table = 'sls_companies';

    protected $fillable = [
		'id', 
		'name', 
		'commission_type_id',
		'country',
		'state',
		'city',
		'address',
		'phone',
		'rep_name_first',
		'rep_name_last',
		'rep_din',
		'rep_phone',
		'rep_cell',
	];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Obtiene el tipo de comision del broker
     */
    public function commissionType()
    {
    	return $this->belongsTo(CommissionType::class);
    }

    /**
     * Obtiene la cofiguración para comisión Fixed
     */
    public function fixedConfig()
    {
        return $this->hasOne(ConfigFixedCommission::class);
    }

    /**
     * Obtiene las ventas de la compañía
     */
    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    /**
     * Obtiene las comisiones de la compañia
     */
    public function commissions()
    {
        return $this->hasManyThrough(CompanyCommission::class, Sale::class, 'company_id', 'sale_id');
    }
}
