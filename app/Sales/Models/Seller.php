<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sls_sellers';

    protected $fillable = [
		'name', 
		'monthly_fee', 
		'parent_seller_id', 
		'commission_type_id', 
		'percentage_extra', 
		'group_id',
		'agent_id',
	];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Obtiene las ventas del vendedor
     */
    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    /**
     * Obtiene el tipo de comision del vendedor
     */
    public function commissionType()
    {
    	return $this->belongsTo(CommissionType::class);
    }

	/**
     * Obtiene el grupo del vendedor
     */
    public function group()
    {
    	return $this->belongsTo(Group::class);
    }
	
    /**
     * Obtiene el vendedor "padre"
     */
    public function parent()
    {
    	return $this->belongsTo(Seller::class, 'parent_seller_id');
    }

    /**
     * Obtiene los vendedores "hijos"
     */
    public function children()
    {
        return $this->hasMany(Seller::class, 'parent_seller_id');
    }

    /**
     * Obtiene la cofiguración para comisión Fixed
     */
    public function fixedConfig()
    {
        return $this->hasOne(ConfigFixedSellerCommission::class);
    }

    /**
     * Obtiene las comisiones del vendedor
     */
    public function commissions()
    {
        return $this->hasMany(SellerCommission::class);
    }

    /**
     * Determina si tiene vendedores "hijos".
     *
     * @return     boolean  
     */
    public function hasChildren()
    {
        return $this->children()->exists();
    }
}
