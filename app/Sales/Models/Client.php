<?php

namespace App\Sales\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'sls_clients';

    protected $fillable = ['name', 'percentage'];

    protected $hidden = ['created_at', 'updated_at'];
}
