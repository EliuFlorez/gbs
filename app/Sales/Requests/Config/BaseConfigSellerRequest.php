<?php 

namespace App\Sales\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Validators\PercentageLessThanParent;

/**
* Clase base para Requests de configuración de vendedores
*/
abstract class BaseConfigSellerRequest extends FormRequest
{
	
	function __construct()
	{
		$this->customValidators();
	}

	/**
     * Validaciones personalizadas
     * @return $this
     */
    public function customValidators()
    {
        \Validator::extend('less_than_parent', PercentageLessThanParent::class);
        return $this;
    }

    /**
     * Mensajes de los validadores personalizados
     *
     * @return     array
     */
    public function customValidatorsMessages()
    {
    	return [
    		'less_than_parent' => 'The percentage cannot be greater than the parent\'s percentage'
    	];
    }
}