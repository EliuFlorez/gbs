<?php

namespace App\Sales\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;

class CommissionConfigRequest extends FormRequest
{

    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'relation_id' => 'required|integer',
					'percentage_type' => 'required|string',
                    'percentage' => 'required|numeric|min:0|max:100'
                ];
			break;
                
            case 'PATCH':
            case 'PUT':
                return [
                    'relation_id' => 'required|integer',
					'percentage_type' => 'required|string',
                    'percentage' => 'sometimes|required|numeric|min:0|max:100'
                ];
			break;
			
            default:
                return [
                    'relation_id' => 'required|integer',
					'percentage_type' => 'required|string',
                    'percentage' => 'required|numeric|min:0|max:100'
                ];
			break;
        }
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [
            'relation_id.exists' => 'The relation specified in :attribute does not exist',
            'relation_id.unique' => 'The relation specified in :attribute has already a config row',
        ];
    }
}
