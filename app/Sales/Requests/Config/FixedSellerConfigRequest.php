<?php

namespace App\Sales\Requests\Config;

use App\Sales\Controllers\ApiResponses;
use App\Sales\Validators\PercentageLessThanParent;


class FixedSellerConfigRequest extends BaseConfigSellerRequest
{
    protected $commissionType = 'Fixed';

    use ApiResponses;

    public function __construct()
    {
        $this->customValidators();   
    }

    /**
     * Validaciones personalizadas
     * @return $this
     */
    public function customValidators()
    {
        \Validator::extend('less_than_parent', PercentageLessThanParent::class);
        return $this;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'company_id' => 'required|integer|exists:sls_companies,id',
                    'seller_id' => 'required|integer|exists:sls_sellers,id|unique:sls_config_fixed_seller_commission,seller_id,NULL,id,company_id,' . $this->company_id,
                    'percentage' => 'required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id,
                    'percentage_rn' => 'required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id
                ];
                break;
                
            case 'PATCH':
            case 'PUT':
                return [
                    'company_id' => 'sometimes|required|integer|exists:sls_companies,id',
                    'seller_id' => 'sometimes|required|integer|exists:sls_sellers,id|unique:sls_config_fixed_seller_commission,seller_id,' . $this->route('id') . ',id,company_id,' . $this->company_id,
                    'percentage' => 'sometimes|required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id,
                    'percentage_rn' => 'sometimes|required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id
                ];
                break;
            default:
                return [
                    'company_id' => 'required|integer|exists:sls_companies,id',
                    'seller_id' => 'required|integer|exists:sls_sellers,id||unique:sls_config_fixed_seller_commission',
                    'percentage' => 'required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id,
                    'percentage_rn' => 'required|numeric|min:0|max:100|less_than_parent:' . $this->seller_id . ',company_id,' . $this->company_id
                ];
                break;
            
        }
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        $messages = [
            'company_id.exists' => 'The company specified in :attribute does not exist',
            'seller_id.unique' => 'The seller specified in :attribute has already a config row',
            'less_than_parent' => 'The percentage cannot be greater than the parent\'s percentage' 
        ];

        return array_merge($messages, $this->customValidatorsMessages());
    }
}
