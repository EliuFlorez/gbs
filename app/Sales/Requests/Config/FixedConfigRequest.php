<?php

namespace App\Sales\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;


class FixedConfigRequest extends FormRequest
{

    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'company_id' => 'required|integer|exists:sls_companies,id|unique:sls_config_fixed_commission',
                    'percentage' => 'required|numeric|min:0|max:100'
                ];
                break;
                
            case 'PATCH':
            case 'PUT':
                return [
                    'company_id' => 'sometimes|required|integer|exists:sls_companies,id|unique:sls_config_fixed_commission,company_id,' . $this->route('fixed'),
                    'percentage' => 'sometimes|required|numeric|min:0|max:100'
                ];
                break;
            default:
                return [
                    'company_id' => 'required|integer|exists:sls_companies,id|unique:sls_config_fixed_commission',
                    'percentage' => 'required|numeric|min:0|max:100'
                ];
                break;
            
        }
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [
            'company_id.exists' => 'The company specified in :attribute does not exist',
            'company_id.unique' => 'The company specified in :attribute has already a config row',
        ];
    }
}
