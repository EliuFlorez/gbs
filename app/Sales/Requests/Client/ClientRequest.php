<?php

namespace App\Sales\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;

class ClientRequest extends FormRequest
{

    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string',
                    'percentage' => 'required|numeric',
                ];
                break;
            case 'PATCH':
            case 'PUT':
                return [
                    'name' => 'required|string',
                    'percentage' => 'required|numeric',
                ];
                break;
            default:
                return [
                    'name' => 'required|string',
                    'percentage' => 'required|numeric',
                ];
                break;
        }
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [];
    }
}
