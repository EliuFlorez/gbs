<?php

namespace App\Sales\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;

class SellerRequest extends FormRequest
{
    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
					'name' => 'required|string',
                    'monthly_fee' => 'numeric',
                    'percentage_extra' => 'numeric',
                    'percentage_vn' => 'numeric',
                    'percentage_rp' => 'numeric',
                    'commission_type_id' => 'required|integer|exists:sls_commission_types,id',
                    'parent_seller_id' => 'sometimes|integer|exists:sls_sellers,id',
				];
                break;
            case 'PATCH':
            case 'PUT':
                return [
					'name' => 'sometimes|required|string',
                    'monthly_fee' => 'sometimesnumeric',
                    'percentage_extra' => 'sometimes|numeric',
                    'percentage_vn' => 'sometimes|numeric',
                    'percentage_rp' => 'sometimes|numeric',
                    'commission_type_id' => 'sometimes|required|integer|exists:sls_commission_types,id',
                    'parent_seller_id' => 'sometimes|integer|exists:sls_sellers,id',
				];
                break;
            default:
                return [
					'name' => 'required|string',
                    'monthly_fee' => 'numeric',
                    'percentage_extra' => 'numeric',
                    'percentage_vn' => 'numeric',
                    'percentage_rp'=> 'numeric',
                    'commission_type_id' => 'required|integer|exists:sls_commission_types,id',
                    'parent_seller_id' => 'sometimes|integer|exists:sls_sellers,id',
				];
                break;
        }
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [
            'commission_type_id.exists' => 'The commission specified in :attribute does not exist',
            'parent_seller_id.exists' => 'The seller specified in :attribute does not exist',
        ];
    }
}
