<?php

namespace App\Sales\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;

class CreateCommissionRequest extends FormRequest
{

    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sale_id' => 'required|integer|exists:sls_sales,id',
        ];
    }
    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [
            'sale_id.exists' => 'The :attribute not exists',
        ];
    }
}
