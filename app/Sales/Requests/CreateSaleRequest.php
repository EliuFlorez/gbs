<?php

namespace App\Sales\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Sales\Controllers\ApiResponses;

class CreateSaleRequest extends FormRequest
{
    use ApiResponses;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_amount'                  => 'required|numeric',
            //'sale_date'                     => 'required|date_format:Y-m-d',
            //'payment_date'                  => 'required|date_format:Y-m-d',
            //'confirmation_date'             => 'required|date_format:Y-m-d',
            'policy_number'                 => 'required',
            //'quote_number'                  => 'required|integer',
            //'details'                       => 'required|array',
            //'details.*.concept'             => 'required|string',
            //'details.*.amount'              => 'required|numeric',
            //'details.*.apply_commission'    => 'required|integer',
            'seller_id'                     => 'required|integer|exists:sls_sellers,id',
            'company_id'                    => 'required|integer|exists:sls_companies,id',
            //'plan_id'                       => 'integer',
            //'service_type_id'               => 'integer',
            'sale_type_id'                  => 'integer',
        ];
    }

    
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->respondBadRequest($errors);
    }

    /**
     * Error messages
     * @return array 
     */
    public function messages()
    {
        return [
            //'seller_id.exists' => 'The :attribute not exists',
            //'company_id.exists' => 'The :attribute not exists',
        ];
    }
}
