<?php

namespace App\Sales\Controllers;

use Illuminate\Http\Request;
use App\Sales\Models\Company;
use App\Sales\Models\ConfigFixedCommission;
use App\Sales\Requests\Company\CompanyRequest;
use App\Http\Controllers\Controller;

class CompaniesController extends Controller
{
	use ApiResponses;

	/**
	 * Listado de compañías
	 *
	 * Muestra un listado paginado de compañías
	 *
	 * @return     json
	 */
	public function index()
	{
		$limit = config('novasales.pagination_size.companies');
		
		$companies = Company::with(['commissionType', 'fixedConfig'])->get();
		
		return response()->success(compact('companies'));
	}

	/**
	 * Mostrar compañía
	 *
	 * Muestra la información de la compañía especificada en el id
	 *
	 * @param      integer  $id     El id de la  compañía
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $company = Company::find($id)) {
			return $this->respondNotFound("The company with id $id does not exist");
		}
		
		$company = Company::with('fixedConfig')->where('id', '=', $id)->first();
		
		return response()->success($company);
	}

	/**
	 * Crear compañía
	 *
	 * Crea una nueva compañía
	 *
	 * @param      Request  $request
	 *
	 * @return     json
	 */
	public function store(CompanyRequest $request)
	{
		$company = Company::create($request->all());
		
		if ($company) {
			ConfigFixedCommission::create([
				'company_id' => $company->id, 
				'percentage' => $request['percentage']
			]);
		}
		
		return response()->success('success');
	}

	/**
	 * Actualizar compañía
	 *
	 * Actualiza la información de la compañía especificada en el id
	 *
	 * @param      \Illuminate\Http\Request  $request
	 * @param      integer                   $id       El id de la compañía
	 *
	 * @return     json
	 */
	public function update(CompanyRequest $request, $id)
	{
		if ( ! $company = Company::find($id)) {
			return $this->respondNotFound("The company with id $id does not exist");
		}

		$configFixed = ConfigFixedCommission::where('company_id', '=', $company->id)->first();
		
		$request = $request->input('data');
		
		if (!empty($configFixed)) {
			$configFixed->percentage = $request['fixed_config']['percentage'];
			$configFixed->save();
		} else {
			ConfigFixedCommission::create([
				'company_id' => $company->id, 
				'percentage' => $request['fixed_config']['percentage']
			]);
		}
		
		$company->update($request);
		
		return response()->success('success');
	}
	 
	/**
	 * Eliminar compañía
	 *
	 * Elimina la compañía especificada en el id
	 *
	 * @param      integer  $id     El id de la compañía
	 *
	 * @return     json
	 */
    public function destroy($id)
    {
    	if ( ! $company = Company::find($id)) {
			return $this->respondNotFound("The company with id $id does not exist");
    	}

    	$company->delete();
		
		return response()->success('success');
    }
    
}