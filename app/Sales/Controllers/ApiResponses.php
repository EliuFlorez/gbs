<?php

namespace App\Sales\Controllers;

use Response;
use Illuminate\Http\Request;

trait ApiResponses
{
	protected $statusCode = 200;

    /**
     * Gets the value of statusCode.
     *
     * @return     mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param      mixed  $statusCode  the status code
     *
     * @return     self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Retorna una respuesta 400
     *
     * @param      mixed                     $message  Mensaje(s) de error
     *
     * @return     Illuminate\Http\Response
     */
    public function respondBadRequest($message = 'Bad Request Error!')
    {
    	return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * Retorna una respuesta 404
     *
     * @param      mixed                     $message  Mensaje(s) de error
     *
     * @return     Illuminate\Http\Response
     */
    public function respondNotFound($message = 'NotFound Error!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * Retorna una respuesta 409 por algún tipo de conflicto
     *
     * @param      mixed                     $message  Mensaje(s) de error
     *
     * @return     Illuminate\Http\Response
     */
    public function respondConflict($message = 'There are conflicts. Can\'t process your request!')
    {
        return $this->setStatusCode(409)->respondWithError($message);
    }

    /**
     * Retorna una respuesta 400 basada en una excepción
     *
     * @param      string  $message  Mensaje(s) de error
     *
     * @return     Illuminate\Http\Response  
     */
    public function respondException(\Exception $e)
    {
        $class = collect(explode('\\', get_class($e)))->last();
        return $this->setStatusCode(400)->respondWithError([
            'type'      => $class,
            'message'   => $e->getMessage()
        ]);
    }

    /**
     * Retorna una respuesta
     *
     * @param      array  $data     Datos de la respuesta
     * @param      array   $headers  The headers
     *
     * @return     Illuminate\Http\Response  
     */
    public function respond($data, $headers = [])
    {
    	return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Retorna una respuesta de error adjuntando el status code
     *
     * @param      mixed  $message  The message
     *
     * @return     Illuminate\Http\Response  
     */
    public function respondWithError($message)
    {
    	return $this->respond([
    		'error' 		=> $message,
    		'status_code'	=> $this->getStatusCode()
    	]);
    }
}