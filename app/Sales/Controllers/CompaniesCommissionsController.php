<?php

namespace App\Sales\Controllers;

use Carbon\Carbon;
use App\Sales\Models\Company;
use App\Http\Controllers\Controller;

class CompaniesCommissionsController extends Controller
{
	use ApiResponses;

    /**
     * Comisiones cobradas a una compañía
     *
     * Muestra las comisiones cobradas a la compañía especificada en el id
     *
     * @param      integer    $id     El id de la compañía
     *
     * @return     json
     */
    public function index($id)
    {
    	if (! $company = Company::find($id)) {
    		return $this->respondNotFound("The company with id $id does not exist");
    	}
		
		$date_ini = (isset($_GET['ini'])) ? $_GET['ini'] : '';
		$date_end = (isset($_GET['end'])) ? $_GET['end'] : '';
		
		$dateIni = new Carbon($date_ini);
		$dateEnd = new Carbon($date_end);

		$limit = config('novasales.pagination_size.commissions_companies');
		
		if (!empty($date_ini) && !empty($date_end)) {
			$commissions = $company->commissions()->with(['sale', 'company', 'seller', 'commissionType'])
				->whereBetween('sls_companies_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])
				->paginate($limit);
		} else {
			$commissions = $company->commissions()->with(['sale', 'company', 'seller', 'commissionType'])->paginate($limit);
		}
		
    	return $this->respond($commissions);
    }
	
}