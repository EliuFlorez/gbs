<?php

namespace App\Sales\Controllers;

use Illuminate\Http\Request;
use App\Sales\Models\CommissionType;
use App\Http\Controllers\Controller;

class CommissionsTypesController extends Controller
{
    use ApiResponses;

    /**
     * Tipos de comisiones
     *
     * Muestra los tipos de comisiones disponibles
     *
     * @return     json  Tipos de comisiones disponibles
     */
    public function index()
    {
        $types = CommissionType::all();
        
        return $this->respond(['data' => $types]);
    }

}