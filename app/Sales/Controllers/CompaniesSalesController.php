<?php

namespace App\Sales\Controllers;

use App\Sales\Models\Company;
use App\Http\Controllers\Controller;

class CompaniesSalesController extends Controller
{
	use ApiResponses;

    /**
     * Ventas de una compañía
     *
     * Muestra las ventas de la compañía especificada en el id
     *
     * @param      integer  $id     El id de la compañía
     *
     * @return     json
     */
    public function index($id)
    {
    	if (! $company = Company::find($id)) {
    		return $this->respondNotFound("The company with id $id does not exist");
    	}

		$limit = config('novasales.pagination_size.sales');
		
		$datas = $company->sales()->with(['company', 'seller'])->paginate($limit);
		
    	return $this->respond($datas);
    }
}