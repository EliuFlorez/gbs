<?php

namespace App\Sales\Controllers;

use App\Sales\Models\Seller;
use App\Http\Controllers\Controller;

class SellersSalesController extends Controller
{

    use ApiResponses;

    /**
     * Ventas de un vendedor
     * 
     * Obtiene las ventas heachas por el vendedor especificado en el id
     *
     * @param      integer  $id     El id del vendedor
     *
     * @return     json
     */
    public function index($id)
    {
        if (! $seller = Seller::find($id)) {
            return $this->respondNotFound("The seller with id $id does not exist");
        }

        $limit = config('novasales.pagination_size.sales');
		
		$datas = $seller->sales()->with(['company', 'seller'])->paginate($limit);
		
    	return $this->respond($datas);
    }
}