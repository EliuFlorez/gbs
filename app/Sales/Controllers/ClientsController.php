<?php

namespace App\Sales\Controllers;

use Illuminate\Http\Request;
use App\Sales\Models\Client;
use App\Sales\Requests\Client\ClientRequest;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
	use ApiResponses;

	/**
	 * Listado de clientes
	 *
	 * Muestra un listado paginado de clientes
	 *
	 * @return     json
	 */
	public function index()
	{
		$limit = config('novasales.pagination_size.clients');
		
		$clients = Client::paginate($limit);
		
		return response()->success(compact('clients'));
	}

	/**
	 * Mostrar cliente
	 *
	 * Muestra la información de la cliente especificada en el id
	 *
	 * @param      integer  $id     El id de el cliente
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $data = Client::find($id)) {
			return $this->respondNotFound("The client with id $id does not exist");
		}
		
		return response()->success($data);
	}

	/**
	 * Crear cliente
	 *
	 * Crea una nuevo cliente
	 *
	 * @param      Request  $request
	 *
	 * @return     json
	 */
	public function store(ClientRequest $request)
	{
		Client::create($request->all());
		
		return response()->success('success');
	}

	/**
	 * Actualizar cliente
	 *
	 * Actualiza la información de el cliente especificada en el id
	 *
	 * @param      \Illuminate\Http\Request  $request
	 * @param      integer                   $id       El id de el cliente
	 *
	 * @return     json
	 */
	public function update(ClientRequest $request, $id)
	{
		if ( ! $data = Client::find($id)) {
			return $this->respondNotFound("The client with id $id does not exist");
		}

		$data->update($request->all());
		
		return response()->success('success');
	}
	 
	/**
	 * Eliminar cleinte
	 *
	 * Elimina la cliente especificada en el id
	 *
	 * @param      integer  $id     El id de la cliente
	 *
	 * @return     json
	 */
    public function destroy($id)
    {
    	if ( ! $data = Client::find($id)) {
			return $this->respondNotFound("The client with id $id does not exist");
    	}

    	$data->delete();
		
		return response()->success('success');
    }
    
}