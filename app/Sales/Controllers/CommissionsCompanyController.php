<?php

namespace App\Sales\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Sales\Models\Sale;
use App\Sales\Models\Company;
use App\Sales\Models\SaleDetail;
use App\Sales\Models\CompanyCommission;
use App\Http\Controllers\Controller;
use App\Sales\CommissionsCompany\CommissionFactory;
use App\Sales\Requests\Company\CreateCommissionRequest;
use App\Sales\CommissionsCompany\Exceptions\CommissionException;

class CommissionsCompanyController extends Controller
{
    use ApiResponses;

    /**
     * Comisiones cobradas a compañías
     *
     * Muestra un listado de todas las comisiones cobradas a compañías
     *
     * @return     json  Comisiones cobradas
     */
    public function index()
    {
		$idParent = (isset($_GET['id_parent'])) ? $_GET['id_parent'] : '';
		$pdfPrint = (isset($_GET['print'])) ? true : false;
		$date_ini = (isset($_GET['ini']) && !empty($date_ini)) ? $_GET['ini'] : '';
		$date_end = (isset($_GET['end']) && !empty($date_end)) ? $_GET['end'] : '';
		
		if (!empty($date_ini) && !empty($date_end)) 
		{
			$dateIni = new Carbon($date_ini);
			$dateEnd = new Carbon($date_end);
			if (!empty($idParent)) {
				$commissions = CompanyCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->where('company_id', '=', $idParent)
				->whereBetween('sls_companies_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])->get();
			} else {
				$commissions = CompanyCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->whereBetween('sls_companies_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])->get();
			}
		} 
		else {
			if (!empty($idParent)) {
				$commissions = CompanyCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->where('company_id', '=', $idParent)
				->get();
			} else {
				$commissions = CompanyCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->get();
			}
		}
		
		if ($pdfPrint == true) 
		{
			//
		}
		
		$commissionsCompanies = $commissions;
		
		return response()->success(compact('commissionsCompanies'));
    }

    /**
     * Comision de compañía local
     *
     * Calcula la comisión, basada en una venta, que debe cobrar la compañía local
     *
     * @param      \App\Sales\Requests\Company\CreateCommissionRequest  $request  La solicitud
     *
     * @return     json
     */
    public function create(CreateCommissionRequest $request)
    {
        $sale = Sale::find($request['sale_id']);

        if ( ! $sale->company()->exists()) {
            return $this->respondConflict('Can\'t calculate. The company for this sale is null');
        }
        
		$percentage = $request['percentage'];
		
        DB::beginTransaction();
        try {
            $commission = (new CommissionFactory)->make($sale->company, $sale)->calculate($percentage)->save();
        } catch (CommissionException $e) {
            DB::rollback();
    		return $this->respondException($e);
    	} catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    	
    	return response()->success('success');
    }
}