<?php

namespace App\Sales\Controllers;

use Illuminate\Http\Request;
use App\Sales\Models\Group;
use App\Http\Controllers\Controller;
use App\Sales\Requests\Seller\GroupRequest;

class GroupsController extends Controller
{

	use ApiResponses;

	/**
	 * Listado de grupos
	 *
	 * Muestra un listado paginado de grupos
	 *
	 * @return     json
	 */
	public function index()
	{
		$limit = config('novasales.pagination_size.groups');
		
		$data = Group::paginate($limit);
		
		return $this->respond($data);
	}

	/**
	 * Mostrar grupos
	 *
	 * Muestra la información de grupo especificada en el id
	 *
	 * @param      integer  $id     El id de el grupo
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $data = Group::find($id)) {
			return $this->respondNotFound("The group with id $id does not exist");
		}
		
		return $this->respond(['data' => $data]);
	}

	/**
	 * Crear grupo
	 *
	 * Crea una nuevo grupo
	 *
	 * @param      Request  $request
	 *
	 * @return     json
	 */
	public function store(GroupRequest $request)
	{
		$data = Group::create($request->all());
		
		return $this->respond(['data' => ['id' => $data->id]]);
	}

	/**
	 * Actualizar grupo
	 *
	 * Actualiza la información de grupo especificada en el id
	 *
	 * @param      \Illuminate\Http\Request  $request
	 * @param      integer                   $id       El id de el grupo
	 *
	 * @return     json
	 */
	public function update(GroupRequest $request, $id)
	{
		if ( ! $data = Group::find($id)) {
			return $this->respondNotFound("The Group with id $id does not exist");
		}

		$data->update($request->all());
		
		return $this->respond(['data' => ['id' => $data->id]]);
	}
	 
	/**
	 * Eliminar cleinte
	 *
	 * Elimina la grupo especificada en el id
	 *
	 * @param      integer  $id     El id de la grupo
	 *
	 * @return     json
	 */
    public function destroy($id)
    {
    	if ( ! $data = Group::find($id)) {
			return $this->respondNotFound("The Group with id $id does not exist");
    	}

    	$data->delete();
		
		return $this->respond(['data' => ['deleted' => $data->id]]);
    }
    
}