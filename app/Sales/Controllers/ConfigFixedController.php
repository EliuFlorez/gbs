<?php

namespace App\Sales\Controllers;

use Illuminate\Http\Request;
use App\Sales\Models\Company;
use App\Sales\Controllers\ApiResponses;
use App\Http\Controllers\Controller;
use App\Sales\Models\ConfigFixedCommission;
use App\Sales\Requests\Config\FixedConfigRequest;

class ConfigFixedController extends Controller
{
	use ApiResponses;

	/**
	 * Listado de configuraciones comisión Fija
	 *
	 * Listado de configuraciones registradas para empresas referentes al tipo
	 * de comisión Fija
	 *
	 * @return     json
	 */
	public function index()
	{
		$limit = config('novasales.pagination_size.config_fixed_companies');
		
		$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
		
		if (!empty($id)) {
			$configs = ConfigFixedCommission::with('company')
				->where('company_id', '=', $id)
				->paginate($limit);
		} else {
			$configs = ConfigFixedCommission::paginate($limit);
		}
		
		return $this->respond($configs);
	}

	/**
	 * Mostrar configuración Fija
	 *
	 * Muestra la configuración especificada en el id
	 *
	 * @param      integer  $id     El id de la configuración
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $config = ConfigFixedCommission::find($id)) {
			return $this->respondNotFound("The config with id $id does not exist");
		}
		
		return $this->respond(['data' => $config]);
	}

	/**
	 * Crear configuración comisión Fija
	 *
	 * Crea una configuración de comisión Fija
	 *
	 * @param      FixedConfigRequest  $request
	 *
	 * @return     json
	 */
	public function store(FixedConfigRequest $request)
	{
		$config = ConfigFixedCommission::create($request->all());
		
		return $this->respond(['data' => ['id' => $config->id]]);
	}

	/**
	 * Actualizar configuración comisión Fija
	 *
	 * Actualiza la configuración de comisión Fija especificada en el id
	 *
	 * @param      FixedConfigRequest  $request
	 * @param      integer             $id       Id de la configuración
	 *
	 * @return     json
	 */
	public function update(FixedConfigRequest $request, $id)
	{
		if ( ! $config = ConfigFixedCommission::find($id)) {
			return $this->respondNotFound("The config with id $id does not exist");
		}

		$config->update($request->all());

		return $this->respond(['data' => ['id' => $config->id]]);
	}
    
    /**
     * Eliminar configuración comisión Fija
     *
     * Elimina una configuración de comision Fija
     *
     * @param      integer  $id     El id de la configuración
     *
     * @return     json
     */
    public function destroy($id)
    {
    	if ( ! $config = ConfigFixedCommission::find($id)) {
			return $this->respondNotFound("The config with id $id does not exist");
		}

    	$config->delete();
		
		return $this->respond(['data' => ['deleted' => $config->id]]);
    }
}