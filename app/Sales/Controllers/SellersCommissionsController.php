<?php

namespace App\Sales\Controllers;

use Carbon\Carbon;
use App\Sales\Models\Seller;
use App\Http\Controllers\Controller;

class SellersCommissionsController extends Controller
{
	use ApiResponses;

    /**
     * Comisiones de un vendedor
     *
     * Muestra las comisiones de un vendedor
     *
     * @param      integer  $id     El id del vendedor
     *
     * @return     json
     */
    public function index($id)
    {
    	if (! $seller = Seller::find($id)) {
    		return $this->respondNotFound("The seller with id $id does not exist");
    	}

		$date_ini = (isset($_GET['ini'])) ? $_GET['ini'] : '';
		$date_end = (isset($_GET['end'])) ? $_GET['end'] : '';
		
		$dateIni = new Carbon($date_ini);
		$dateEnd = new Carbon($date_end);
		
		$limit = config('novasales.pagination_size.commissions_sellers');
		
		if (!empty($date_ini) && !empty($date_end)) {
			$commissions = $seller->commissions()->with(['company', 'seller', 'commissionType'])
				->whereBetween('sls_sellers_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])
				->paginate($limit);
		} else {
			$commissions = $seller->commissions()->with(['company', 'seller', 'commissionType'])->paginate($limit);
		}
		
    	return $this->respond($commissions);
    }
}