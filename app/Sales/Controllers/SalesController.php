<?php

namespace App\Sales\Controllers;

use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Sales\Models\Sale;
use App\Sales\Models\Seller;
use App\Sales\Models\Company;
use App\Sales\Models\SaleDetail;
use App\Sales\Requests\CreateSaleRequest;
use App\Http\Controllers\Controller;
use App\Sales\CommissionsCompany\CommissionFactory;
use App\Sales\CommissionsCompany\Exceptions\CommissionException;

class SalesController extends Controller
{
	use ApiResponses;

	/**
	 * Listado de ventas
	 * 
	 * Muestra un listado paginado de ventas
	 * 
	 * @return json
	 */
	public function index()
	{
		$sales = Sale::with([
			'company', 
			'commissionCompany', 
			'seller', 
			'commissionsSeller'
		])->get();
		
		return response()->success(compact('sales'));
	}

	/**
	 * Mostrar venta
	 * 
	 * Muestra una venta a partir de su id
	 *
	 * @param      integer  $id     El id de la venta
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $sale = Sale::find($id)) {
			$this->respondNotFound("The sal with id $id does not exist");
		}

		// Company
		$companyData = Company::with('fixedConfig')->where('id', '=', $sale['company_id'])->first();
		if (!empty($companyData)) {
			$companyData = json_decode(json_encode($companyData), true);
			$companyPercentage = $companyData['fixedConfig']['percentage'];
		} else {
			$companyPercentage = 0;
		}
		
		// Seller
		$sellerData = Seller::with('fixedConfig')->where('id', '=', $sale['seller_id'])->first();
		if (!empty($sellerData)) {
			$sellerData = json_decode(json_encode($sellerData), true);
			if ($sale['sale_type_id'] == 1) {
				$sellerPercentage = $sellerData['fixedConfig']['percentage'];
			} else if ($sale['sale_type_id'] == 2) {
				$sellerPercentage = (int)$sellerData['fixedConfig']['percentage_rn'];
			} else {
				$sellerPercentage = $sellerData['fixedConfig']['percentage'];
			}
		} else {
			$sellerPercentage = 0;
		}
		
		return $this->respond([
			'company_percentage' => $companyPercentage,
			'seller_percentage' => $sellerPercentage
		]);
		
		return response()->success($sale->load(['details', 'company', 'seller']));
	}

	/**
	 * Crea venta
	 * 
	 * Crea una nueva venta
	 *
	 * @param      CreateSaleRequest  $request
	 *
	 * @return     json
	 */
    public function store(CreateSaleRequest $request)
    {
		$sale = DB::transaction(function () use($request) {
	    	$sale = Sale::create($request->all());
			if (isset($request['details']) && !empty($request['details'])) {
				foreach ($request['details'] as $saleDetail) {
					$detail = new SaleDetail($saleDetail);
					$sale->details()->save($detail);
				}
			}
	    	return $sale;
		});

    	return response()->success('success');
    }
}
