<?php

namespace App\Sales\Controllers;

use DB;
use Carbon\Carbon;
use App\Http\Requests;
use App\Sales\Models\Sale;
use App\Sales\CascadeCommission;
use App\Sales\Models\SellerCommission;
use App\Http\Controllers\Controller;
use App\Sales\CommissionsSeller\CommissionFactory;
use App\Sales\Requests\Seller\CreateCommissionRequest;
use App\Sales\CommissionsCompany\Exceptions\CommissionException;

class CommissionsSellerController extends Controller
{
    use ApiResponses;

    /**
     * Comisiones de vendedores
     *
     * Muestra la lista de comisiones calculadas para vendedores
     *
     * @return     json
     */
    public function index()
    {
		$idParent = (isset($_GET['id_parent'])) ? $_GET['id_parent'] : '';
		$pdfPrint = (isset($_GET['print'])) ? true : false;
		$date_ini = (isset($_GET['ini']) && !empty($date_ini)) ? $_GET['ini'] : '';
		$date_end = (isset($_GET['end']) && !empty($date_end)) ? $_GET['end'] : '';
		
		if (!empty($date_ini) && !empty($date_end)) 
		{
			$dateIni = new Carbon($date_ini);
			$dateEnd = new Carbon($date_end);
			if (!empty($idParent)) {
				$commissions = SellerCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->where('seller_id', '=', $idParent)
				->whereBetween('sls_sellers_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])->get();
			} else {
				$commissions = SellerCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->whereBetween('sls_sellers_commissions.created_at', [
					$dateIni->format('Y-m-d')." 00:00:00", 
					$dateEnd->format('Y-m-d')." 23:59:59"
				])->get();
			}
		} 
		else {
			if (!empty($idParent)) {
				$commissions = SellerCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->where('seller_id', '=', $idParent)
				->get();
			} else {
				$commissions = SellerCommission::with(['sale', 'company', 'seller', 'commissionType'])
				->get();
			}
		}
		
		if ($pdfPrint == true) 
		{
			//
		}
		
		$commissionsSellers = $commissions;
		
        return response()->success(compact('commissionsSellers'));
    }

    /**
     * Comisión de vendedor
     *
     * Calcula la comisión, basada en una venta, que le corresponde al vendor
     * que la realizo
     *
     * @param      \App\Sales\Requests\Seller\CreateCommissionRequest  $request  La solicitud
     *
     * @return     json
     */
    public function create(CreateCommissionRequest $request)
    {
        $sale = Sale::find($request['sale_id']);

        if ( ! $sale->seller()->exists()) {
            return $this->respondConflict('Can\'t calculate. The seller for this sale is null');
        }

		$percentage = $request['percentage'];
		
        DB::beginTransaction();
		
    	try {
    		$commission = (new CommissionFactory)->make($sale->seller, $sale)->calculate($percentage)->save();
    	} catch (CommissionException $e) {
            DB::rollback();
    		return $this->respondException($e);
    	} catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        /**
         * Si tiene "padre" calcular comisiones en cascada
         */
        if ($sale->seller->parent_seller_id and true) {
            try {
                $cascade = (new CascadeCommission($sale))->handle();
            } catch (CommissionException $e) {
                DB::rollback();
                return $this->respondException($e);
            } catch (\Exception $e) {
                DB::rollback();
				throw $e;
            }
            
			DB::commit();

            return response()->success('success');
        }

        DB::commit();
		
        return response()->success('success');
    }
}