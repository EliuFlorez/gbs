<?php

namespace App\Sales\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Sales\Models\Seller;
use App\Sales\Models\ConfigFixedSellerCommission;
use App\Sales\Requests\Seller\SellerRequest;
use App\Http\Controllers\Controller;

/**
 * Vendedores
 */
class SellersController extends Controller
{
	use ApiResponses;
	
	protected $sellerData = [];

	/**
	 * Listado de vendedores
	 * 
	 * Muestra un listado paginado de vendedores
	 *
	 * @return     json 
	 */
	public function index()
	{
		$sellers = Seller::with(['commissionType', 'fixedConfig'])->get();
		
		return response()->success(compact('sellers'));
	}

	public function getSellers($id, $config = '')
	{
		$datas = [];

		$datas = ConfigFixedSellerCommission::with('seller')->where('company_id', '=', $id)->get();

		foreach ($datas as $key => $value) {
			$id = $value->seller->id;
			$status = $this->searcharray($id, 'id', $this->sellerData);
			if ($status == false) {
				$this->sellerData[] = [
					'id' => $value->seller->id,
					'name' => $value->seller->name
				];
			}
		}
	}

	function searcharray($value, $key, $array) 
	{
		foreach ($array as $k => $val) {
			if ($val[$key] == $value) {
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Mostrar vendedor
	 * 
	 * Muestra un vendedor a partir de su id
	 *
	 * @param      integer  $id     El id del vendedor
	 *
	 * @return     json
	 */
	public function show($id)
	{
		if ( ! $seller = Seller::find($id)) {
			return $this->respondNotFound("The seller with id $id does not exist");
		}
		
		$seller = Seller::with('fixedConfig')->where('id', '=', $id)->first();
		
		return response()->success($seller);
	}

	/**
	 * Crear vendedor
	 *
	 * Crea un nuevo vendedor
	 *
	 * @param      Request  $request
	 *
	 * @return     json
	 */
	public function store(SellerRequest $request)
	{
		$seller = DB::transaction(function () use($request) 
		{
	    	$seller = Seller::create($request->all());
			
			if ($seller) 
			{
				ConfigFixedSellerCommission::create([
					'seller_id' => $seller->id, 
					'company_id' => $request['company_id'], 
					'percentage' => $request['percentage_vn'], 
					'percentage_rn' => $request['percentage_rp'], 
				]);
			}
			
	    	return $seller;
		});

    	return response()->success('success');
	}

	/**
	 * Actualizar vendedor
	 * 
	 * Actualiza la información del vendedor especificado en el id
	 *
	 * @param      \Illuminate\Http\Request  $request
	 * @param      integer                   $id       El id del vendedor
	 *
	 * @return     json
	 */
	public function update(SellerRequest $request, $id)
	{
		if ( ! $seller = Seller::find($id)) {
			return $this->respondNotFound("The seller with id $id does not exist");
		}

		$seller->update($request->all());
		
		return response()->success('success');
	}
    
    /**
     * Eliminar vendedor
     * 
     * Elimina el vendedor especificado en el id
     *
     * @param      integer  $id     El id del vendedor
     *
     * @return     json
     */
    public function destroy($id)
    {
    	if ( ! $seller = Seller::find($id)) {
			return $this->respondNotFound("The seller with id $id does not exist");
    	}

    	if ( $seller->hasChildren()) {
    		return $this->respondConflict("Can't delete. The seller has " . $seller->children()->count() . " dependent sellers.");
    	}

    	$seller->delete();
		
		return response()->success('success');
    }
}