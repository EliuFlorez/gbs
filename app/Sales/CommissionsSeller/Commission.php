<?php

namespace App\Sales\CommissionsSeller;

use App\Sales\Models\Sale;
use App\Sales\Models\Seller;
use App\Sales\Models\CommissionType;
use App\Sales\Models\SellerCommission;
use App\Sales\CommissionsCompany\Exceptions\ConfigRowNotFound;
use App\Sales\CommissionsCompany\Exceptions\CommissionTypeNotFound;
use App\Sales\CommissionsCompany\Exceptions\CommissionConfigModelNotFound;

/**
* Comision base
*/
abstract class Commission
{

	/**
	 * Venta de la cual se calculará la comisión
	 * @var App\Sales\Models\Sale
	 */
	protected $sale;

	/**
	 * Compañia al que se adjudica la comisión
	 * @var App\Sales\Models\Company
	 */
	protected $company;
	
	/**
	 * Vendedor al que se adjudica la comisión
	 * @var App\Sales\Models\Seller
	 */
	protected $seller;

	/**
	 * Porcentaje que se aplicará a la venta para obtener la comisión
	 * @var Decimal
	 */
	protected $percentage;
	
	/**
	 * Porcentaje que se aplicará a la venta para obtener la comisión
	 * @var Decimal
	 */
	protected $percentage_rn;

	/**
	 * Porcentaje extra que se aplicará a la venta para obtener la comisión
	 * @var Decimal
	 */
	protected $percentageExtra = 0;

	/**
	 * Ha alcanzado o superado la meta mensual de ventas
	 * @var Decimal
	 */
	protected $achieveMonthlyFee = false;

	/**
	 * Valor sobre el cual se aplicará el porcentaje de comisión
	 * @var Decimal
	 */
	protected $baseCommissionValue;

	/**
	 * Valor de la comisión.
	 * @var Decimal
	 */
	protected $value;

	/**
	 * Modelo que corresponde a la tabla de configuración de este tipo de comisión.
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $configModel;

	/**
	 * Modelo en donde se guardará la info de la comisión calculada
	 */
	protected $model;

	public function __construct(Sale $sale)
	{
		$this->sale = $sale;
		$this->company = $sale->company;
		$this->seller = $sale->seller;
		
		if(! class_exists($this->configModel)){
			throw new CommissionConfigModelNotFound("The class " . $this->configModel . " does not exists");
		}
		
		$this->configModel = new $this->configModel;

		$this->setupValues();
	}

	/**
	 * Calcula la comisión correspondiente por la venta
	 * @return this
	 */
	public function calculate($percentage = null)
	{
		if (!empty($percentage)) {
			$this->percentage = $percentage;
		}
		
		$this->value = ($this->baseCommissionValue * $this->percentage) / 100;

		$this->value += ($this->baseCommissionValue * $this->percentageExtra) / 100;

		return $this;
	}

	/**
	 * Guarda la comisión y sus detalles en la base de datos
	 * @return this
	 */
	public function save()
	{
		$sale_id = $this->sale->id;
		$seller_id = $this->seller->id;
		$sale_type = $this->sale->sale_type;
		$company_id = $this->company->id;
		$parent_seller_id = $this->seller->parent_seller_id;
		
		// Sub Agent
		if (!empty($parent_seller_id) && $parent_seller_id > 0) 
		{
			$sellerFixedPar = ConfigFixedSellerCommission::where('company_id', '=', $company_id)
				->where('seller_id', '=', $parent_seller_id)
				->first();
			
			if (!empty($sellerFixedPar)) 
			{
				if ($sale_type == 'renovacion') {
					$percentagePar = $sellerFixedPar['percentage_rn'];
				} else {
					$percentagePar = $sellerFixedPar['percentage'];
				}
				
				$percentagePar = abs($percentagePar - $this->percentage);
				
				$valuePar = ($this->baseCommissionValue * $percentagePar) / 100;
				$valuePar += ($this->baseCommissionValue * $this->percentageExtra) / 100;
				$valuePar = abs($valuePar - $this->value);
				
				$dataPar = array(
					'sale_id' 				=> $sale_id,
					'commission_type_id' 	=> $this->getCommissionTypeId(),
					'company_id' 			=> $company_id,
					'seller_id' 			=> $parent_seller_id,
					'percentage' 			=> $percentagePar,
					'percentageExtra' 		=> $this->percentageExtra,
					'value' 				=> $valuePar
				);
				
				$this->setSellerCommission($dataPar);
			}
		}
		
		$dataSub = array(
			'sale_id' 				=> $sale_id,
			'commission_type_id' 	=> $this->getCommissionTypeId(),
			'company_id' 			=> $company_id,
			'seller_id' 			=> $seller_id,
			'percentage' 			=> $this->percentage,
			'percentageExtra' 		=> $this->percentageExtra,
			'value' 				=> $this->value
		);
		
		$this->setSellerCommission($dataSub);
		
		return $this;
	}

	public function setSellerCommission($data = [])
	{
		$commissionsSellers = SellerCommission::where('sale_id', '=', $data['sale_id'])
			->where('seller_id', '=', $data['seller_id'])
			->first();
		
		if (!empty($commissionsSellers)) {
			$commissionsSellers->commission_type_id			= $data['commission_type_id'];
			$commissionsSellers->company_id					= $data['company_id'];
			$commissionsSellers->seller_id					= $data['seller_id'];
			$commissionsSellers->percentage_applied			= $data['percentage'];
			$commissionsSellers->extra_percentage_applied	= $data['percentageExtra'];
			$commissionsSellers->value						= $data['value'];
			$commissionsSellers->save();
		} else {
			$commissionsSellers = new SellerCommission;
			$commissionsSellers->sale_id					= $data['sale_id'];
			$commissionsSellers->commission_type_id			= $data['commission_type_id'];
			$commissionsSellers->company_id					= $data['company_id'];
			$commissionsSellers->seller_id					= $data['seller_id'];
			$commissionsSellers->percentage_applied			= $data['percentage'];
			$commissionsSellers->extra_percentage_applied	= $data['percentageExtra'];
			$commissionsSellers->value						= $data['value'];
			$commissionsSellers->save();
		}
	}
	
	/**
	 * Prepara los valores para el cálculo de la comisión
	 */
	public function setupValues()
	{
		$type = '';
		
		$sale_type = $this->sale->sale_type;
		
		if ($sale_type == 'renovacion') {
			$type = 'RN';
		}
		
		$this->percentage = $this->getConfigPercentage($type);

		$commissionValue = $this->sale->baseCommissionValue;
		
		if ($commissionValue > 0) {
			$this->baseCommissionValue = $commissionValue;
		} else {
			$this->baseCommissionValue = $this->sale->total_amount;
		}
		
		if($this->achieveMonthlyFee = $this->hasAchievedMonthlyFee()){
			$this->percentageExtra += $this->seller->percentage_extra;
		}

		return $this;
	}

	public function hasAchievedMonthlyFee()
	{
		$salesValue = 0;
		
		$sales = Sale::currentMonth()->where('seller_id', $this->seller->id)->get();
		
		foreach ($sales as $sale) {
			$salesValue += $sale->baseCommissionValue;
		}
		
		return ($salesValue > $this->seller->monthly_fee);
	}

	/**
	 * Obtiene el porcentage que debe ser aplicado para calcular la comisión
	 * @return decimal 
	 */
	abstract public function getConfigPercentage();

	/**
	 * Numero que corresponde al id del tipo de comision en la tabla
	 * sls_commission_types
	 * @return integer
	 */
	public function getCommissionTypeId()
	{
		$commissionName = $this->getCommissionName();
		$commissionType = CommissionType::findByName($commissionName)->first();
		
		if (! $commissionType) {
			throw new CommissionTypeNotFound("Commission Type '$commissionName' not found in commission_types table", 1);
		}

		return $commissionType->id;
	}

	/**
	 * Obtiene el nombre de la comisión basado en el nombre de la clase
	 * @return string
	 */
	public function getCommissionName()
	{
		return str_replace('Commission', '', collect(explode('\\', get_class($this)))->last());
	}

	/**
	 * Obtener datos de la comisión calculada
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function getData()
	{
		return $this->model;
	}

	/**
	 * Obtiene el nombre del modelo de configuración
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function getConfigModel()
	{
		return $this->configModel;
	}

	/**
	 * Corrige el valor del porcentaje de la comisión. Se aplica para los casos en los 
	 * que la comisión no viene de una venta directa sino en cascada, es decir, un 
	 * vendedor "hijo" es el que realizo la venta. Por lo tanto el vendedor que 
	 * hace uso de este método, recibe su porcentaje menos el porcentaje de
	 * su "hijo".
	 *
	 * @param      Seller  $seller  El vendedor hijo
	 *
	 * @return     this  
	 */
	public function fromChild(Seller $sellerChild)
	{
		$percentageChild = (new \App\Sales\CommissionsSeller\CommissionFactory)
			->make($sellerChild, $this->sale)->percentage;
		
		$this->percentage = $this->percentage - $percentageChild;

		/** Por comision en cascada no aplica porcentaje extra por meta de ventas */
		$this->percentageExtra = 0;

		return $this;
	}

	/**
	 * Arroja una excepción para cuando no se encuentra una fila de configuración
	 * @param  array  $fields Array con parametros que no se encontraron en la busqueda
	 * @return App\Sales\CommissionsCompany\Exceptions\ConfigRowNotFound         
	 */
	protected function throwNoConfigRow(array $fields=[])
	{
		$string = '';

		foreach ($fields as $field => $value) {
			$string .= $field . ':' . $value . ' ';
		}

		throw new ConfigRowNotFound('There\'s no configuration row for ' . ($string ? $string : ' this case'));
	}

	/**
	 * Magic method
	 * @param  string $name 
	 * @return mixed       
	 */
	public function __get($name)
	{
		if (property_exists($this, $name)) {
			return $this->$name;
		}

		return false;
	}
}