<?php

namespace App\Sales\CommissionsSeller;

use App\Sales\CommissionsSeller\Commission;
use App\Sales\Models\ConfigFixedSellerCommission;

/**
* La comision del vendedor siempre es fija
*/
class FixedCommission extends Commission
{
	
	protected $configModel = ConfigFixedSellerCommission::class;

	/**
	 * Obtiene el porcentage que debe ser aplicado para calcular la comisión
	 * @return decimal 
	 */
	public function getConfigPercentage($type = '')
	{
		$configRow = $this->configModel->findConfigValues($this->seller->id, $this->sale->company_id)->first();

		if (! $configRow) {
			$this->throwNoConfigRow([
				'company_id' 	=> $this->sale->company_id,
				'seller_id' 	=> $this->sale->seller_id
			]);
		}

		if ($type == 'RN') {
			return $configRow->percentage_rn;
		} else {
			return $configRow->percentage;
		}
	}
	
}