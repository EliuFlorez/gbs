<?php

namespace App\Sales\CommissionsSeller;

use App\Sales\Models\Sale;
use App\Sales\Models\Seller;

class CommissionFactory
{

	protected $namespace = 'App\\Sales\\CommissionsSeller\\';

	public function make(Seller $seller, Sale $sale)
	{
		$commssionClass = $this->namespace . $seller->commissionType->name . 'Commission';
		
		return app()->make($commssionClass, [$sale]);
	}
}