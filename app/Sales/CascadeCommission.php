<?php

namespace App\Sales;

use App\Sales\Models\Sale;
use App\Sales\Models\Seller;
use App\Sales\CommissionsSeller\CommissionFactory;

/**
* Calcula las comisiones en cascada de los vendedores
*/
class CascadeCommission
{
	/**
	 * Venta a partir de la cual se toma el vendedor
	 * @var App\Sales\Models\Sale
	 */
	protected $sale;

	/**
	 * Árbol de vendedores en orden jerárquico
	 * @var array
	 */
	protected $sellersTree = [];

	/**
	 * Arbol de comisiones ya calculadas en orden jerárquico
	 * @var array
	 */
	protected $commissionsTree = [];

	public function __construct(Sale $sale)
	{
		$this->sale = $sale;
		$this->seller = $this->sale->seller;
		$this->loadSellersTree($this->seller);
	}

	public function handle()
	{
		$this->iterateSellersTree();

		return $this;
	}

	/**
	 * Carga el padre de un vendedor
	 *
	 * @param      \App\Sales\Models\Seller  $seller  Vendedor
	 *
	 * @return     self
	 */
	protected function loadSellersTree(Seller $seller)
	{
		array_push($this->sellersTree, $seller);

		if (! $parent = $seller->parent) {
			return $this;
		}
		
		$this->loadSellersTree($parent);
	}

	/**
	 * Recorrer el arbol de vendedores para aplicar las comisiones
	 * @return this 
	 */
	public function iterateSellersTree()
	{
		foreach (array_slice($this->sellersTree, 1) as $key => $seller) {
			
	    	$this->commissionsTree[] = (new CommissionFactory)->make($seller, $this->sale)
	    		->fromChild($this->sellersTree[$key])
	    		->calculate()
	    		->save()
	    		->getData();
		}
		
		return $this;
	}

	public function commissionsTree()
	{
		return $this->commissionsTree;
	}
}