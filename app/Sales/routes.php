<?php

Route::group([
	'prefix' => 'api', 
	'namespace' => 'App\Sales\Controllers', 
	'middleware' => config('novasales.middleware')
], 
function () {

	/**
	 * Ventas
	 */
	Route::resource('sales', 'SalesController', [
		'only' => ['index', 'show', 'store'], 'parameters' => ['sales' => 'id']
	]);

	/**
	 * Vendedores
	 */
	Route::resource('sellers', 'SellersController', [
		'except' => ['create', 'edit'], 'parameters' => ['sellers' => 'id']
	]);
	Route::get('sellers/{id}/sales', 'SellersSalesController@index');
	Route::get('sellers/{id}/commissions', 'SellersCommissionsController@index');
	
	/**
	 * Rutas referentes a compañías
	 */
	Route::resource('companies', 'CompaniesController', [
		'except' => ['create', 'edit'], 'parameters' => ['companies' => 'id']
	]);
	Route::get('companies/{id}/sales', 'CompaniesSalesController@index');
	Route::get('companies/{id}/commissions', 'CompaniesCommissionsController@index');

	/**
	 * Cliente
	 */
	Route::resource('clients', 'ClientsController', [
		'except' => ['create', 'edit'], 'parameters' => ['clients' => 'id']
	]);
	
	/**
	 * Grupos
	 */
	Route::resource('groups', 'GroupsController', [
		'except' => ['create', 'edit'], 'parameters' => ['groups' => 'id']
	]);
	
	/**
	 * Tipos de comisiones
	 */
	Route::get('commissions/types', 'CommissionsTypesController@index');

	/**
	 * Comisiones de compañía
	 */
	Route::get('commissionsCompanies', 'CommissionsCompanyController@index');
	Route::post('commissions/companies', 'CommissionsCompanyController@create');

	/**
	 * Comisiones de vendedor
	 */
	Route::get('commissionsSellers', 'CommissionsSellerController@index');
	Route::post('commissions/sellers', 'CommissionsSellerController@create');
	
	/**
	 * Config routes de compañía
	 */
	Route::group(['prefix' => 'companies'], function() {
		Route::resource('config/fixed', 'ConfigFixedController', [
			'except' => ['create', 'edit'], 'parameters' => ['fixed' => 'id']
		]);
	});

	/**
	 * Config route de vendedores
	 */
	Route::group(['prefix' => 'sellers'], function() {
		Route::resource('config/fixed', 'ConfigFixedSellerController', [
			'except' => ['create', 'edit'], 'parameters' => ['fixed' => 'id']
		]);
	});
	
});