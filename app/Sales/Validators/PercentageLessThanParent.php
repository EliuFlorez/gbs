<?php

namespace App\Sales\Validators;

use App\Sales\Models\Seller;

/**
 * Validador que controla que los porcentajes asignados al vendedor no superen
 * los de su "padre"
 */
class PercentageLessThanParent
{

	/**
	 * Campos que deben tomarse en cuenta al comparar la estructura de la
	 * configuración del vendedor "padre" con la que se está intentando
	 * registrar para el vendedor "hijo"
	 *
	 * @var        array
	 */
	protected $considerFields = ['company_id', 'sale_type_id', 'plan_id', 'service_type_id'];

	/**
	 * Construye el validador
	 *
	 * @param      string     $attribute   Atributo validado
	 * @param      float      $percentage  El porcentaje
	 * @param      array      $parameters  Los parametros
	 * @param      Validator  $validator   Validador
	 *
	 * @return     boolean    Pasa la validación o no
	 */
	public function validate($attribute, $percentage, $parameters, $validator) 
	{
		/**
		 * Si el atributo que se está intentando validar no es 'percentage', se
		 * omite la validación y se asume que pasó la misma
		 */
		if ($attribute != 'percentage') {
			return true;	
		}
		
		$sellerId = array_first($parameters);

		if ( ! $seller = Seller::find($sellerId)) {
			return false;
		}

		if ( ! $parent = $seller->parent) {
			return true;
		}

		$childFields = $this->childFieldsArray($parameters);

		/**
		 * Si no se encuentra ningún modelo de configuración, se asume que no
		 * pasa la validación pues no hay datos con los que comparar el la
		 * configuración y más especificamente el porcentaje que se está
		 * intentando guardar
		 */
		if ( ! $parentConfig = $this->getAppropriateConfigModel($parent, $childFields)) {
			return false;
		}

		return $percentage < $parentConfig->percentage;
    }

    /**
     * Obtiene el nombre del modelo de configuración del vendedor
     *
     * @param      \App\Sales\Models\Seller  $seller  El vendedor
     *
     * @return     string                               Nombre del modelo
     */
    protected function sellerConfigModel(Seller $seller)
    {
    	return '\\App\\Sales\\Models\\Config' . $seller->commissionType->name . 'SellerCommission';
    }

    /**
     * Obtiene el modelo de configuración apropiado. En caso de que se este
     * intentando crear una configuración para el mismo tipo de comisión que el
     * vendedor padre, se toma la configuración que coincida con los valores que
     * se están intentando guardar. Ej. Si se intenta crear una configuración
     * para tipo de venta 1 en la compañía 5 (['sale_type_id' => 1, 'company_id'
     * => 5]), se busca una coincidencia para esos valores en las
     * configuraciones del padre. En caso de que no se trate del mismo tipo de
     * comisión, se toma la configuración con el porcentaje más bajo del
     * vendedor padre.
     *
     * @param      Seller  $parent       El vendedor padre
     * @param      array   $childFields  Los campos del vendedor hijo
     *
     * @return     Model   El modelo de configuración apropiado.
     */
    protected function getAppropriateConfigModel(Seller $parent, array $childFields)
    {
    	$configModelName = $this->sellerConfigModel($parent);
    	$parentConfigs = $configModelName::where('seller_id', $parent->id)
    		->where('company_id', $childFields['company_id'])
    		->get();

		if( ! $parentConfigs->count()) {
			return false;
		}

		$parentKeys = array_divide(
			array_only($parentConfigs->first()->toArray(), $this->considerFields)
		)[0];
		$childKeys = array_divide($childFields)[0];

		if ($childKeys == $parentKeys) {
			return $this->searchConicidentConfig($parentConfigs, $childFields);
		}

		return $this->searchLowerPercentageConfig($parentConfigs);
    }

    /**
     * Busca la configuración que coincida con los parametros proporcionados al
     * validador
     *
     * @param      Collection  $parentConfigs  Las configuraciones del vendedor
     *                                         padre
     * @param      array       $childFields    Los campos del vendedor hijo
     *
     * @return     mixed       Modelo de configuración coincidente o falso si no se encuentra 
     */
    protected function searchConicidentConfig($parentConfigs, array $childFields)
    {
    	foreach ($parentConfigs as $config) {
    		$parentFields = array_only($config->toArray(), $this->considerFields);
    		if ($parentFields == $childFields) {
    			return $config;
    		}
    	}

    	return false;
    }

    /**
     * Busca la configuración que contenga el menor porcentaje
     *
     * @param      Collection  $parentConfigs  Las configuraciones del vendedor padre
     *
     * @return     Model  Modelo de configuración con el menor porcentaje
     */
    protected function searchLowerPercentageConfig($parentConfigs)
    {
    	$sortedConfigs = $parentConfigs->sortBy(function ($config, $key) {
    		return $config->percentage;
    	});

    	return $sortedConfigs->first();
    }

    /**
     * Extrae de los parametros recibidos los campos que se compararán con la
     * configuración del vendedor padre y sus valores
     *
     * @param      array  $parameters  Los parametros
     *
     * @return     array  
     */
    protected function childFieldsArray(array $parameters)
    {
    	$parameters = array_slice($parameters, 1);

    	$count = count($parameters);

    	$childFields = [];

    	for ($i=0; $i < $count; $i = $i + 2) { 
    		$childFields[$parameters[$i]] = $parameters[$i + 1];
    	}
    	return $childFields;
    }
}
