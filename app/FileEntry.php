<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileEntry extends Model
{
	protected $table = 'file_entry';

	protected $fillable = [
		'file_name',
		'file_mime',
		'file_original',
		'file_path',
		'driver',
		'status',
		'table_type',
		'table_id',
		'data',
		'description'
	];

	public function ticketDetailAttach()
	{
		return $this->belongsToMany('App\Moduls\TicketDetail',
			'ticket_attach',
			'file_entry_id',
			'ticket_detail_id'
		);
	}

	public function saveWithDefaults($params)
	{
		foreach( $this->fillable as $field  )
		{
			$val = ($field == 'status') ? 1 : '';
			$this->$field = array_get($params, $field, $val);
		}

		$this->save();
	}

}
