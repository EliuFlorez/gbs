<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = \Carbon\Carbon::now()->toDateTimeString();
		
		$seed = DB::table('permission_role')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('permission_role')->insert([
				[
					'id' => 1,
					'permission_id' => 1,
					'role_id' => 2,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 2,
					'permission_id' => 2,
					'role_id' => 3,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 3,
					'permission_id' => 3,
					'role_id' => 4,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 4,
					'permission_id' => 1,
					'role_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 5,
					'permission_id' => 2,
					'role_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 6,
					'permission_id' => 3,
					'role_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				],
			]);
		}
    }
}
