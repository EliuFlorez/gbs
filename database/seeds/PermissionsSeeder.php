<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = \Carbon\Carbon::now()->toDateTimeString();
		
		$seed = DB::table('permissions')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('permissions')->insert([
				[
					'id' => 1,
					'name' => 'Manage Users',
					'slug' => 'manage.users',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 2,
					'name' => 'Manage Roles',
					'slug' => 'manage.roles',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 3,
					'name' => 'Manage Permissions',
					'slug' => 'manage.permissions',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				],
			]);
		}
    }
}
