<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PlanTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		$seed = DB::table('plan_type')->where('name', 'Individual')->first();
		
		if (empty($seed)) {
			\DB::table('plan_type')->insert([
				array('name' => 'Individual', 'description' => 'Individual', 'member_number' => 1),
				array('name' => '2 Members', 'description' => 'Familia de 2 mienbros', 'member_number' => 2),
				array('name' => 'Familiar', 'description' => 'Toda la familia', 'member_number' => 3)
			]);
		}
	}

}