<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReferenceChildSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Eloquent::unguard();
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');

      DB::table('references_childs')->truncate();

      DB::table('references_childs')->insert([

        /*
        * Option 6
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>63,'price'=>528),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>63,'price'=>280),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>63,'price'=>831),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>63,'price'=>440),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>63,'price'=>1180),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>63,'price'=>625),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>63,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>63,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>63,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>63,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>63,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>63,'price'=>0),

        /*
        * Option 1
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>64,'price'=>406),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>64,'price'=>215),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>64,'price'=>639),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>64,'price'=>339),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>64,'price'=>908),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>64,'price'=>481),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>64,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>64,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>64,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>64,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>64,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>64,'price'=>0),

        /*
        * Option 2
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>65,'price'=>302),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>65,'price'=>160),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>65,'price'=>498),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>65,'price'=>264),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>65,'price'=>706),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>65,'price'=>374),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>65,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>65,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>65,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>65,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>65,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>65,'price'=>0),

        /*
        * Option 3
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>66,'price'=>246),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>66,'price'=>130),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>66,'price'=>403),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>66,'price'=>214),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>66,'price'=>572),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>66,'price'=>303),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>66,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>66,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>66,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>66,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>66,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>66,'price'=>0),

        /*
        * Option 4
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>67,'price'=>209),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>67,'price'=>111),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>67,'price'=>330),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>67,'price'=>175),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>67,'price'=>480),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>67,'price'=>254),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>67,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>67,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>67,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>67,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>67,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>67,'price'=>0),

        /*
        * Option 5
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>68,'price'=>154),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>68,'price'=>82),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>68,'price'=>240),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>68,'price'=>127),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>68,'price'=>352),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>68,'price'=>187),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>68,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>68,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>68,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>68,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>68,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>12,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>68,'price'=>0),

        /*
        * Plan Medical Elite
        */

        /*
        * Option 1
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>40,'price'=>2469),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>40,'price'=>1309),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>40,'price'=>3874),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>40,'price'=>2053),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>40,'price'=>5633),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>40,'price'=>2985),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>40,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>40,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>40,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>40,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>40,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>40,'price'=>0),

        /*
        * Option 2
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>41,'price'=>1573),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>41,'price'=>834),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>41,'price'=>2500),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>41,'price'=>1325),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>41,'price'=>3658),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>41,'price'=>1939),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>41,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>41,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>41,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>41,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>41,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>41,'price'=>0),

        /*
        * Option 3
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>42,'price'=>1168),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>42,'price'=>619),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>42,'price'=>1860),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>42,'price'=>986),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>42,'price'=>2726),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>42,'price'=>1445),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>42,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>42,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>42,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>42,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>42,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>42,'price'=>0),

        /*
        * Option 4
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>43,'price'=>936),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>43,'price'=>496),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>43,'price'=>1491),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>43,'price'=>790),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>43,'price'=>2184),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>43,'price'=>1158),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>43,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>43,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>43,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>43,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>43,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>43,'price'=>0),

        /*
        * Option 5
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>44,'price'=>789),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>44,'price'=>418),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>44,'price'=>1256),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>44,'price'=>666),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>44,'price'=>1841),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>44,'price'=>976),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>44,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>44,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>44,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>44,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>44,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>44,'price'=>0),

        /*
        * Option 6
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>45,'price'=>583),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>45,'price'=>309),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>45,'price'=>932),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>45,'price'=>494),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>45,'price'=>1434),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>45,'price'=>760),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>45,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>45,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>45,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>45,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>45,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>8,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>45,'price'=>0),

        /*
        * Fin Plan Medical Elite
        */

        /*
        * Plan Medical Care Mundial
        */

        /*
        * Option 6
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>57,'price'=>633),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>57,'price'=>335),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>57,'price'=>998),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>57,'price'=>529),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>57,'price'=>1416),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>57,'price'=>750),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>57,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>57,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>57,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>57,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>57,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>57,'price'=>0),

        /*
        * Option 1
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>58,'price'=>487),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>58,'price'=>258),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>58,'price'=>768),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>58,'price'=>407),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>58,'price'=>1089),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>58,'price'=>577),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>58,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>58,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>58,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>58,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>58,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>58,'price'=>0),

        /*
        * Option 2
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>59,'price'=>363),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>59,'price'=>192),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>59,'price'=>597),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>59,'price'=>316),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>59,'price'=>846),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>59,'price'=>448),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>59,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>59,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>59,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>59,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>59,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>59,'price'=>0),

        /*
        * Option 3
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>60,'price'=>295),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>60,'price'=>156),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>60,'price'=>484),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>60,'price'=>257),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>60,'price'=>687),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>60,'price'=>364),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>60,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>60,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>60,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>60,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>60,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>60,'price'=>0),

        /*
        * Option 4
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>61,'price'=>251),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>61,'price'=>133),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>61,'price'=>395),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>61,'price'=>209),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>61,'price'=>575),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>61,'price'=>305),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>61,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>61,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>61,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>61,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>61,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>61,'price'=>0),

        /*
        * Option 5
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>62,'price'=>184),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>62,'price'=>98),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>62,'price'=>289),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>62,'price'=>153),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>62,'price'=>422),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>62,'price'=>224),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>62,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>62,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>62,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>62,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>62,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>11,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>62,'price'=>0),

        /*
        * Fin Plan Medical Care Mundial
        */

        /*
        * Plan Premier Plus
        */

        /*
        * Option 1
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>35,'price'=>1538),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>35,'price'=>815),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>35,'price'=>2420),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>35,'price'=>1283),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>35,'price'=>3520),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>35,'price'=>1866),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>35,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>35,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>35,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>35,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>35,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>35,'price'=>0),
        /*
        * Option 2
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>36,'price'=>1088),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>36,'price'=>577),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>36,'price'=>1711),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>36,'price'=>907),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>36,'price'=>2488),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>36,'price'=>1319),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>36,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>36,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>36,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>36,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>36,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>36,'price'=>0),

        /*
        * Option 3
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>37,'price'=>772),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>37,'price'=>409),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>37,'price'=>1250),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>37,'price'=>663),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>37,'price'=>1903),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>37,'price'=>1009),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>37,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>37,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>37,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>37,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>37,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>37,'price'=>0),

        /*
        * Option 4
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>38,'price'=>694),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>38,'price'=>368),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>38,'price'=>1102),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>38,'price'=>584),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>38,'price'=>1590),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>38,'price'=>843),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>38,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>38,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>38,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>38,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>38,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>38,'price'=>0),

        /*
        * Option 5
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>39,'price'=>507),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>39,'price'=>269),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>39,'price'=>739),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>39,'price'=>392),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>39,'price'=>1312),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>39,'price'=>695),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>39,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>39,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>39,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>39,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>39,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>7,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>39,'price'=>0),

        /*
        * Fin Plan Premier Plus
        */

        /*
        * Plan Global Care
        */

        /*
        * Option 1
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>52,'price'=>751),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>52,'price'=>398),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>52,'price'=>1187),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>52,'price'=>629),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>52,'price'=>1721),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>52,'price'=>912),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>52,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>52,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>52,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>52,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>52,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>52,'price'=>0),

        /*
        * Option 2
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>53,'price'=>578),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>53,'price'=>306),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>53,'price'=>876),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>53,'price'=>464),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>53,'price'=>1272),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>53,'price'=>674),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>53,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>53,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>53,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>53,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>53,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>53,'price'=>0),

        /*
        * Option 3
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>54,'price'=>431),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>54,'price'=>228),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>54,'price'=>683),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>54,'price'=>362),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>54,'price'=>993),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>54,'price'=>526),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>54,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>54,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>54,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>54,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>54,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>54,'price'=>0),

        /*
        * Option 4
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>55,'price'=>362),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>55,'price'=>192),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>55,'price'=>569),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>55,'price'=>302),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>55,'price'=>827),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>55,'price'=>438),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>55,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>55,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>55,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>55,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>55,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>55,'price'=>0),

        /*
        * Option 5
        */
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>56,'price'=>312),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>56,'price'=>165),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>56,'price'=>493),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>56,'price'=>261),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'ANUAL','time'=>'Annual','deductible_id'=>56,'price'=>717),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'SEMI-ANUAL','time'=>'Semiannual','deductible_id'=>56,'price'=>380),

        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>56,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 1, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>56,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>56,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 2, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>56,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'TRIMESTRAL','time'=>'three-Annual','deductible_id'=>56,'price'=>0),
        array('due_year'=>2016,'due_start_date'=>Carbon::create(2016, 4, 1)->format('Y-m-d'),'due_finish_year'=>Carbon::create(2017, 4, 1)->format('Y-m-d'),'plan_id'=>10,'childs' => 3, 'display_time' => 'MENSUAL','time'=>'mensual','deductible_id'=>56,'price'=>0)

        /*
        * Fin Plan Global Care
        */

      ]);

      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
      Eloquent::reguard();
    }
}
