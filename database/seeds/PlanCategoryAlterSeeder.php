<?php

use Illuminate\Database\Seeder;

class PlanCategoryAlterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\DB::statement("ALTER TABLE `plan_category` ADD `company_id` INT(11) NOT NULL AFTER `id`;");
	}
}
