<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$seed = DB::table('type_currency')->where('name', 'usd')->first();
		
		if (empty($seed)) {
			DB::table('type_currency')->insert([
				array('name' => 'usd', 'display_name' => 'USD'),
				array('name' => 'euro', 'display_name' => 'Euro')
			]);
		}
    }
}
