<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(AddressSeeder::class);
        $this->call(BankTypeSeeder::class);
        $this->call(CommissionTypeSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(QuizSeeder::class);
        $this->call(PlanSeeder::class);
		//$this->call(ReferenceChildSeeder::class);
        //$this->call(ReferenceDeductibleSeeder::class);
        //$this->call(ReferencePriceSeeder::class);
        //$this->call(ReferenceSeeder::class);
        //$this->call(ReferencesPrice2016::class);
        //$this->call(ReferenceTimeSeeder::class);
        
        Model::reguard();
    }
}
