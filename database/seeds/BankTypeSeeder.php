<?php

use Illuminate\Database\Seeder;

class BankTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = Carbon\Carbon::now();
		
		$seed = DB::table('bank_account_type')->where('display_name', 'Ahorro')->first();
		
		if (empty($seed)) {
			DB::insert("
				INSERT INTO 
					bank_account_type (display_name, created_at, updated_at) 
				VALUES 
					('Ahorro', '" . $dateTime . "', '" . $dateTime . "'),
					('Corriente', '" . $dateTime . "', '" . $dateTime . "')
			");
		}
    }
}
