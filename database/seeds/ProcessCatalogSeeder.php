<?php

use Illuminate\Database\Seeder;

class ProcessCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		/*
		$carbonNow = Carbon\Carbon::now();
		
        \DB::insert("
		INSERT INTO `process_catalog` (`id`, `name`, `role_id`, `procedure_catalog_id`, `group`, `number_next`, `created_at`, `updated_at`, `deleted_at`, `description`, `icon`, `link`, `display`, `number_last`) VALUES
		(1, 'PolicyInitial', 7, 1, 1, 1, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Inicio del Trámite', 'glyphicon glyphicon-tag', '.policy-init', 1, 0),
		(2, 'SendCheck', 7, 1, 1, 2, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Enviar Cheque Pago', 'glyphicon glyphicon-envelope', '.send-check', 1, 0),
		(3, 'PolicySolicited', 5, 1, 2, 2, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Ingresar Solicitud', 'glyphicon glyphicon-pencil', '.policy-solicited', 1, 0),
		(4, 'PolicyNewBD', 5, 1, 4, 4, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Req App BD', 'glyphicon glyphicon-envelope', '.policy-new-db', 0, 0),
		(5, 'PolicyReceiveBD', 7, 1, 5, 5, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Recibir Póliza BD', 'glyphicon glyphicon-copy', '.policy-receive-bd', 1, 0),
		(6, 'PolicyConfirmBD', 5, 1, 5, 5, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Confirmar Datos Póliza BD', 'glyphicon glyphicon-eye-open', '.policy-confirm-bd', 1, 0),
		(7, 'RegisterResponse', 5, 1, 6, 6, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Respuesta Cliente', 'glyphicon glyphicon-edit', '.register-response', 1, 0),
		(8, 'RegisterPayment', 5, 1, 7, 7, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Registrar Pago', 'glyphicon glyphicon-usd', '.register-payment', 1, 0),
		(9, 'RegisterInvoice', 5, 1, 9, 9, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Confirmación Pago y envío Factura', 'glyphicon glyphicon-send', '.register-invoice', 1, 0),
		(10, 'SendDocsReception', 5, 1, 8, 8, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Enviar Documentos Recepción', 'glyphicon glyphicon-print', '.send-docs-reception', 1, 0),
		(11, 'SendPolicyClient', 7, 1, 10, 10, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Enviar Póliza al Cliente', 'glyphicon glyphicon-send', '.send-policy-client', 1, 0),
		(12, 'UploadPolicySigned', 7, 1, 11, 11, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Cargar Póliza Firmada', 'glyphicon glyphicon-upload', '.updalo-policy-signed', 1, 0),
		(13, 'SendPolicyBD', 7, 1, 12, 12, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Enviar Póliza BD', 'glyphicon glyphicon-send', '.send-policy-bd', 1, 0),
		(14, 'UploadReceipt', 7, 1, 13, 13, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Cargar Acuse Recibido', 'glyphicon glyphicon-upload', '.upload-receipt', 1, 1),
		(15, 'ClaimsInit', 7, 2, 1, 1, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Inicio del Trámite', 'glyphicon glyphicon-upload', '.claim-init', 1, 0),
		(16, 'ClaimsReviewDocuments', 10, 2, 2, 2, '2016-09-12 10:54:57', '2016-10-03 15:41:00', NULL, 'Revisón de Documentos', 'glyphicon glyphicon-edit', 'claim-review-docs', 1, 0),
		(17, 'ClaimsPrintLetter', 10, 2, 3, 3, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Impresión Carta Reclamos', 'glyphicon glyphicon-print', '.claim-print-letter', 1, 0),
		(18, 'ClaimsSendDocsBD', 5, 2, 4, 4, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Envío Reclamo a BD', 'glyphicon glyphicon-envelope', '.claim-send-bd', 1, 0),
		(19, 'ClaimsReceiveReceipt', 5, 2, 5, 5, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Registrar Acuse Recibido', 'glyphicon glyphicon-upload', '.register-receive-receipt', 1, 1),
		(20, 'SettlementInit', 5, 3, 1, 1, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Inicio del Trámite', 'glyphicon glyphicon-upload', '.settlement-init', 0, 0),
		(21, 'SettlementUploadFile', 5, 3, 2, 2, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Subir archivos de liquidacion', 'glyphicon glyphicon-upload', '.settlement-upload-file', 1, 0),
		(22, 'SettlementRegister', 10, 3, 3, 3, '2016-09-12 10:54:57', '2016-10-03 15:41:00', NULL, 'Registar valores liquidacion', 'glyphicon glyphicon-edit', 'settlement-register', 1, 0),
		(23, 'SettlementRefund', 10, 3, 4, 4, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Registrar Pago', 'glyphicon glyphicon-usd', '.settlement-refund', 0, 0),
		(24, 'SettlementFinish', 10, 3, 5, 5, '2016-09-12 10:54:57', '2016-09-12 10:54:57', NULL, 'Cerrar liquidacion', 'glyphicon glyphicon-ok-circle', '.settlement-finish', 0, 1),
		(25, 'RegisterEmergency', 11, 4, 1, 1, '2016-10-03 16:03:39', '2016-10-03 16:03:39', NULL, 'Registrar datos de la Emergencia', 'glyphicon glyphicon-edit', 'register-emergency', 1, 0),
		(26, 'RegisterWarranty', 11, 4, 1, 2, '2016-10-03 16:03:39', '2016-10-03 16:03:39', NULL, 'Registrar recibido de carta de garantia', 'glyphicon glyphicon-list-alt', '.register-warranty', 1, 1);
		*/
    }
}
