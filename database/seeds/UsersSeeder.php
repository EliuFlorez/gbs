<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$seed = DB::table('users')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('users')->insert([
				'id' => 1,
				'name' => 'admin',
				'email' => 'admin@example.com',
				'password' => bcrypt('password'),
				'email_verified' => '1',
				'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
			]);
		}
    }
}
