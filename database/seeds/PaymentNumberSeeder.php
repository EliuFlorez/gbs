<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PaymentNumberSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		$seed = DB::table('payment_number')->where('number', 1)->first();
		
		if (empty($seed)) {
			\DB::table('payment_number')->insert([
				array('name' => 'annual', 'description' => 'Anual', 'number' => 1),
				array('name' => 'biannual', 'description' => 'Semetral', 'number' => 2),
				array('name' => 'quarterly', 'description' => 'Trismetal', 'number' => 4),
				array('name' => 'monthly', 'description' => 'Mensual', 'number' => 12)
			]);
		}
	}

}