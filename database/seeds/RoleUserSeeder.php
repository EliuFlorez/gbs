<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = \Carbon\Carbon::now()->toDateTimeString();
		
		$seed = DB::table('role_user')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('role_user')->insert([
				[
					'id' => 1,
					'role_id' => 1,
					'user_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 2,
					'role_id' => 2,
					'user_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 3,
					'role_id' => 3,
					'user_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 4,
					'role_id' => 4,
					'user_id' => 1,
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				],
			]);
		}
    }
}
