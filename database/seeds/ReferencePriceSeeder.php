<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReferencePriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Eloquent::unguard();
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');

      DB::table('references_prices')->truncate();

          $deductibles = DB::table('references_deductibles')->get();
          $times       = DB::table('references_times')->get();
          $childs      = DB::table('references_childs')->get();

          foreach($deductibles as $deductible){
            foreach($times as $time){
              foreach($childs as $child){
                DB::table('references_prices')->insert([
                  array('plan_id' => 5, 'deductible_id' => $deductible->id,'childs_id'=>$child->id,'time_id'=>$time->id),
                ]);
              }
            }
          }



      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
      Eloquent::reguard();
    }
}
