<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReferenceDeductibleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Eloquent::unguard();
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');

      DB::table('references_deductibles')->truncate();

      DB::table('references_deductibles')->insert([
        array('plan_id'=>11,'inside_usa_price' => 1000, 'outside_usa_price' => 1000,'deductible_option'=>'1'),
        array('plan_id'=>11,'inside_usa_price' => 2000, 'outside_usa_price' => 2000,'deductible_option'=>'2'),
        array('plan_id'=>11,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'3'),
        array('plan_id'=>11,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'4'),
        array('plan_id'=>11,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'5'),
        array('plan_id'=>11,'inside_usa_price' => 50000, 'outside_usa_price' => 50000,'deductible_option'=>'6')
      ]);

      DB::table('references_deductibles')->insert([
        array('plan_id'=>12,'inside_usa_price' => 1000, 'outside_usa_price' => 1000,'deductible_option'=>'2'),
        array('plan_id'=>12,'inside_usa_price' => 2000, 'outside_usa_price' => 2000,'deductible_option'=>'2'),
        array('plan_id'=>12,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'3'),
        array('plan_id'=>12,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'4'),
        array('plan_id'=>12,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'5'),
        array('plan_id'=>12,'inside_usa_price' => 50000, 'outside_usa_price' => 50000,'deductible_option'=>'6')
      ]);

      DB::table('references_deductibles')->insert([
        array('plan_id'=>8,'inside_usa_price' => 1000, 'outside_usa_price' => 500,'deductible_option'=>'1'),
        array('plan_id'=>8,'inside_usa_price' => 2000, 'outside_usa_price' => 1000,'deductible_option'=>'2'),
        array('plan_id'=>8,'inside_usa_price' => 3000, 'outside_usa_price' => 2000,'deductible_option'=>'3'),
        array('plan_id'=>8,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'4'),
        array('plan_id'=>8,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'5'),
        array('plan_id'=>8,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'6')
      ]);

      DB::table('references_deductibles')->insert([
        array('plan_id'=>7,'inside_usa_price' => 1250, 'outside_usa_price' => 500,'deductible_option'=>'1'),
        array('plan_id'=>7,'inside_usa_price' => 2500, 'outside_usa_price' => 1000,'deductible_option'=>'2'),
        array('plan_id'=>7,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'3'),
        array('plan_id'=>7,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'4'),
        array('plan_id'=>7,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'5')
      ]);

      DB::table('references_deductibles')->insert([
        array('plan_id'=>9,'inside_usa_price' => 1000, 'outside_usa_price' => 500,'deductible_option'=>'1'),
        array('plan_id'=>9,'inside_usa_price' => 2000, 'outside_usa_price' => 1000,'deductible_option'=>'2'),
        array('plan_id'=>9,'inside_usa_price' => 3000, 'outside_usa_price' => 2000,'deductible_option'=>'3'),
        array('plan_id'=>9,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'4'),
        array('plan_id'=>9,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'5'),
        array('plan_id'=>9,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'6')
      ]);

      DB::table('references_deductibles')->insert([
        array('plan_id'=>10,'inside_usa_price' => 1000, 'outside_usa_price' => 500,'deductible_option'=>'1'),
        array('plan_id'=>10,'inside_usa_price' => 2000, 'outside_usa_price' => 2000,'deductible_option'=>'2'),
        array('plan_id'=>10,'inside_usa_price' => 5000, 'outside_usa_price' => 5000,'deductible_option'=>'3'),
        array('plan_id'=>10,'inside_usa_price' => 10000, 'outside_usa_price' => 10000,'deductible_option'=>'4'),
        array('plan_id'=>10,'inside_usa_price' => 20000, 'outside_usa_price' => 20000,'deductible_option'=>'5')
      ]);

      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
      Eloquent::reguard();
    }
}
