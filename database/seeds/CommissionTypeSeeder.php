<?php

use Illuminate\Database\Seeder;

class CommissionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = Carbon\Carbon::now();
		
		$seed = DB::table('sls_commission_types')->where('name', 'Fixed')->first();
		
		if (empty($seed)) {
			DB::insert("
				INSERT INTO 
					sls_commission_types (name, display_name, created_at, updated_at) 
				VALUES 
					('Fixed', 'Comisión fija', '" . $dateTime . "', '" . $dateTime . "'),
					('SaleType', 'Comisión por tipo de venta', '" . $dateTime . "', '" . $dateTime . "'),
					('Plan', 'Comisión por plan vendido', '" . $dateTime . "', '" . $dateTime . "'),
					('PlanAndSaleType', 'Comisión por plan vendido y tipo de venta', '" . $dateTime . "', '" . $dateTime . "'),
					('ServiceType', 'Comisión por el tipo de servicio vendido', '" . $dateTime . "', '" . $dateTime . "');
			");
		}
    }
}
