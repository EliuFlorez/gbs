<?php

use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		$seed = DB::table('address_country')->where('name', 'Ecuador')->first();
		
		if (empty($seed)) {
			DB::table('address_country')->insert([
				'name' => 'Ecuador',
				'iso2' => 'EC',
				'iso3' => 'ECU',
				'code' => '218'
			]);

			DB::table('address_state')->insert([
				array('name' => 'Guayas', 'country_id' => 1),
				array('name' => 'Pichincha', 'country_id' => 1)
			]);

			DB::table('address_city')->insert([
				array('name' => 'Guayaquil', 'state_id' => 1),
				array('name' => 'Quito', 'state_id' => 2)
			]);
		}
    }
}
