<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dateTime = \Carbon\Carbon::now()->toDateTimeString();
		
		$seed = DB::table('roles')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('roles')->insert([
				[
					'id' => 1,
					'name' => 'Super Admin',
					'slug' => 'admin.super',
					'description' => 'Super Admin',
					'level' => '1',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 2,
					'name' => 'User Admin',
					'slug' => 'admin.user',
					'description' => 'Can manage users',
					'level' => '1',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 3,
					'name' => 'Role Admin',
					'slug' => 'admin.role',
					'description' => 'Can manage user roles',
					'level' => '1',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				], [
					'id' => 4,
					'name' => 'Permission Admin',
					'slug' => 'admin.permission',
					'description' => 'Can manage permissions',
					'level' => '1',
					'created_at' => $dateTime,
					'updated_at' => $dateTime,
				],
			]);
		}
    }
}
