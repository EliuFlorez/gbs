<?php

use Illuminate\Database\Seeder;

class PaymentAlterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\DB::statement("ALTER TABLE `payment_number` CHANGE `display` `name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
	}
}
