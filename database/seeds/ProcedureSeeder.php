<?php

use Illuminate\Database\Seeder;

class ProcedureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$seed = DB::table('plan_category')->where('name', 'BDIL')->first();
		
		if (empty($seed)) {
			\DB::insert("
				INSERT INTO 
					procedure_catalog (name, description) 
				VALUES 
					('policy', 'New Policy'),
					('claims', 'Reclamos'),
					('settlement', 'Liquidaciones'),
					('service', 'Servicio al Cliente')
			");
		}
    }
}
