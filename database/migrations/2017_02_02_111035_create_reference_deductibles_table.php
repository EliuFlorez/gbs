<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReferenceDeductiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::create('references_deductibles', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('inside_usa_price')->unsigned();
			$table->integer('outside_usa_price')->unsigned();
			$table->integer('plan_id')->unsigned()->nullable();
			$table->string('deductible_option');
			$table->timestamps();
			
			$table->foreign('plan_id')->references('id')->on('plan')
				->onDelete('cascade')->onUpdate('cascade');
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('references_deductibles');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
