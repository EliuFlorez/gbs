<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcedureServiceTable extends Migration {

	public function up()
	{
		Schema::create('procedure_service', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->string('category')->default('general');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	public function down()
	{
		Schema::drop('procedure_service');
	}

}
