<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('policy_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender', 1);
            $table->date('brithday');
			$table->string('email');
            $table->string('height');
            $table->string('weight');
			$table->text('data');
			$table->date('effective_date');
            $table->date('dismiss_date')->nullable();
            $table->decimal('premium_amount', 15, 2);
            $table->tinyInteger('role');
			
			$table->text('extra');
			$table->text('annexe');
			
            $table->timestamps();
            $table->softDeletes();
			
			$table->foreign('policy_id')
				->references('id')->on('policy');
        });
		
		Schema::create('affiliate_deducible', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('plan_deducible_type_id')->unsigned();
            $table->integer('affiliate_id')->unsigned();
            $table->float('amount')->unsigned();

            $table->foreign('plan_deducible_type_id')
                    ->references('id')->on('plan_deducible_type')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->foreign('affiliate_id')
                    ->references('id')->on('affiliate')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('affiliate_deducible');
        Schema::drop('affiliate');
    }
}
