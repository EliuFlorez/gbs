<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReferenceTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::create('references_times', function (Blueprint $table) {
			$table->increments('id');
			$table->string('display_time');
			$table->string('time');
			$table->integer('plan_id')->unsigned()->nullable();
			$table->integer('deductible_id')->unsigned();
			$table->integer('min_age')->unsigned();
			$table->integer('max_age')->unsigned();
			$table->float('price', 8, 2);
			$table->integer('due_year')->nullable();
			$table->date('due_start_date')->nullable();
			$table->date('due_finish_year')->nullable();
			$table->timestamps();

			$table->foreign('plan_id')->references('id')->on('plan')
				->onDelete('cascade')->onUpdate('cascade');
				
			$table->foreign('deductible_id')->references('id')->on('references_deductibles')
				->onDelete('cascade')->onUpdate('cascade');

		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('references_times');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
