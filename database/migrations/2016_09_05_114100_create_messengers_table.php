<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessengersTable extends Migration {

	public function up()
	{
		Schema::create('messengers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('type');
			$table->string('dni');
			$table->string('dni_type');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	public function down()
	{
		Schema::drop('messengers');
	}

}
