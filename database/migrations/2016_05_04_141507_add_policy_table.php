<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy', function (Blueprint $table) {
			$table->increments('id');
			$table->string('policy_number');
			$table->integer('seller_id')->unsigned();
			$table->integer('plan_type_id')->unsigned();
			$table->integer('plan_deducible_id')->unsigned();
			$table->integer('payment_number_id')->unsigned();
			$table->integer('emision_number')->unsigned();
			$table->integer('endoso_number')->unsigned();
			$table->integer('renewal_number')->unsigned();
			$table->date('start_date');
			$table->date('end_date');
			$table->date('endoso_date')->nullable();
			$table->date('emision_date');
			$table->integer('plan_type')->unsigned();
			$table->integer('parent_id')->unsigned()->nullable();
			$table->string('state');

			$table->foreign('seller_id')
				->references('id')->on('sls_sellers');
				
			$table->foreign('plan_type_id')
				->references('id')->on('plan_type');
				
			$table->foreign('plan_deducible_id')
				->references('id')->on('plan_deducible');

			$table->foreign('payment_number_id')
				->references('id')->on('payment_number');

			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
