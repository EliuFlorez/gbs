<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorSpecialtyTable extends Migration {

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('doctor_specialty', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("specialty_id")->unsigned();
			$table->integer("doctor_id")->unsigned();
			$table->timestamps();
			
			$table->foreign('specialty_id')
			->references('id')->on('specialty')
			->onUpdate('cascade')
			->onDelete('restrict');
			
			$table->foreign('doctor_id')
			->references('id')->on('doctor')
			->onUpdate('cascade')
			->onDelete('restrict');
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('doctor_specialty');
	}

}
