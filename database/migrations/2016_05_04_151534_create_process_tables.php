<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_catalog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procedure_catalog_id')->unsigned();
			$table->string('name');
			$table->string('icon', 60);
            $table->string('link', 250);
			$table->integer('display')->default(1);
			$table->string('description', 250)->nullable();
            $table->integer('role_id')->unsigned();
            $table->integer('group');
            $table->integer('number_next');
            $table->integer('number_last');
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('role_id')
                        ->references('id')->on('roles')
                        ->onUpdate('cascade')
                        ->onDelete('restrict');
						
            $table->foreign('procedure_catalog_id')
                        ->references('id')->on('procedure_catalog')
                        ->onUpdate('cascade')
                        ->onDelete('restrict');
        });

        Schema::create('process_entry', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('process_catalog_id')->unsigned();
            $table->integer('procedure_entry_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable()->default(null);
            $table->string('state');
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('process_catalog_id')
                        ->references('id')->on('process_catalog')
                        ->onUpdate('cascade')
                        ->onDelete('restrict');
						
            $table->foreign('procedure_entry_id')
                        ->references('id')->on('procedure_entry')
                        ->onUpdate('cascade')
                        ->onDelete('restrict');
						
            $table->foreign('user_id')
                        ->references('id')->on('users')
                        ->onUpdate('cascade')
                        ->onDelete('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('process_entry');
        Schema::drop('process_catalog');        
    }
}
