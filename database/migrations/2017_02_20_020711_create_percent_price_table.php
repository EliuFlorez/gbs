<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePercentPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::create('references_percent', function (Blueprint $table) {
			$table->increments('id');

			$table->float('percent', 8, 2);
			$table->integer('due_year')->nullable();
			$table->date('due_start_date')->nullable();
			$table->date('due_finish_year')->nullable();
			$table->integer('plan_id')->unsigned();
			$table->timestamps();
			
			$table->foreign('plan_id')->references('id')->on('plan')
				->onDelete('cascade')->onUpdate('cascade');
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('references_percent');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
