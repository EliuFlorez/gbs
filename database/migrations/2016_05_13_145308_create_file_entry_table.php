<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_entry', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('file_name');
                $table->string('file_mime');
                $table->string('file_original');
                $table->string('file_path');
                $table->string('driver');
				$table->tinyInteger('status')->default(1);
				$table->string('table_type');
                $table->integer('table_id')->unsigned();
				$table->text('data')->nullable()->default('');
                $table->string('description');
                $table->timestamps();
				$table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_entry');
    }

}
