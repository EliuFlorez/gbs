<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy', function (Blueprint $table) {
			$table->increments('id');
			$table->string('policy_number');
			$table->integer('agent_id')->unsigned();
			$table->integer('plan_type_id')->unsigned();
			$table->integer('plan_deducible_id')->unsigned();
			$table->integer('payment_number_id')->unsigned();
			$table->integer('emision_number')->unsigned();
			$table->integer('endoso_number')->unsigned();
			$table->integer('renewal_number')->unsigned();
			$table->date('start_date');
			$table->date('end_date');
			$table->date('endoso_date')->nullable();
			$table->date('emision_date');
			$table->integer('plan_type')->unsigned();
			$table->integer('parent_id')->unsigned()->nullable();
			$table->string('state');

			$table->foreign('agent_id')
				->references('id')->on('agent');
				
			$table->foreign('plan_type_id')
				->references('id')->on('plan_type');
				
			$table->foreign('plan_deducible_id')
				->references('id')->on('plan_deducible');

			$table->foreign('payment_number_id')
				->references('id')->on('payment_number');

			$table->timestamps();
			$table->softDeletes();
        });
		
		Schema::create('policy_renovations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('policy_number');
			$table->text('clients');
            $table->text('payments');
			$table->text('payment_status');
            $table->text('results');
            $table->text('file');
            $table->timestamps();
            $table->softDeletes();
        });
		
		Schema::create('policy_prev', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('policy_id')->unsigned();
            $table->string('company_name');
            $table->string('plan_name');
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('policy_id')
                    ->references('id')->on('policy')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
		
		Schema::create('policy_discount', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('policy_id')->unsigned();
            $table->string('concept');
            $table->float('percentage');
            $table->tinyInteger('state'); //0 - requested
                                          //1 - applied
                                          //2 - cancelled
            $table->timestamps();
            $table->softDeletes();
        });
		/*
		Schema::create('policy_deducible', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('policy_id')->unsigned();
            $table->integer('plan_deducible_type_id')->unsigned();
            $table->float('amount')->unsigned();

            $table->foreign('policy_id')
                    ->references('id')->on('policy')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->foreign('plan_deducible_type_id')
                    ->references('id')->on('plan_deducible_type')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->timestamps();
            $table->softDeletes();
        });
		*/
		Schema::create('policy_cost', function(Blueprint $table)
        {
            $table->increments('id');
			$table->integer('policy_id');
            $table->float('total'); //value pay
            $table->date('date_paidoff');
            $table->integer('state'); //0 paidoff, 1 pending , 2 cancelled
            $table->integer('quote_number');
			$table->string("cancel_reason");
            $table->timestamps();
            $table->softDeletes();
        });
		
		Schema::create('policy_cost_detail', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('policy_cost_id')->unsigned();
			$table->string('concept');
            $table->float('value');
			$table->float("value_origen");
			$table->tinyInteger('is_discount')->default(0);
            $table->tinyInteger('is_commission')->default(1);
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('policy_cost_id')
				->references('id')
				->on('policy_cost')
				->onUpdate('cascade')
				->onDelete('restrict');
        });
		
		Schema::create('policy_cost_tax_fees', function(Blueprint $table)
        {
            $table->increments('id');
			$table->integer('policy_cost_id')->unsigned();
            $table->string('concept');
            $table->float('value');
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('policy_cost_id')
				->references('id')
				->on('policy_cost')
				->onUpdate('cascade')
				->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policy_cost_tax_fees');
        Schema::drop('policy_cost_detail');
        Schema::drop('policy_cost');
        //Schema::drop('policy_deducible');
        Schema::drop('policy_discount');
        Schema::drop('policy_renovations');
        Schema::drop('policy_prev');
        //Schema::drop('policy');
    }
}
