<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('identity_document');
            $table->date('brithday');
            $table->string('email');
            $table->string('skype');
            $table->string('mobile');
            $table->string('phone');
            $table->integer("country_id");
			$table->integer("province_id");
			$table->integer("city_id");
            $table->string('address');
            //true or false, indicate if the agent is onder other agent
            $table->boolean('sub_agent');
            $table->smallInteger('commission');
            //if the agent is under other agent, this field hold the id of the agnet over him
            $table->integer('leader')->unsigned()->nullable()->default(NULL);
            $table->foreign('leader')
                    ->references('id')->on('agent')
                    ->onDelete('restrict');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent');
    }

}
