<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorTable extends Migration {

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('doctor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer("hospital_id")->unsigned();
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('hospital_id')
                    ->references('id')->on('hospital')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('doctor');
	}

}
