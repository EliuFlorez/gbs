<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('quiz', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code');
			$table->string('description');
			$table->integer('company_id')->unsigned();
			$table->integer('parent_id')->unsigned()->default(0);
			$table->timestamps();
			$table->softDeletes();

			$table->unique(['code', 'company_id']);

			$table->foreign('company_id')->references('id')->on('company')
				->onUpdate('cascade')->onDelete('restrict');
		});

		Schema::create('quiz_item', function (Blueprint $table) {
			$table->increments('id');
			$table->string('description');
			$table->enum('resp_type', ['bool', 'text']);
			$table->integer('quiz_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('quiz_id')->references('id')->on('quiz')
				->onUpdate('cascade')->onDelete('restrict');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quiz_item');
        Schema::drop('quiz');
    }
}
