<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('payment', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payment_method_id')->unsigned();
            $table->float('amount');
            $table->date('date');
            $table->smallInteger('file_uploaded'); //1 to indicate a file was updated, 0 otherwise
            $table->timestamps();
        });
		
		Schema::create('payment_method', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('method');
			$table->string('display');
            $table->timestamps();
        });
		
        //monthly, anual, semianual, quarterly
        Schema::create('payment_number', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('number');
            $table->string('display');
			$table->string('description');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('payment_number');
        Schema::drop('payment_method');
		Schema::drop('payment');
    }

}
