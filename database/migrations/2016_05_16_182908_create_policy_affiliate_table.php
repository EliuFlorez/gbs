<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyAffiliateTable extends Migration
{
    public function up()
    {
		Schema::create('policy_affiliate', function (Blueprint $table) 
		{
			$table->increments('id');
			$table->integer('affiliate_id')->unsigned();
			$table->integer('policy_id')->unsigned();
			$table->date('effective_date');
			$table->date('dismiss_date')->nullable();
			$table->decimal('premium_amount', 15, 2);
			$table->tinyInteger('role');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('affiliate_id')->references('id')->on('affiliate')
			->onUpdate('cascade')->onDelete('restrict');

			$table->foreign('policy_id')->references('id')->on('policy')
			->onUpdate('cascade')->onDelete('restrict');
		});
    }

    public function down()
    {
        Schema::drop('policy_affiliate');
    }
}
