<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReferencePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::create('references_prices', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('plan_id')->unsigned();
			$table->integer('deductible_id')->unsigned();
			$table->integer('childs_id')->unsigned();
			$table->integer('time_id')->unsigned();
			$table->timestamps();

			$table->foreign('plan_id')->references('id')->on('plan')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('deductible_id')->references('id')->on('references_deductibles')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('childs_id')->references('id')->on('references_childs')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('time_id')->references('id')->on('references_time')
				->onDelete('cascade')->onUpdate('cascade');
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('references_prices');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
