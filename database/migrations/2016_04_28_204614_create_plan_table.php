<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('plan_category', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('display_name');
            $table->timestamps();
            $table->softDeletes();
        });
		
		Schema::create('plan_deducible_type', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
			$table->timestamps();
            $table->softDeletes();
        });
		
        Schema::create('plan', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('company_id')->unsigned();
			$table->integer('plan_category_id')->unsigned()->nullable();
			
            $table->foreign('company_id')
				->references('id')->on('company')
				->onDelete('Cascade');
				
			$table->foreign('plan_category_id')
				->references('id')->on('plan_category')
				->onDelete('Cascade');
				
            $table->softDeletes();
            $table->timestamps();
        });
		
        Schema::create('plan_deducible', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('plan_id')->unsigned();
			
            $table->foreign('plan_id')
				->references('id')->on('plan')
				->onDelete('Cascade');
				
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('plan_deducible_options', function(Blueprint $table)
        {
            $table->increments('id');
            $table->float('value');
            $table->integer('plan_deducible_id')->unsigned();
			$table->integer('plan_deducible_type_id')->unsigned()->nullable();
			
            $table->foreign('plan_deducible_id')
				->references('id')->on('plan_deducible')
				->onDelete('Cascade');
				
			$table->foreign('plan_deducible_type_id')
				->references('id')->on('plan_deducible_type')
				->onUpdate('cascade')
				->onDelete('restrict');
				
            $table->softDeletes();
            $table->timestamps();
        });

		Schema::create('plan_deducible_cover', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('plan_deducible_id')->unsigned();
            $table->tinyInteger('require_all_members'); //
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('plan_deducible_id')
                  ->references('id')->on('plan_deducible')
                  ->onDelete('Cascade');
        });
		
		Schema::create('plan_deducible_value', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('plan_deducible_cover_id')->unsigned();
            $table->float('value');
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('plan_deducible_cover_id')
                  ->references('id')->on('plan_deducible_cover')
                  ->onDelete('Cascade');
        });
		
        //individual, familiar, 2 members
        Schema::create('plan_type', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
			$table->tinyInteger("member_number");
            $table->softDeletes();
            $table->timestamps();
        });

        //esto sirve en especial en best doctors para calcular si ha habido
        //algun cambio en los valores del plan del que haya que notificar
        Schema::create('plan_range_age', function(Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('start');
            $table->tinyInteger('end');
            $table->integer('plan_id')->unsigned();
			
            $table->foreign('plan_id')
				->references('id')->on('plan')
				->onDelete('Cascade');
				
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('plan_cost', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('plan_deducible_id')->unsigned();
            $table->integer('plan_type_id')->unsigned();
            $table->integer('payment_number_id')->unsigned();
            $table->integer('start_age');
            $table->integer('end_age');
            $table->float('value');
			$table->date("from_date");
            $table->date("to_date");
			
            $table->foreign('plan_deducible_id')
				->references('id')->on('plan_deducible')
				->onDelete('Cascade');
					
            $table->foreign('plan_type_id')
				->references('id')->on('plan_type')
				->onDelete('Cascade');
					
            $table->foreign('payment_number_id')
				->references('id')->on('payment_number')
				->onDelete('Cascade');
					
            $table->unique([
				'plan_deducible_id',
				'plan_type_id',
				'payment_number_id',
				'start_age',
				'end_age'
			], 'values_payment_ukey');
			
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('plan_cost_extra', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('reason');
            $table->integer('plan_deducible_id')->unsigned();
            $table->integer('payment_number_id')->unsigned();
            $table->string('reason_value');
            $table->float('value');
			
            $table->foreign('plan_deducible_id')
				->references('id')->on('plan_deducible')
				->onDelete('Cascade');
					
            $table->foreign('payment_number_id')
				->references('id')->on('payment_number')
				->onDelete('Cascade');
					
            $table->unique([
				'plan_deducible_id',
				'payment_number_id',
				'reason'
			], 'values_payment_ukey');
			
            $table->timestamps();
			$table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('plan_deducible_value');
        Schema::drop('plan_deducible_cover');
		Schema::drop('plan_deducible_options');
		Schema::drop('plan_deducible_type');
		
		Schema::drop('plan_cost_extra');
        Schema::drop('plan_cost');
        Schema::drop('plan_range_age');
		
		Schema::drop('policy');
		Schema::drop('payment_number');
		
        Schema::drop('plan_type');
        Schema::drop('plan_deducible');
        Schema::drop('plan');
		Schema::drop('plan_category');
    }

}
