<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsSettlementsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('claim', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('policy_id')->unsigned();
			$table->integer('type_diagnosis_id')->unsigned()->nullable();
			$table->tinyInteger('status')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('policy_id')->references('id')->on('policy')
				->onUpdate('cascade')->onDelete('restrict');
				
			$table->foreign('type_diagnosis_id')->references('id')->on('type_diagnosis')
				->onUpdate('cascade')->onDelete('restrict');
		});

		Schema::create('claim_concept', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('display_name');
            $table->boolean('notify')->default(0);
			$table->float('deduct_discount')->default(0);
            $table->timestamps();
			$table->softDeletes();
        });
		
      Schema::create('claim_procedure', function(Blueprint $table)
      {
          $table->increments('id');
          $table->integer('claim_id')->unsigned();
          $table->integer('procedure_entry_id')->unsigned();

          $table->foreign('claim_id')
                  ->references('id')->on('claim')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->foreign('procedure_entry_id')
                  ->references('id')->on('procedure_entry')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->timestamps();
			$table->softDeletes();
      });

      Schema::create('claim_file', function(Blueprint $table)
      {
          $table->increments('id');
          $table->string('description');
          $table->integer('file_entry_id')->unsigned()->nullable();
          $table->integer('claim_id')->unsigned();
          $table->integer('procedure_document_id')->unsigned();
          $table->integer('supplier_id')->unsigned()->nullable();
		  $table->integer('claim_concept_id')->unsigned()->nullable();
		  $table->integer('type_currency_id')->unsigned()->nullable();
          $table->boolean('usa')->default(0);

          $table->date('date_invoice')->nullable();
          $table->float('amount')->unsigned()->default(0);
          $table->string('concept')->nullable();
		  $table->tinyInteger('prev_order')->default(0);
		  $table->tinyInteger('invalid')->default(0);

          $table->foreign('claim_id')
                  ->references('id')->on('claim')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->foreign('file_entry_id')
                  ->references('id')->on('file_entry')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->foreign('procedure_document_id')
                  ->references('id')->on('procedure_document')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
	
          $table->foreign('claim_concept_id')
                  ->references('id')->on('claim_concept')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');		  
			
          $table->foreign('type_currency_id')
                  ->references('id')->on('type_currency')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->timestamps();
			$table->softDeletes();
      });
		
      Schema::create('claim_settlement', function(Blueprint $table)
      {
          $table->increments('id');
          $table->integer('claim_file_id')->unsigned();
		  $table->float('amount');
		  $table->float('expected_deduct');
          $table->float('expected_refund');
          $table->float('uncovered_value');
          $table->float('descuento');
          //que valor de la factura va para cubrir el deducible
          $table->float('deducible');
          $table->float('coaseguro');
          $table->float('refunded');
          $table->string('notes');
          $table->date('serv_date');
		  $table->string('ic_num_claim');
          $table->tinyInteger('status')->default(0);

          $table->foreign('claim_file_id')
                  ->references('id')->on('claim_file')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->timestamps();
			$table->softDeletes();
      });

      Schema::create('claim_settlement_refund', function(Blueprint $table)
      {
          $table->increments('id');
          $table->float('value');
          $table->integer('payment_method_id')->unsigned();
          $table->integer('claim_settlement_id')->unsigned();
          $table->boolean('to_supplier');
		  $table->date('pay_date');
		  $table->string('reference_number');

          $table->foreign('claim_settlement_id')
                  ->references('id')->on('claim_settlement')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->foreign('payment_method_id')
                  ->references('id')->on('payment_method')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

          $table->timestamps();
			$table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('claim_file');
        Schema::dropIfExists('claim_procedure');
        Schema::dropIfExists('claim_concept');
        Schema::dropIfExists('claim');
        Schema::dropIfExists('claim_settlement');
        Schema::dropIfExists('claim_settlement_refund');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
