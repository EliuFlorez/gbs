<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteCodeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_code', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('table_type');
            $table->integer('table_id')->unsigned();
            $table->string('value');
            $table->integer('company_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
			
            $table->foreign('company_id')
                    ->references('id')->on('company')
                    ->onDelete('Cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quote_code');
    }

}
