<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalSpecialtyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_specialty', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("specialty_id")->unsigned();
			$table->integer("hospital_id")->unsigned();
			$table->timestamps();
			
            $table->foreign('specialty_id')
                    ->references('id')->on('specialty')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            
            $table->foreign('hospital_id')
                    ->references('id')->on('hospital')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital_specialty');
    }

}
