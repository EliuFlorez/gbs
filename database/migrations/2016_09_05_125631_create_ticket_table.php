<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('table_type');
            $table->integer('table_id');
            $table->integer('ticket_cat_id')->unsigned();
			$table->integer('policy_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->date('start_date');
            $table->date('end_date')->nullable();
            $table->text('short_desc');
			$table->timestamps();
			$table->softDeletes();
			
            $table->foreign('ticket_cat_id')
                  ->references('id')->on('ticket_cat')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('policy_id')
                  ->references('id')->on('policy')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });

        Schema::create('ticket_detail',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
			$table->integer("ticket_id")->unsigned();
			$table->text('comment');
			$table->text('extra_data')->nullable();
            $table->tinyinteger('type');
			$table->timestamps();
			$table->softDeletes();
			
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
			
			$table->foreign('ticket_id')
				->references('id')->on('ticket')
				->onUpdate('cascade')
				->onDelete('restrict');
        });
        
        Schema::create('ticket_attach',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('file_entry_id')->unsigned();
            $table->integer('ticket_detail_id')->unsigned();
			$table->string('description');
			$table->timestamps();
			$table->softDeletes();
			
            $table->foreign('file_entry_id')
                  ->references('id')->on('file_entry')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('ticket_detail_id')
                  ->references('id')->on('ticket_detail')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_attach');
		Schema::drop('ticket_detail');
		Schema::drop('ticket');
    }

}
