<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalizationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('hospitalization_type', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });
		
        Schema::create('hospitalizations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('hospitalization_type_id')->unsigned();
			$table->integer('policy_id')->unsigned();
			$table->integer('doctor_id')->unsigned();
			$table->integer('hospital_id')->unsigned();
			$table->integer('specialty_id')->unsigned();
			$table->integer('diagnosis_id')->unsigned();
            $table->integer('procedure_entry_id')->unsigned();
			$table->integer('ticket_id')->unsigned()->nullable();
			$table->text('process');
			$table->string('form')->nullable();
            $table->string('report')->nullable();
            $table->timestamps();
            $table->softDeletes();
			
            $table->foreign('hospitalization_type_id')
                  ->references('id')->on('hospitalization_type')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('policy_id')
                  ->references('id')->on('policy')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('doctor_id')
                  ->references('id')->on('doctor')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('hospital_id')
                  ->references('id')->on('hospital')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('specialty_id')
                  ->references('id')->on('specialty')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('diagnosis_id')
                  ->references('id')->on('type_diagnosis')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

            $table->foreign('procedure_entry_id')
                  ->references('id')->on('procedure_entry')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

            $table->foreign('ticket_id')
                  ->references('id')->on('ticket')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospitalizations');
        Schema::drop('hospitalization_type');
    }

}
