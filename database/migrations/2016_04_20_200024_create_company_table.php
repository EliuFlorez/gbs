<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('representative');
            $table->string('type');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('company_email', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('email');
            $table->string('contact_name');
            $table->string('reason'); //sale, support, emision, renovation,
			$table->string('subject');
            $table->text('template');
            $table->integer('company_id')->unsigned();
            $table->timestamps();
			
            $table->foreign('company_id')
                    ->references('id')->on('company')
                    ->onDelete('cascade');
					
            $table->softDeletes();
        });

        Schema::create('company_office', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('office_name');
            $table->string('representative');
            $table->string('email'); //contact email of the sucursal
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('address');
            $table->boolean('default'); //this is the default information we get
            $table->integer('company_id')->unsigned();
            $table->timestamps();
			
            $table->foreign('company_id')
                    ->references('id')->on('company')
					->onDelete('cascade');
					
            $table->softDeletes();
        });

        Schema::create('company_phone', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('number');
            $table->boolean('default');
            $table->integer('company_office_id')->unsigned();
            $table->timestamps();
			
            $table->foreign('company_office_id')
                    ->references('id')->on('company_office')
                    ->onDelete('cascade');
					
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_email');
        Schema::drop('company_phone');
		Schema::drop('company_office');
		Schema::drop('company');
    }

}
