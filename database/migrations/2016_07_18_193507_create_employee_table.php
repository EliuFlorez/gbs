<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('user_id')->unsigned();
            $table->string('document_id')->unique();
			$table->timestamps();
            $table->softDeletes();
			
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
		
		Schema::create('employee_department', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('type_department_id')->unsigned();
            $table->integer('state'); //1 active, 0 inactive

            $table->foreign('employee_id')
                        ->references('id')
                        ->on('employee')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
						
            $table->foreign('type_department_id')
                        ->references('id')
                        ->on('type_department')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
						
            $table->primary(['employee_id', 'type_department_id']);
			
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_department');
        Schema::drop('employee');
    }
}
