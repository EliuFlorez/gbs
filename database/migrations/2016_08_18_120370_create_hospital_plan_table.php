<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalPlanTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_plan', function(Blueprint $table)
        {
            $table->increments('id');
			$table->integer("hospital_id")->unsigned();
            $table->integer("plan_id")->unsigned();
            $table->timestamps();
			
			$table->foreign('hospital_id')
                    ->references('id')->on('hospital')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
					
			$table->foreign('plan_id')
                    ->references('id')->on('plan')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital_plan');
    }

}
